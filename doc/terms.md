The Peer Editing Exchange service is provided "as is" without
warranty of any kind, either expressed or implied, including, but not
limited to, the implied warranties of merchantability and fitness for
a particular purpose.  The entire risk of participation is with you.

In no event unless required by applicable law or agreed to in writing
will any copyright holder be liable to you for damages, including any
general, special, incidental or consequential damages arising out of
the use or inability to use the service (including but not limited to
loss of time or data or losses sustained by you or third parties),
even if such holder or other party has been advised of the possibility
of such damages.
