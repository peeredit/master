# Peer Editing Exchange - Frequently Asked Questions

## Ethical Considerations

For writers, how much collaboration is appropriate? There is no single
answer. On one hand, some kinds of writing minimize the importance of
authorship in favor of fostering the finest possible prose (e.g.,
[Wikipedia](http://tools.wikimedia.de/~tangotango/nubio/view.php?id=187)).
On the other hand, there may be composition teachers who prefer
students refrain from any collaboration in order that the skills
of a student can be (ostensibly) assessed unambiguously.

The exchange does not mandate any particular policy.  In the
"instructions and context" box when registering a manuscript, it is up to
the author to set out what kind of feedback is desired and
acceptable. It is up to the reviewer to decide whether to accept an
assignment. It is your responsibility to know the policy at your
institution. When in doubt, contact your instructor.

In any case, it is good practice to keep an exact record of the
evolution of your manuscript in the event that suspicion of plagiarism
arises.

## Why does the due date on my manuscript keep increasing?

There aren't enough members on the exchange yet. If your manuscript
doesn't attract any reviewers by the time half the review period has
elapsed then the review period will be increased back to the original
duration. The way to prevent this from happening is to convince more
people to join the exchange.

## The review I received was pretty unhelpful. What's the deal?

The best way to insure that you receive useful reviews is to give them
to others. Once you build your reputation as a good reviewer then the
system will match your manuscripts with other good reviewers. Until
then, you may continue to receive poor reviews. Sometimes it helps to
request two or more reviewers per manuscript.

## How do I write an excellent review?

There are many resources available on the web. For example:

* [How to write gracious criticism.](http://www.gmu.edu/departments/writingcenter/handouts/eiphand.html)
* [Peter Elbow interview](http://www.etext.org/Zines/Critique/writing/elbow.html)

## Is there any scientific basis for believing that peer editing can improve writing?

Some writing teachers have identified peer editing as an
effective means for manuscript improvement. See:

"Copy editing other people's writing is good practice for improving
your own. It is also less painful than editing your own and much
easier than actually writing. Any piece of prose will do." (p. 14)

[Bem, D. J. (2003) Writing the Empirical Journal Article.](http://dbem.ws/WritingArticle.pdf)

Elbow, Peter. (1973). Writing without teachers. Oxford University
Press.

James, Conely. (1992). A Class Exercise in Proofreading: Getting
Students to Read What They Write. Paper presented at the Annual
Meeting of the College English Association, Pittsburgh, PA.

Rosen, Lois Matz. (1987). Developing Correctness in Student Writing:
Alternatives to the Error-Hunt. The English Journal, Vol. 76, No. 3,
62-69.

Contact me if you are aware of other references to add to this list.

Also contact me if you are interested in doing research. It would be
interesting to compare the writing performance of students who use the
exchange with students who do not use the exchange.

## About Feeds

Typically, feeds offer a summary of web content. On the exchange,
feeds are used to provide you with a regularly updated list of
manuscripts which are available for editing. Feeds are an
alternative to e-mail notification. You probably don't want to use
both feeds and e-mail.

To subscribe, you must use a feed reader. If you click on a feed link
in your browser, you may get a page of gobbledygook.  For more
information about feed readers, visit [Wikipedia's news
aggregator](http://en.wikipedia.org/wiki/News_aggregator) page.

## Can I get a word-count loan?

Yes. For a limited time, word-count loans are available. E-mail your
proposal to pex@peeredit.us.

## What is "manuscript screening?"

It is necessary to remove manuscripts which are spam--filled with
meaningless gibberish. If such manuscripts were not removed then
reviewers could claim to have edited them and fraudulently record
word-count credit. Hence, there is a screening stage before
manuscripts are made available for editing. Currently
screening takes a long time, as long as 24 hours, because each
manuscript is evaluated by PEX staff. However, this is just a
temporary measure. As soon as there is enough trading volume on the
exchange then members who wish to submit a manuscript will first be
required to screen six manuscripts submitted by other members. This
will improve objectivity and turn-around time. With a high enough
trading volume, we anticipate that screening will eventually take less
than 1 hour.

## How is my composite quality of feedback determined?

### Feedback Quality

First of all, keep in mind that feedback quality at different writing
levels (intended audience) is independent. The purpose of the writing level is
to hopefully unconfound the two variables: intended audience and feedback
quality. With that out of the way, we will focus on feedback quality.

Feedback quality is determined by a peculiar recursive
definition. Quality is defined as the weighted average of authors'
ratings of the reviewer's reviews. Each author's weight is the
author's quality rating.
The important characteristic of this system is that it is
a good model of what happens socially.

For example, suppose Alice is a good reviewer. She attracts good
ratings of her reviews. Furthermore, suppose Bob is a poor
reviewer. He attracts low ratings of his reviews. Suppose Steve
reviews a manuscript submitted by Alice and a manuscript submitted by
Bob. Suppose Bob rates Steve's review as poor and Alice rates Steve's
review as excellent. How should Steve's feedback quality be adjusted?
Of course Bob and Alice have rated two separate things. It may be the
case that Steve's review for Bob really is bad and Steve's review for
Alice really is good. Unfortunately, we don't have that data so leave
the possibility aside. What can do with the data we have? Since Alice
is a good reviewer, let's give more weight to her feedback for Steve
than we give to Bob's feedback. That's what the algorithm does.

It may not be practical to calculate a globally exact solution to the
network of equations but a locally "good enough" solution is easy to
estimate.
Here's an example:

![Quality Calculation Example](quality-example.png)

<table border="1">
<tr>
<td ></td>
<th  colspan=2 align="left"  >Alice</th>
<th  colspan=2 align="left"  >Bob</th>
<th  colspan=2 align="left"  >Ron</th>
<th  colspan=2 align="left"  >Zoe</th>
</tr>
<tr>
<td  align="left"  >step</td>
<td  align="left"  >quality</td>
<td  align="left"  >samples</td>
<td  align="left"  >quality</td>
<td  align="left"  >samples</td>
<td  align="left"  >quality</td>
<td  align="left"  >samples</td>
<td  align="left"  >quality</td>
<td  align="left"  >samples</td>
<td ></td>
<td ></td>
</tr>
<tr>
<td  align="right"  >0</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0</td>
<td ></td>
</tr>
<tr>
<td  align="right"  >1</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0</td>
<td  align="right"  >0.25</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.75</td>
<td  align="right"  >0.5</td>
</tr>
<tr>
<td  align="right"  >2</td>
<td  align="right"  >1</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.375</td>
<td  align="right"  >1</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.5</td>
<td  align="right"  >1</td>
</tr>
<tr>
<td  align="right"  >3</td>
<td  align="right"  >1</td>
<td  align="right"  >0.5</td>
<td  align="right"  >0.33</td>
<td  align="right"  >1.5</td>
<td  align="right"  >0.5</td>
<td  align="right"  >1</td>
<td  align="right"  >0.58</td>
<td  align="right"  >1.5</td>
</tr>
</table>

Squares labelled with "Once upon a time" are manuscripts. The circles
connecting with each manuscript are editing assignments. Stars are
members. The fractions labelling the connections between manuscripts
and assignments are the author's rating of the quality of feedback
provided by the editor. The lowest quality is 0 (useless) and the
highest quality is 1 (meticulous, valuable).

At step 0, quality is assumed .5 (the prior) and samples is 0. At step
1, the algorithm considers Alice's manuscript. At step 2, the
algorithm considers Ron's manuscript. Since Alice is a reviewer of
Ron's manuscript, Alice's quality (weight) is updated. This update
triggers a re-evaluation of Alice's manuscript at step 3. Ron is a
reviewer of Alice's manuscript but his quality (weight) remains at .5
so the algorithm stops. The algorithm converges because a single
review is always a small contribution to the reviewer's overall weight
(quality). Of interest are Bob and Zoe's quality ratings. Bob's rating
is less than 3/8 (the equal weighted average of 1/4 and 1/2) because
Alice's opinion is weighted more than Ron's because Ron judged Alice
an excellent reviewer. For the same reason, Zoe's rating is somewhat
greater than 1/2 (the equal weighted average of 1/4 and 3/4).

I believe the only conceivable attack is if an exceptionally good
writer gives most reviewers a poor rating while attracting excellent
ratings for his own feedback. His arrogant behavior would suppress, to
some extent, the quality scores of other skilled writers. However, this
does not seems like more than a theoretical risk.

There are a few more details. New members are assigned an estimated
feedback quality according to the average over the whole
population. As members gain actual ratings, the contribution of the
population average diminishes according to the square root of the sum
of the authors' weights. This formulation is Bayesian in spirit but
I'm not sure if the math is exactly correct. Suggestions to improve
the algorithm are welcome. [See the code for details.](copyright.html)

### Vetting New Members

Suppose a hundred people join the exchange. Suppose 25 of these
members establish their feedback quality as good or excellent.
Furthermore, suppose that when any of these 25 member submit
manuscripts, they limit reviewers to those members who have a track
record of giving good feedback. What has happened is that the top 25
members have established an editing circle and new members (who have
an unknown editing ability as far as the exchange is concerned) will
not have a chance to edit any of the manuscripts registered by the top
25 established members who are in the best position to rate the
quality of feedback of new members.

To remedy this unfortunate situation, the exchange implements a
vetting system. New members are permitted to edit manuscripts submitted
by highly rated established members without deducting word-count
from the manuscript author. The benefits are that highly
rated established members get free reviews in exchange for determining
the quality of the reviews they get and new members get a wider
selection of manuscripts to edit.

## Has there been any press coverage?

+ [Sugar News, 13 Oct 2008](http://sugarlabs.org/go/Sugar_Labs/Current_Events/Archive/2008-10-13)

+ [Adjunct Advice, 27 Aug 2007](http://adjunctcentral.com/index.php/site/peer_editing_exchange/)

+ [Free Software Magazine, 14 Jul 2007](http://www.freesoftwaremagazine.com/blogs/interview_joshua_n_pritikin)

## What about other languages?

There is no reason that this site can't serve other languages besides
English. Contact me if you are interested in doing a translation.

## What inspired you to set up this exchange?

"Real democrats are interested in the individual points of view of
others, because they know that people need each other to hone and
sharpen their ideas and intuitions, to improve them and elaborate
them." [Verhulst, J., & Nijeboer, A. (2007). _Direct Democracy: Facts
and Arguments about the Introduction of Initiative and Referendum_.
Democracy International, Brussels](http://www.democracy-international.org/book-direct-democracy.html), p. 8.

"It might be objected to this approach that such a study
deals only with the point of intersection of a theory of language and a
theory of action. But my reply to that would be that if my conception
of language is correct, a theory of language is part of a theory
of action, simply because speaking is a rule-governed form of behavior.
Now, being rule-governed, it has formal features which admit of
independent study. But a study purely of those formal features, without
a study of their role in speech acts, would be like a formal study of the
currency and credit systems of economies without a study of the role of
currency and credit in economic transactions. A great deal can be said
in the study of language without studying speech acts, but any such
purely formal theory is necessarily incomplete. It would be as if baseball
were studied only as a formal system of rules and not as a game."
Searle, John. (1969). _Speech Acts_. Cambridge University Press, p. 17.

## Enhancement Ideas

### OpenID

OpenID is probably the wrong solution because the design admits
security flaws. A better alternative is to use a keyring or password
manager on your local machine.

### Invitation-only Editing Sessions

Currently, the exchange supports soliciting reviews from the public
anonymously. It might make sense to, in addition, add invitation-only
editing sessions which allow a group of e-mail addresses to work on a
manuscript. This would happen independently of the word-count and
other rating systems. The advantage of such sessions over e-mail would
be the easy availability of change reports. Let me know if you think
this would be useful.
