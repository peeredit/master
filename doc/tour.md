Here is a step-by-step tour of how a manuscript is edited using the
Peer Editing Exchange. (Last updated 29 Aug 2007)

Bob wants to get his manuscript edited.

![](01.png)

He clicks on the "Register Manuscript" button.

![](02.png)

He enters some information about his manuscript.

![](03.png)

He enters a bit more information and clicks the "Submit" button.

![](04.png)

He can check whether the manuscript was uploaded faithfully.

![](07.png)

A stop sign appears in front of his manuscript and the status column
indicates that reviews will be available after 1 hour. (You can
specify the minimum and maximum amount of time you are willing to wait
for reviews. One hour is typically not enough time.)

![](08.png)

Alice, another member of the exchange has subscribed to the PEX
newsfeed "human resources." Bob's manuscript appears on her newsfeed
screen.  (Is it not necessary to use a newsfeed reader. Notification
by e-mail is available as well.)

![](09.png)

Alice decides to edit the manuscript. She clicks on the "Download
Original" button and makes a first pass over it. She judges that the
writing is about 9th to 12th grade level.

![](10.png)

Alice spends an hour checking the manuscript and uploads her revised
copy. (The "contact author" option is no longer available.)

![](11.png)

After uploading her revision, Alice's changes are immediately
highlighted in context.

![](12.png)

You can also inspect the line-wise changes if that is easier to read.

![](13.png)

Bob notices that somebody has decided to review his manuscript.

![](14.png)

After 13:30, Bob checks again and the review is available.

![](15.png)

Bob studies the feedback and decides to rate the review as "helpful,
many good suggestions."

![](17.png)

If Alice did not have enough time to complete her review, she has
at least 24 hours to submit a late review.

![](18.png)

Bob proceeds to archive the manuscript.

![](19.png)

Bob finds that 2787 words are deducted from his word-count balance.

![](20.png)

On the other hand, Alice sees her word-count balance increase by the
same amount.

