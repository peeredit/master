# Copyright Information

## Manuscripts

Authors retain respective ownership of all contributions made via the
member interface (web and e-mail).

## Web Site

The source code is &copy; Copyright 2007 Joshua Nathaniel Pritikin
<jpritikin@pobox.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You can retrieve a copy of the license and source code using
[GIT](http://git.or.cz/):

	git clone git://gitorious.org/peeredit/master.git
