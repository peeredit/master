#!/usr/bin/perl

use strict;
use warnings;
use lib '../src';
use PPX::Test;
use PPX::Config;
use PPX::DB;
use PPX::Util;
use Carp;
use Time::HiRes qw(gettimeofday tv_interval);

use constant TITLE => 'Torture Test';

connect_to_database();

our $Year = 1900 + (localtime(time))[5];

sub enable_screening {
  my ($email) = @_;
  my $id = email_to_id($email);
  $DB->do(<<SQL, {}, $Year - 30, $id);
UPDATE member SET member_birth = ? WHERE member_id = ?
SQL
  if (!int $DB->do(<<SQL, {}, $id)) {
UPDATE member_lang
SET member_lang_samples = '[0:4]={3,3,3,3,3}',
  member_lang_quality = '[0:4]={1,1,1,1,1}'
WHERE member_id = ?
SQL
      $DB->do(<<SQL, {}, $id);
INSERT INTO member_lang (member_id, member_lang_samples, member_lang_quality)
VALUES (?, '[0:4]={3,3,3,3,3}', '[0:4]={1,1,1,1,1}')
SQL
  }
}

sub create_member {
  my ($email, $year) = @_;
  Log LOG_DEBUG, "ENTER create_member($email, $year)";

  clean_mailbox($email, 'account activation');

  talk(HTTP::Request->new(GET => SITE));
  talk click_link('register');

  $form0->value('termsok', 'on');
  $form0->value('email', $email);
  $form0->value('birth', $year);
  $form0->value('p1', 'abc');
  $form0->value('p2', 'abc');
  talk $form0->click;
  if ($matter_id eq 'Member::front') {
    Log LOG_WARN, "$email exists";
    Log LOG_DEBUG, "EXIT create_member($email, $year)";
    return;
  }
  if ($matter !~ /confirmation e-mail has been sent/) {
    drop_member(email_to_id($email));
    return create_member($email, $year);
  }

  my $body = recv_mail($email, 'account activation');
  $body =~ /visit this web page:\s+(\S+)\s/;
  my $url = $1;
  Log LOG_DEBUG, "Activation URL $url";
  talk(HTTP::Request->new(GET => $url));
  die if !$matter_id eq 'Member::query_tz1';

  enable_screening($email);

  $form0->value('continent', 'Asia');
  talk $form0->click;

  $form0->value('city', 'Calcutta');
  talk $form0->click;

  for my $in ($form0->inputs) {
    next if (!defined $in->name or $in->name !~ /^\d+$/);
    $in->value('on');
    last;
  }
  talk $form0->click;
  die if $matter_id ne 'Member::front';

  Log LOG_DEBUG, "EXIT create_member($email, $year)";
}

sub switch_to {
  my ($email, $screen) = @_;

  if (email_to_id($email)) {
    enable_screening($email);
  }

  talk(HTTP::Request->new(GET => SITE));
  $form0->value('login', $email);
  $form0->value('passwd', 'abc');
  talk $form0->click;

  if ($matter_id ne 'Member::front') {
    create_member($email, $Year - (30 + int rand(20)));
  }
}

sub submit_manuscript {
  my ($title) = @_;

  if (find_link($title)) {
    Log LOG_DEBUG, "manuscript already exists";
    return;
  }

  talk click_link('register manuscript');

  $form0->value('title', $title);
  $form0->value('genre', 'incoherent rambling');
  $form0->value('due', '4h');
  $form0->value('manuscript', "documents/1.txt");
  $form0->value('min_rev', 1);
  $form0->value('spellok', 'on');
  talk $form0->click;
  talk click_link('continue');
  return if $matter_id eq 'Member::find_screening_needed1';
  talk $form0->click;
}

sub test_spam {
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  for my $m (2..4) {
    switch_to('m'.$m.'@test.org', 1);
    talk click_link 'screen';
    talk click_link TITLE;
    talk click_link 'screen';
    talk click_link 'spam';
    die if $matter_id !~ /^Member::find_screening_needed/;
  }
  
  switch_to('m1@test.org');
  talk click_link 'history';
  talk click_link TITLE;
  die if $matter !~ /manuscript was flagged as spam/;
}

sub test_prohibited {
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  for my $m (2..4) {
    switch_to('m'.$m.'@test.org', 1);
    talk click_link 'screen';
    talk click_link TITLE;
    talk click_link 'screen';
    talk click_link 'prohibited';
    die if $matter_id !~ /^Member::find_screening_needed/;
  }
  
  switch_to('m1@test.org');
  $DB->do('update member set member_birth = ? where member_id = ?', {}, $Year - 10, email_to_id('m1@test.org'));

  talk click_link TITLE;
  my $err = extract_div_error();
  die if $err !~ /not allowed to submit/;
  die if $form0->find_input('adult');

  switch_to('m1@test.org');
  talk click_link TITLE;
  $err = extract_div_error();
  die if $err !~ /must tick the appropriate checkbox/;
  die if ($form0->value('adult')||'') eq 'on';
}

sub test_miscat {
  clean_mailbox('m1@test.org', 'Re');
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  for my $m (2..4) {
    switch_to('m'.$m.'@test.org', 1);
    talk click_link 'screen';
    talk click_link TITLE;
    talk click_link 'screen';
    talk $form0->click;
    my $err = extract_div_error();
    die if $err !~ /read the instructions/;
    $form0->param('really', 'on');
    talk $form0->click;
    die if $matter_id !~ /^Member::find_screening_needed/;
  }

  switch_to('m1@test.org');
  talk click_link TITLE;
  my $err = extract_div_error();
  die if $err !~ /inappropriately represent your manuscript/;

  my $body = recv_mail('m1@test.org', 'Re');
  die if $body !~ /problem\:\s+(\S+?)\s/;
  my $url = $1;
  Log LOG_DEBUG, "Quicklogin $url";
  talk $url;
  $err = extract_div_error();
  die if $err !~ /inappropriately represent your manuscript/;
}

sub test_confused {
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  my @action = qw(miscat prohibit spam);
  for my $m (2..4) {
    switch_to('m'.$m.'@test.org', 1);
    talk click_link 'screen';
    talk click_link TITLE;
    talk click_link 'screen';
    my $act = shift(@action);
    if ($act eq 'miscat') {
      talk $form0->click;
    } else {
      my $link = click_link $act;
      talk $link;
      die if $matter_id !~ /^Member::find_screening_needed/;
      talk $link;
      die if $matter !~ /your decision is final/;
    }
  }
}

sub test_overdue_screening {
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  switch_to('m2@test.org', 1);
  talk click_link 'screen';
  talk click_link TITLE;
  talk click_link 'screen';
  time_shift 1.1;
  talk click_link 'spam';
  die if $matter !~ /More than one hour has elapsed/;
}

sub test_screening_withdrawn {
  $DB->do('delete from manuscript');

  switch_to('m1@test.org');
  submit_manuscript(TITLE);
  die if $matter_id ne 'Member::front';

  switch_to('m2@test.org', 1);
  talk click_link 'screen';
  talk click_link TITLE;
  talk click_link 'screen';
  my $link = click_link 'spam';

  switch_to('m1@test.org');
  my @w = grep /withdraw/, get_link_targets();
  talk $w[0];

  talk $link;
  # no error report expected
  die if $matter !~ /no manuscripts in need of screening/;
}

sub test_submit_skip_screening {
  $DB->do('delete from manuscript');

  for my $n (2..13) {
    switch_to('m'.$n.'@test.org');
    submit_manuscript(sprintf 'manuscript %02d', $n);
  }

  switch_to('m1@test.org', 1);
  submit_manuscript('manuscript a');
  die if $matter_id ne 'Member::find_screening_needed1';
  my $err = extract_div_error();
  die if $err !~ /Before you can submit more manuscripts/;

  talk click_link 'manuscript 02';
  talk click_link 'screen';
  for my $k (qw(context genre title abstract audience really)) {
    $form0->value($k, 'on') if
      $form0->find_input($k, 'checkbox');
  }
  talk $form0->click;
  talk click_link 'go back';

  talk click_link 'submit';
  talk $form0->click;
  die if $matter_id ne 'Member::front';
}

sub test_maximum_screening {
  $DB->do('delete from manuscript');

  for my $n (2..13) {
    switch_to('m'.$n.'@test.org');
    submit_manuscript(sprintf 'manuscript %02d', $n);
  }

  switch_to('m1@test.org', 1);

  talk click_link 'screen';
  my @m = grep /show_manuscript/, get_link_targets();
  die if @m < 10;
  for (my $x=0; $x < 10; $x++) {
    talk $m[$x];
    talk click_link 'screen';
    if ($x < 9) {
      die $x if $matter !~ /Your Evaluation/;
    } else {
      die if $matter !~ /completed your share of the screening work/;
      last;
    }
  }

  # Insure that the count of screening assignments is reset after
  # each manuscript submission.
  #
  switch_to('m1@test.org', 1);
  submit_manuscript('fake');
  talk $m[9];
  talk click_link 'screen';
  die if $matter !~ /Your Evaluation/;
}

while (@ARGV) {
  my $opt = shift @ARGV;
  if ($opt eq '-v') {
    ++$LogLevel;
  } else {
    warn "$opt ??";
  }
}

sub all_tests {
  my $t0 = [gettimeofday];
  for my $test (qw(test_spam test_prohibited
  test_miscat test_overdue_screening
  test_screening_withdrawn test_submit_skip_screening
		   test_maximum_screening

test_confused)) { # test_confused should be the last test
    Log LOG_WARN, $test;
    no strict 'refs';
    $test->();
  }
  my $elapsed = tv_interval($t0);
  Log LOG_WARN, sprintf("\nelapsed time %.1fs\n", $elapsed);
}

all_tests();

# don't confuse other tests
$DB->do('delete from member_lang');
