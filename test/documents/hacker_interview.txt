http://www.itmanagersjournal.com/articles/10695

Interviewing hackers
February 21, 2006 (11:00:00 PM)
By: Joshua N. Pritikin and Sriram Narayanan

Many articles address the question of how to interview people when trying to fill a technical post. Perhaps the most important part of such an interview is the technical assessment. Here's a technique that we believe can improve the accuracy of technical assessment.

The next time you have a candidate for a technical interview, try to gauge his self-study ability by assigning a problem that he never has solved before. If the candidate has never worked on a similar problem, so much the better. Place all available documentation within reach. Give the candidate a computer. Then watch what happens. If the candidate makes good progress towards a solution, tilt your mental scale toward hire.

Self-study

The importance of self-study is well known among the GNU/Linux community. Eric Raymond, in "How to Become a Hacker," mentions self-study as an essential ingredient in the hacker personality. In the hacker culture, the student-teacher relationship is between you and the problem. There is no middleman. Such a high degree of self-sufficiency is crucial because of the nature of our profession.

Tony Kuphaldt, a teacher of electronics, offers lucid reasoning on the issue:

    A person with a strong knowledge of "the basics" but with no ability to teach themselves never could succeed in a fast-changing profession, at least not without the help of others to continually teach them what they did not know. Conversely, a person knowing nothing but how to teach themselves could always learn what they needed to know in whatever profession they chose, with or without anyone else's assistance. Ergo, the ability to learn independently is more important than the mastery of specific knowledge areas.

Traditionally, one asks technical questions at an interview to verify that a candidate actually knows what he claims to know. Such questions may target professional certifications, specific technical trivia, or code written previously. The primary weakness of such methods is that the candidate has significant control over which claims are open to verification. The candidate may be tempted to bluff, for example, by cramming facts during the few days prior to the interview.

We suggest using technical questions to gauge a candidate's self-study ability. Ask a candidate to solve a programming problem on the spot -- but determine in advance how much familiarity the candidate is likely to have with the given problem.

For example, imagine you ask the candidate to write C code to reverse a string in-place. How do you interpret the candidate's performance? There are three cases:

   1. If the candidate claims to be a expert, you can expect convergence to a correct solution with negligible trial and error.
   2. If the candidate is somewhat familiar with the problem domain, it's hard to know what to expect. It is not clear how this uncertain kind of questioning provides useful data to the interviewer.
   3. If the candidate is unfamiliar with the problem domain, you are tapping his ability to learn new material. When a candidate is denied access to a domain expert, self-study ability can be revealed.

Hence, the interviewer must carefully select programming assignments using data uncovered by traditional technical inquiry. On one hand, a candidate wants to answer technical questions comprehensively. On the other hand, comprehensive answers better reveal what the candidate doesn't know. These two opposing goals work to keep the candidate honest.

There's nothing wrong with asking about simple string manipulation functions, per se. It depends on the candidate. For one candidate, in-place string reversal is effective for self-study assessment. For another candidate, the same assignment merely verifies that an expert C programmer knows how to manipulate strings.

Once you decide on a particular self-study assignment, you can discuss your intent to gauge self-study ability with the candidate and hand over the assignment. Ensure that the environment is prepared. Your carefully selected programming assignment should not be hampered by a missing reference book or a hardware failure.

The longer you watch a candidate work, the more accurate your evaluation can be. A lengthy assignment, therefore, is desirable, though of course it is not always possible for someone to complete a lengthy assignment in an hour. The fact that the assignment might require more time is not a real problem. Some tangible progress -- or the lack thereof -- is sufficient to rate the candidate.

A candidate may over-perform if the interviewer provides too much help. Keep in mind that the whole point of assessing self-study is to gauge how well the candidate works independently. As an interviewer, you must exercise self-control. React to questions with encouragement, not with answers. Observe the candidate.

Avoid putting undue pressure on the candidate. The knowledge that placement selection depends upon solving the given assignment usually is enough to stifle a candidate's problem-solving ability (viz. Punished by Rewards ). As much as possible, put the candidate at ease.

Question bank

In the spirit of one famous mock final exam, here are a few questions that may exercise a candidate's self-study ability. These questions intend to test whether a candidate can fend for himself when on the job.

For an experienced Java programmer who is familiar with fat client applications:

    * Write Eclipse plugins for either of the following requirements:
      1) Select resources and add to an existing or new zip file.
      2) Select a Jar and list all the servlets within it.
    * A Mailet for Apache James where, if a mail is received from a particular e-mail address, with a specific subject body then
      1) Extract the attachment and save it to some location.
      2) Update some column in a database.

For a programmer who has never worked with, say, Perl 6, the task would be to write an indent algorithm for Perl 6.

Parting thoughts

Although it demands time, effort, and ingenuity, a self-study assessment rewards the interviewer with a useful metric: the result of the problem-solving session and the amount of time taken. You can gauge the quality and depth of analysis of the problem, illustrations, and use cases that the candidate may have offered, and so on.

If schooling today poorly supports independent learning, one of the reasons might be because businesses are not demanding such a skill. Most companies can benefit from hiring a hacker. Instead of asking your recruiter for only a specific skill set, also ask for self-study ability.

Sidebar: The other side

What if you're on the other side of the interviewer's table? In general, as a candidate, you have to be nimble and cope with whatever happens as best you can. Is there a good way to prepare for a self-study assessment?

You might assume that self-study ability cannot be taught -- either you're born with it or stuck without it. Not true. Some educators harbor a deep appreciation for self-study and can help you develop yourself toward self-sufficiency.

We are not aware of a computer science educator who specializes in teaching self-study. But remember, it's not the domain of study that is important, it's the attitude. A professor of electricity and electronics, Tony Kuphaldt, demonstrates what is possible. His curriculum, Socratic Electronics, is meticulously designed to nurture self-study.

Self-study is not merely today's eccentric intellectual trend but a sharpening of the meaning and method of education. It can be traced back to George Polya and Maria Montessori, and is certain to be a recurring theme of future educators.

Joshua N. Pritikin worked as a software engineer for eight years at various financial firms in the New York City area. Today he is preparing for graduate school. Sriram Narayanan works at ThoughtWorks in Bangalore as an Application Developer.
