db/tz.gettext: db/gen-tz
	cd db; ./gen-tz gettext > ../$@

locale/files:
	git ls-files src | grep 'pm$$' > $@
	echo db/tz.gettext >> $@

LOCALE_FILES := $(shell cat locale/files)

locale/ppx.po: locale/files $(LOCALE_FILES)
	xgettext -j -d ppx -p locale -L perl -f locale/files

src/PPX/TZ.pm: db/gen-tz
	cd db; ./gen-tz perl > ../$@.new && mv ../$@.new ../$@

htdocs/static:
	test -d $@ || mkdir $@

htdocs/static/copyright.html: doc/copyright.md htdocs/static
	markdown $< > $@

htdocs/static/faq.html: doc/faq.md htdocs/static
	markdown $< > $@

htdocs/static/tour.html: doc/tour.md doc/tour.head htdocs/static
	cp doc/tour.head $@
	markdown $< >> $@

htdocs/static/terms.html: doc/terms.md htdocs/static
	@echo
	@echo '** Whenever the Terms and Conditions of Use change, you must change the'
	@echo "** default in the database for member_termsok. Don't forget!"
	@echo
	markdown $< > $@

static: htdocs/static/copyright.html htdocs/static/terms.html htdocs/static/faq.html htdocs/static/tour.html

dir:
	for dir in htdocs/download htdocs/captcha var/jobs; do \
	  test -d $$dir || mkdir $$dir; \
          chown www-data $$dir || exit; \
	done

# su postgres -c 'cd ~joshua/ppx/ && make updatedb' && sv 1 apache2
#
updatedb: db/extract_refresh_schema db/refresh_schema.in
	cd db; ./extract_refresh_schema | psql ppx && ./grant
	@echo "Now you must restart apache2."

.PHONY: dir static updatedb
