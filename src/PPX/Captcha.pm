use strict;
use warnings;

package PPX::Captcha;
use PPX::Config;
use PPX::HTML;
use PPX::Util;
use GD::SecurityImage;
use Locale::gettext;

use constant CAPTCHA_LEN => 6;
use constant CAPTCHA_EXPIRE => 1800;

our @ISA = 'Exporter';
our @EXPORT = qw(eval_captcha captcha_table_row);

sub create {
  my $si = GD::SecurityImage->new(width => 250,
				  height => 60,
				  font => $BaseDir.'/art/VeraBd.ttf',
				  ptsize => 14,
				  rndmax => CAPTCHA_LEN,
				  rnd_data => [2,3,4,7,
					       'C'..'D','F'..'H',
					       'J'..'M','P','R','T','V'..'Y'],
				  scramble => 1,
				  lines => 5);
  $si->random;
  $si->create('ttf', 'default');
  $si->particle(1024);
  my ($data, undef, $code) = $si->out(force => 'png');
  my $token = gen_token(8);

  my $png = $BaseDir.'/htdocs/captcha/'.$token.'.png';
  {
    open my $fh, ">$png" or do {
      warn "open >$png: $!";
      return;
    };
    print $fh $data;
  }

  my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO captcha (captcha_token, captcha_code) values (?,?)
SQL
  $i1->execute($token, $code);
  $token
}

sub invalid {
  my ($code, $token) = @_;
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT COUNT(*) FROM captcha
WHERE EXTRACT(EPOCH from now() - captcha_ctime) < ? AND
  captcha_token = ? AND captcha_code = ?
SQL
  $q1->execute(CAPTCHA_EXPIRE, $token, uc $code);
  my ($ok) = $q1->fetchrow_array;
  $q1->finish;
  !$ok;
}

sub captcha_table_row {
  my ($err) = @_;
  my $token = create();
  hidden(md5sum => $token).
  Tr(td({style=>'vertical-align: middle;'},
	lsty($err,'captcha', gettext('Verification Code').':')),
     td(tag('table'),
	Tr(td({style=>'padding:0; vertical-align: middle;'},
	      textfield('captcha', size=>CAPTCHA_LEN, maxlength=>CAPTCHA_LEN)),
	   td({style=>'padding:0;'}, hskip(4)),
	   td({style=>'padding:0; vertical-align: middle;'},
	      img("captcha/$token.png", 'captcha image'))),
	gat('table'))
    )
}

sub eval_captcha {
  my ($err, $flag) = @_;

  if (invalid($R->param('captcha'), $R->param('md5sum'))) {
    push @$err, gettext('In order to gain some assurance that you are
a genuine human being and not a spam robot, please enter the verification
code depicted in the image.');
    $flag->{captcha} = 1;
  }

  # Otherwise automated regression testing is difficult.
  if (@$err == 1 and $flag->{captcha} and SYSTEM_TEST) {
#    warn "Ignoring captcha failure because SYSTEM_TEST is true";
    @$err = ();
    %$flag = ();
  }
}

sub clean {
  my ($basedir) = @_;
  $DB->do(<<SQL, {}, CAPTCHA_EXPIRE);
DELETE FROM captcha
WHERE EXTRACT(EPOCH from now() - captcha_ctime) > ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT captcha_token FROM captcha
SQL
  $q1->execute;
  my %valid;
  while (my ($token) = $q1->fetchrow_array) { $valid{$token} = 1 }

  my $del = 0;
  my $dir = "$basedir/htdocs/captcha";
  opendir my($dh), $dir;
  while (my $file = readdir($dh)) {
    next if $file !~ /^(.*)\.png$/;
    if (!$valid{$1}) {
      unlink("$dir/$file")==1 or warn "unlink $dir/$file: $!";
      ++$del;
    }
  }
#  print "$del\tstale captcha\n"
#    if $del;
}

1;
