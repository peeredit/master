# Copyright (C) 2003, 2004, 2005, 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::Guest::Impl;
use PPX::Captcha;

sub crash_test1 {
  die 'This is a test';
}

sub crash_test2 {
  banner('Oops');
  die 'This is a test';
}

sub reset_passwd {
  my ($class, $err) = @_;
  $err ||= {};

  banner(gettext('Reset Password'));

  print(br);

  if ($err->{from_new}) {
    my $m = sprintf(gettext("The address `%s' is already registered."),
		    $err->{from_new});
    print(div({ class => 'error' }, txt($m)));
  } elsif ($err->{msg}) {
    print(div({ class => 'error' }, $err->{msg}))
  }

  print(p(txt(gettext('To regain access to your account, you can request
that your password be reset.  If you choose to do so,
you will shortly receive your new password by e-mail.'))));

  my @email;
  my $email = extract_email($R->param('email'));
  if ($email) {
    @email = (value => $email);
  }

  print(start_form('eval_reset_passwd'),
	tag('table', cellspacing => 8),
	Tr(td(lsty($err,'email', gettext('E-mail').':')),
	   td(textfield('email', size => 40, maxlength=>EMAIL_LEN, @email))),
	captcha_table_row($err),
	gat('table'),
	p({align => 'center'}, submit(gettext('Reset Password'))),
	gat('form'));
}

sub eval_reset_passwd {
  my ($class) = @_;
  my (%err, @err);

  my $email = extract_email($R->param('email'));
  my $id;

  if (!$email) {
    push @err, gettext('Enter your e-mail address.');
    $err{email} = 1;
  } else {
    ($id) = $DB->selectrow_array('select member_id from member where member_email = ?', {}, $email);
    if (!defined $id) {
      push @err, gettext('That e-mail address is not registered with us.');
      $err{email} = 1;
    }
  }

  eval_captcha(\@err, \%err);

  if (@err) {
    $err{msg} = join_remedy(@err);
    return $class->reset_passwd(\%err);
  }

  my $msgstamp = $DB->selectrow_array('select current_date - member_msgstamp from member where member_id = ?', {}, $id);
  if ($msgstamp == 0) {
    banner(gettext('Reset Password'));

    my $m = sprintf(gettext("An e-mail has already been sent to `%s' today.
If you don't receive this e-mail then contact %s for assistance."),
                    $email, EMAIL_FROM);

    print(br.div({ class => 'bigmsg' }, p(txt($m))));
    return;
  }

  my @chars = ('a'..'z', 'A'..'Z', 0..9);
  my $pwd = '';
  for (my $i=0 ; $i < 9 ; ++$i) {
    $pwd .= $chars[rand @chars];
  }
  
  $DB->do("update member set member_password=?, member_activated='p'
 where member_id=?", {}, Crypt($pwd), $id);
  
  my $body = sprintf(gettext('
We received a request to reset your password.  Accordingly, your
password has been reset to:

  %s

Please use this new password to login to Peer Editing Exchange:

  %s

Please reply to this e-mail if you have any other questions.'), $pwd, $R->url);

  sendmail($id, $email, gettext('Password reset'), $body);
  
#  activity_notification($email, "[Orwell] $email password reset");
  
  $class->await_login($email);
}

sub await_login {
  my ($class, $email) = @_;

  banner(gettext('Password Changed'));

  my $m = sprintf(gettext("Your password is changed.  The new password
has been sent to `%s'."), $email);

  print(br.div({ class => 'bigmsg' }, p(txt($m))));
}

sub front {
  my ($class, $err) = @_;
  $err ||= {};

  banner('Welcome');

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(div({ style => "float: left; margin: 1em;" },
	    libimg('pencil-big.png')),
	p("Don't worry, it is
much easier to correct somebody else's writing than your own!"),
	p(a({ href => '/static/tour.html' },
	    'Please take a tour of the exchange!')),
       );

  print(p('The exchange facilitates matching peers with respect to writing
skill. It is not reasonable to expected a 6th grader to edit
college level writing and perhaps visa versa.  Each participant is rated on a
coarse scale for writing and editing ability by other participants.
Peers are matched on the basis of composite ability scores.'),
	p('You can also take the opportunity to improve your editing ability.
When two or more participants edit the same manuscript then the highest
rated review is made available to the other reviewers. By writing your
own critique and then studying the best critique, you can refine your
skill with words.'),
	tag('p'),
	tag('table'),
	Tr(td('Get your manuscript edited immediately.
There is never any monetary cost associated with using this site.'),
	   td(hskip(4)),
	   td(leaf(gettext('Register'), 'handshake'))),
	gat('table','p'),
	hr);

  if (1) {
    print(p('Recent submissions:'),
	  tag('table'),
	  join(th(hskip(4)),
	       th('When (UTC)'),
	       th('Genre'),
	       th('Title'),
	       th('Words')));

    my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_submitted, manuscript_title, manuscript_wc, manuscript_genre
FROM manuscript
WHERE manuscript_submitted IS NOT NULL and manuscript_approved IS NOT NULL AND
  manuscript_withdrawn IS NULL AND NOT manuscript_vulgar
ORDER BY manuscript_submitted DESC
LIMIT 8
SQL
    $q1->execute;
    while (defined(my $m = $q1->fetchrow_hashref)) {
      print(Tr(join(td(),
		    td(substr($m->{manuscript_submitted}, 0, 16)),
		    td(txt(truncate_to_words($m->{manuscript_genre}, 2))),
		    td(txt(truncate_to_chars($m->{manuscript_title}, 50))),
		    td({ align => 'right' }, $m->{manuscript_wc}),
		   )));
    }
    print(gat('table'));
  }

  login_box();
}

sub login {
  my ($class, $err) = @_;
  $err ||= {};

  banner('Login');

  login_box($err);
}

sub login_box {
  my ($err) = @_;
  $err ||= {};

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  my (@login);
  {
    my $login = $R->param('login');
    if ($login) {
      $login = substr $login, 0, EMAIL_LEN;
      @login = (value => $login);
    }
  }

  print(p(txt(gettext('If you already have an account then you may login below:'))),
	start_form('evaluate_login'),
	tag('table', cellspacing => 8),
	Tr(td(lsty($err,'login', gettext('E-mail').':')),
	   td(textfield('login', tabindex=>1, size => 30, maxlength=>EMAIL_LEN, @login)),
	   td(hskip(6)),
	   td()),
	Tr(td(lsty($err,'password', gettext('Password').':')),
	   td(password('passwd', tabindex=>2, maxlength => PASSWORD_LEN)),
	   td(leaf("Forgot your password?", 'reset_passwd'))),
	Tr(td(),
	   td({ colspan=>2 }, checkbox('cookie', $R->param('cookie')), ' ',
	      label({ tabindex=>3 }, 'cookie', txt(gettext('Keep me signed in for a year.'))),
	      ' ('.
	      gettext('Enable this option only if you are using your own computer.').')')),
	Tr(td(),
	   td({ align => 'center' },
	      submit(gettext('Login'), tabindex=>4))),
	gat('table'),
	gat('form'));
}

sub evaluate_login {
  my ($class) = @_;
  my $member_prop = get_member_properties();
  my (%err, @err);

  my $passwd;
  my $cookie;
  my $u;

  my @pl = split(/ /, member('session_payload') || '');

  if (@pl == 3 and $R->param('md5sum')) {
    $cookie = $pl[1];
    $passwd = $pl[2];

    eval_captcha(\@err, \%err);

    my $q1 = $DB->prepare_cached(<<"SQL");
SELECT member_password, member_activated, member_email, $member_prop
FROM member_tz m
WHERE member_id = ?
SQL
    $q1->execute($pl[0]);
    $u = $q1->fetchrow_hashref;
    $q1->finish;
    $u->{member_id} = $pl[0];
  } else {
    if (!$R->param('login')) {
      push @err, gettext('Enter your e-mail address.');
      $err{login} = 1;
      $err{password} = 1;
    }
    if (!$R->param('passwd')) {
      push @err, gettext('Enter your password.');
      $err{password} = 1;
    }
    $cookie = $R->param('cookie')? 1 : 0;
    if (@err) {
      $err{msg} = join_remedy(@err);
      return $class->login(\%err);
    }

    my $email = extract_email($R->param('login'));
    my $sth =
      $DB->prepare_cached(<<"SQL");
SELECT member_id, member_password, member_activated, member_email, $member_prop
FROM member_tz m
WHERE member_email = ?
SQL
    $sth->execute($email);
    $u = $sth->fetchrow_hashref;
    $sth->finish;

    $passwd='';
    if ($u) {
      $passwd = Crypt($R->param('passwd'), $u->{member_password});
    }
  }

  if ($u and $u->{member_failed_login} > 3 and
      !($R->param('md5sum') and !@err)) {
    banner('Verification');

    if (@err) {
      print(div({ class => 'error' }, join_remedy(@err)));
    }

    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE session SET session_payload = ?
WHERE session_id = ?
SQL
    $u1->execute(join(' ', $u->{'member_id'}, $cookie, $passwd),
		 member('session_id'));

    print(start_form('evaluate_login'),
	  p("There have been $u->{member_failed_login} recent failed
attempts to login into your account."),
	  tag('table', cellspacing => 8),
	  Tr(td(gettext('E-mail').':'), td(txt($u->{member_email}))),
	  Tr(td(gettext('Password').':'), td('*' x 10)),
	  Tr(td($cookie? '[X]' : '[ ]'), td(gettext('Remember Me'))),
	  captcha_table_row(\%err),
	  gat('table'),
	  p({align => 'center'},
	    leaf("Forgot your password?", 'reset_passwd'), hskip(4),
	    submit(gettext('Continue Login'))),
	  gat('form'));
    return;
  }

  my $ok = ($u and $passwd eq $u->{member_password});
  if (!$ok) {
    if ($u) {
      my $u1 = $DB->prepare_cached(<<SQL);
UPDATE member SET member_failed_login = member_failed_login + 1
WHERE member_id = ?
SQL
      $u1->execute($u->{member_id});
    }
    $err{msg} =
      txt(gettext('That e-mail / password combination is unrecognized.'));
    return $class->login(\%err);
  }

  activation_problem($u) or do {
    setup_login($u, $cookie);
    'PPX::Member'->front();
  };
}

sub activation_problem {
  my ($u) = @_;
  if ($u->{member_activated} eq 'n') {
    my %err;
    my $m = sprintf(gettext(q[Your account is not activated yet. Please
check your e-mail for the account activation instructions. If you
don't receive this e-mail then contact %s for assistance.]), EMAIL_FROM);
    $err{msg} = txt($m);
    'PPX::Guest'->login(\%err);
    return 1;
  }
  if ($u->{member_activated} eq 'p') {
    # Password was reset so if we got a login with the correct
    # password then we also know the e-mail address is good.
    $DB->do("update member set member_activated = 'y' where member_id = ?",
	    {}, $u->{member_id});
  }
  0;
}

sub setup_login {
  my ($u, $cookie) = @_;

  if (!exists $u->{stale}) {
    carp 'stale unknown, assuming true';
    $u->{stale} = 1;
  }

  for my $k (qw(member_id member_email member_activated stale)) {
    confess $k if !exists $u->{$k};
  }

  if ($u->{stale}) {
    my $u_hitstamp = $DB->prepare_cached('update member
set member_hitstamp = now() where member_id = ?');
    $u_hitstamp->execute($u->{member_id});
  }
  return
    if (member()||0) == $u->{member_id};

  $DB->do('delete from session where member_id = ?',
	  {}, $u->{member_id})
    if !SYSTEM_TEST;
  $DB->do('insert into session (member_id) values (?)', {}, $u->{member_id});
  my $session_id = currval('session', 'session_id');
  my ($session_salt) =
    $DB->selectrow_array("select session_salt from session where session_id = ?", {}, $session_id);

#   activity_notification($u->{member_email},
# 			"[Orwell] $u->{member_email} login$to",
# 	   join('', map { "$_\n" }
# 		$ENV{REMOTE_ADDR},
# 		$ENV{HTTP_USER_AGENT}));

  $u->{session_id} = $session_id;
  $u->{session_salt} = $session_salt;
  set_session_id($u, $cookie);

  my $caller = (caller(1))[3];
  $caller =~ s/^PPX::\w+?::Impl:://;
  set_member('leaf', $caller);  # got reset, so fill something
}

sub handshake {
  my ($class, $err) = @_;
  $err ||= {};

  banner(gettext('Registration'));

  print(div({ class => 'section'}, gettext('Terms and Conditions of Use')));

  open my $fh, "$BaseDir/doc/terms.md" or do {
    warn "open $BaseDir/doc/terms.md: $!";
    print(div({class => 'error'}, 'Unable to read the Terms and Conditions'));
    return;
  };
  my $terms = join '', <$fh>;

  print(start_form('new_member'),
	p(textarea('terms', $terms, 12, 80, readonly => 1)));

  print(div($err->{termsok}? { class => 'error' } : {},
	    join('',
		 checkbox('termsok', $R->param('termsok')), ' ',
		 label('termsok', gettext('I agree to the Terms and Conditions of Use.'))))
       );

  print(div({ class => 'section'}, gettext('Your Profile')));

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(p(txt(gettext('Your e-mail address will be kept confidential.
It will not be revealed to other participants. Assuming you do not reveal
your own identity by accident (e.g., in a by-line), this service
can be used anonymously.'))));

  my (@email, @dn, @dy);
  {
    my $email = extract_email($R->param('email'));
    if ($email) {
      @email = (value => $email);
    }
  }
  my @birth;
  {
    no warnings 'numeric';
    my $y = int($R->param('birth') || 0);
    if ($y) {
      @birth = (value => substr($y, 0, 4));
    }
  }
  print(
	tag('table', cellspacing => 8),
	Tr(td(lsty($err,'email', gettext('E-mail').':')),
	   td(textfield('email', size => 40, maxlength=>EMAIL_LEN, @email))),
	Tr(td(lsty($err,'birth', gettext('Year of Birth (YYYY)').':')),
	   td(textfield('birth', size => 4, maxlength=>4, @birth))),
	Tr(td({ colspan => 3 }, txt(cheap_password_hint()))),
	Tr(td(lsty($err,'password', gettext('Password').':')),
	   td(password('p1', maxlength => PASSWORD_LEN))),
	Tr(td(lsty($err,'password', gettext('Re-type Password').':')),
	   td(password('p2', maxlength => PASSWORD_LEN))),
#	captcha_table_row($err),
	gat('table'));

  print(p({align => 'center'}, submit(gettext('Register'))),
	gat('form'));
}

sub new_member {
  my ($class) = @_;
  my (%err, @err);

  if (!$R->param('termsok')) {
    push @err, gettext("Please read the Terms and Conditions of Use.
If you agree then tick the checkbox.
If you do not agree then please contact us and explain why.");
    $err{termsok} = 1;
  }

  my $email = extract_email($R->param('email'));
  if (!$email) {
    push @err, gettext('Enter a valid e-mail address.');
    $err{email} = 1;
  }

  my $year = 1900 + ((localtime)[5]);
  my $ymin = $year - 150;

  my $birth;
  {
    no warnings 'numeric';
    $birth = int $R->param('birth');
  }
  if ($birth < $ymin or $birth > $year) {
    push @err, sprintf(gettext("Your year of birth must be between %d and %d."), $ymin, $year);
    $err{birth} = 1;
  }

  if (!$R->param('p1') or !$R->param('p2')) {
    push @err, gettext('You must provide a password for your account.');
    $err{password} = 1;
  } else {
    if ($R->param('p1') ne $R->param('p2')) {
      push @err, gettext("Those passwords don't match.
Enter your password twice to insure that it is recorded accurately.");
      $err{password} = 1;
    } elsif (length $R->param('p1') < 3) {
      push @err, gettext("Your password is too short.");
      $err{password} = 1;
    } elsif (length $R->param('p1') > PASSWORD_LEN) {
      push @err, gettext("Your password is too long.");
      $err{password} = 1;
    }
  }

#  eval_captcha(\@err, \%err);

  if (@err) {
    $err{msg} = join_remedy(@err);
    return $class->handshake(\%err);
  }

  my $passwd = substr $R->param('p1'), 0, PASSWORD_LEN;

  $DB->begin_work;
  $DB->do("LOCK TABLE member IN SHARE ROW EXCLUSIVE MODE");

  # Short circuit to login if registration is already complete.
  my $q1 = $DB->prepare_cached('select member_id, member_password, member_activated,'.
get_member_properties().'from member_tz m where member_email = ?');
  $q1->execute($email);
  my $u = $q1->fetchrow_hashref;
  $q1->finish;
  if ($u) {
    $DB->commit;
    if (Crypt($passwd, $u->{member_password}) eq $u->{member_password}) {
      return (activation_problem($u) or do {
	setup_login($u);
	'PPX::Member'->front();
      });
    } else {
      return $class->reset_passwd({ from_new => $email });
    }
  }

  my $ins = $DB->prepare_cached('insert into member (member_password,
member_email, member_birth) values (?,?,?)');
  $ins->execute(Crypt($passwd), $email, $birth);

#  activity_notification($email, "[Orwell] new account $email ",
#			'referred by '.member('session_http_referer'));

  my $id = currval('member', 'member_id');
  my $link = get_email_link(EMAIL_ACTIVATE, $id);

  $DB->commit;

  my $body = sprintf(gettext('
We have received your request to create a new account.  To confirm
that your e-mail address is correct, visit this web page:

  %s

If you do not wish to create an account for Peer Editing Exchange
then simply disregard this message.

Please reply to this e-mail if you have any other questions.'), $link);

  sendmail_no_msgstamp($email, gettext('Account activation instructions'),
		       $body);

  $class->await_activation($email);
}

sub await_activation {
  my ($class, $email) = @_;

  banner(gettext('Acknowledgment'));

  my $m1 =
    sprintf(gettext('A confirmation e-mail has been sent to %s.'), $email);
  my $m2 = sprintf(gettext('If you do not receive any e-mail from us
within one or two hours then contact %s for assistance.'), EMAIL_FROM);

  print(br.div({ class => 'bigmsg' },
	       p(txt($m1)),
	       p(txt(gettext("Upon receiving this e-mail message, follow the
enclosed instructions to activate your account."))),
	       p(txt($m2))));
}

sub email {
  my ($class) = @_;
  
  my $token = $R->param('token');

  my $q1 = $DB->prepare_cached("select * from email
WHERE email_token = ? LIMIT 1");
  $q1->execute($token);
  my ($e) = $q1->fetchrow_hashref;
  $q1->finish;

  if (!$e) {
    banner(gettext('Link Error'));
    print(br,
	  div({ class => 'bigmsg' },
	      p(txt(gettext('Please check the URL and try again.')))
	   ));
    return;
  }

  if (member()) {
    if ($e->{member_id} != member()) {
      $DB->do('delete from session where session_id = ?',
	      {}, member('session_id'));
      set_member(member_id => 0);
    }
  }

  my $purpose = $e->{email_purpose};
  if ($purpose eq EMAIL_ACTIVATE)   { activate($class, $e) }
  elsif ($purpose eq EMAIL_ADDRESS) { email_confirm($class, $e) }
  elsif ($purpose eq EMAIL_LOGIN)   { quicklogin($class, $e) }
  else {
    confess $purpose
  }
}

sub activate {
  my ($class, $e) = @_;

  my $id = $e->{member_id};
  $DB->do("update member set member_activated='y' where member_id = ?",{},$id);

  my $q1 = $DB->prepare_cached('select member_email,'.
get_member_properties().'from member_tz m where member_id = ?');
  $q1->execute($id);
  my $u = $q1->fetchrow_hashref;
  $q1->finish;
  $u->{member_id} = $id;
  $u->{member_activated} = 'y';

  setup_login($u);
  'PPX::Member'->front();
}

sub quicklogin {
  my ($class, $e) = @_;

  my $id = $e->{member_id};
  my $q1 = $DB->prepare_cached('select member_email, member_activated,'.
get_member_properties().'from member_tz m where member_id = ?');
  $q1->execute($id);
  my $u = $q1->fetchrow_hashref;
  $q1->finish;
  $u->{member_id} = $id;

  activation_problem($u) or do {
    setup_login($u);

    my $payload = $e->{email_payload};
    my @kv = split /;/, $payload;

    my $leaf = 'front';

    for my $kv (@kv) {
      my ($k,$v) = split /=/, $kv;
      if ($k eq 'leaf') {
	$leaf = $v;
      } else {
	$R->param($k, $v);
      }
    }

    banner_announce(div({ class => 'bigmsg' },
		       'Welcome, '.txt(member('member_email'))));

    $class = determine_member_class($u);
    $class->$leaf();
  };
}

sub email_confirm {
  my ($class, $e) = @_;

  my $id = $e->{member_id};
  my $new = $e->{email_payload};
  $DB->do(<<SQL, {}, $new, $id);
UPDATE member SET member_email=?, member_bouncing='f'
WHERE member_id = ?
SQL

  banner('E-mail Confirmation');
  print(br,
	div({ class => 'bigmsg' },
	    p(txt("Your e-mail address is changed to <$new>.")),
	    p(leaf('OK', 'front'))
	   ));
}

package PPX::Member::Impl;

sub logout {
  $DB->do('delete from session where session_id = ?',
	  {}, member('session_id'));
  set_member(member_id => 0);

  PPX::Guest->front({ msg => txt(gettext('You are logged out.')) });
}

sub chg_email {
  my ($class, $err) = @_;
  $err ||= {};

  banner('Change E-mail');

  print(p(txt('Changing your e-mail address requires confirmation from your
new address to ensure accuracy and prevent misuse.')),
	p(txt('Submitting the form below will mail a confirmation
URL to the new e-mail address. Once you receive the e-mail then follow
the enclosed instructions to complete the change.')));

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(start_form('await_email_confirm'),
	tag('table'),
	Tr(td(lsty($err,'email', 'Email:')),
	   td(hskip(4)),
	   td(textfield('email', size => 40, maxlength=>EMAIL_LEN))),
	gat('table'),
	div({align => 'center'}, submit('Change')),
	gat('form'));
}

sub await_email_confirm {
  my ($class) = @_;
  my $email = extract_email($R->param('email'));

  if (!$email) {
    my %err;
    $err{email} = 1;
    $err{msg} = 'Enter your new e-mail address.';
    return $class->chg_email(\%err);
  }

  my $old_email = member('member_email');
  if ($email eq $old_email) {
    my %err;
    $err{email} = 1;
    $err{msg} = "Your e-mail address is already $email.";
    return $class->chg_email(\%err);
  }

  my $col = $DB->prepare_cached('select member_id from member where member_email = ?');
  $col->execute($email);
  my ($id) = $col->fetchrow_array;
  $col->finish;
  if (defined $id) {
    # Do we want to leak membership information like this?
    my %err;
    $err{email} = 1;
    $err{msg} = "The address '$email' belongs to another member.  Enter _your_ new e-mail address.";
    return $class->chg_email(\%err);
  }

  my $link = get_email_link(EMAIL_ADDRESS, member(), $email);

  sendmail(member(), $email, 'Confirm New E-mail Address', '
We have received a request to change your e-mail address from

  '.$old_email.'
to
  '.$email.'

To confirm that you want your e-mail address changed, visit this
web page:

  '.$link.'

If you do not wish to change your e-mail address, simply disregard this
message.

Please reply to this e-mail if you have any other questions.');

  banner('E-mail Acknowledgment');

  print(br.div({ class => 'bigmsg' },
	    p(txt("An e-mail message has been sent to '$email'.")),
	    p(txt("Follow the enclosed instructions to complete
your change of address request.")),
	    p(leaf('OK', 'front'))
	   ));
}

sub chg_passwd {
  my ($class, $err) = @_;
  $err ||= {};

  banner('Change Password');

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(p(txt(cheap_password_hint())),
	start_form('update_passwd'),
	tag('table', cellspacing => 8),
	Tr(join(td(hskip(4)),
		td(lsty($err,'password', 'New Password:')),
		td(password('p1', maxlength => PASSWORD_LEN)),
		td({ rowspan => 2},
		   txt('Enter your new password twice to insure
that it is recorded accurately.')))),
	Tr(join(td(),
		td(lsty($err,'password', 'Re-type Password:')),
		td(password('p2', maxlength => PASSWORD_LEN)))),
	gat('table'),
	p({align => 'center'}, submit('Change')),
	gat('form'));
}

sub update_passwd {
  my ($class) = @_;
  my (%err, @err);

  if (!$R->param('p1') and !$R->param('p2')) {
    push @err, 'You did not type any password.';
    $err{password} = 1;
  } elsif ($R->param('p1') ne $R->param('p2')) {
    push @err, "Those passwords don't match.  Try again with more
careful typing.";
    $err{password} = 1;
  } elsif (length $R->param('p1') < 3) {
    push @err, "Your password is too short.";
    $err{password} = 1;
  } elsif (length $R->param('p1') > PASSWORD_LEN) {
    push @err, "Your password is too long.";
    $err{password} = 1;
  } else {
    $DB->do('update member set member_password = ? where member_id = ?', {},
	    Crypt($R->param('p1')), member());

    banner('Password Changed');
    print(br.div({ class => 'bigmsg' },
		 p(txt('Your password has been updated.')),
		 p(leaf('Continue', 'front'))
		));
    return;
  }

  $err{msg} = join_remedy(@err);
  return $class->chg_passwd(\%err);
}

package PPX::Admin::Impl;

sub switch_user {
  my ($to) = $R->param('to');
  if ($to) {
    my $got = int $DB->do(<<SQL, {}, $to, member('session_id'));
UPDATE SESSION SET member_id =
  (SELECT member_id FROM member WHERE member_email = ?)
WHERE session_id = ?
SQL
    if ($got) {
      my @q = (leaf => 'front', session_id => member('session_id'),
	       session_salt => member('session_salt'));
      my @p;
      while (@q) {
	my $k = shift @q;
	my $v = shift @q;
	push @p, $k.'='.CGI::Simple::Util::escape($v);
      }
      print $R->redirect(CGI_URL.'?'.join(';', @p));
    }
  }

  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_submitted AT TIME ZONE ? AS manuscript_submitted,
  manuscript_title,
  (SELECT member_email FROM member WHERE member_id = m.member_id) AS author,
  (SELECT member_email FROM member WHERE member_id = a.member_id) AS reviewer,
  a.assignment_for
FROM manuscript m
LEFT JOIN assignment a ON (a.manuscript_id = m.manuscript_id)
WHERE manuscript_submitted IS NOT NULL
ORDER BY manuscript_submitted DESC
LIMIT 25
SQL
  $q1->execute(member('member_tz'));

  banner('Switch User');

  print(tag('table'),
	Tr(join(th(hskip(2)),
		th('Submitted'),
		th('Title'),
		th('Author'),
		th('Reviewer'),
		th('How'))));
  while (defined(my $m = $q1->fetchrow_hashref)) {
    my $for = do {
      if (!defined $m->{assignment_for}) { '-' }
      elsif ($m->{assignment_for} == ASSIGN_FOR_EDITING)   { 'editing' }
      elsif ($m->{assignment_for} == ASSIGN_FOR_SCREENING) { 'screen' }
      elsif ($m->{assignment_for} == ASSIGN_FOR_VETTING)   { 'vetting' }
      else { '?' }
    };
    print(Tr(join(td(),
		  td(substr($m->{manuscript_submitted}, 0, 10)),
		  td(txt(truncate_to_chars($m->{manuscript_title}, 40))),
		  td(txt($m->{author})),
		  td(txt($m->{reviewer} || '-')),
		  td($for),
		 )));
  }
  print(gat('table'));

  print(p('Switch to who?'),
	start_form('switch_user'),
	tag('table'),
	Tr(td('To:'), td(hskip(4)),
	   td(textfield('to'))),
	gat('table'),
	p({align => 'center'}, submit('Switch')),
	gat('form'));
}

sub bounce_email {
  banner('Bounce Toggle');

  my $u1 = $DB->prepare(<<SQL);
UPDATE member SET member_bouncing = NOT member_bouncing
WHERE member_email = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT member_bouncing FROM member WHERE member_email = ?
SQL

  for my $x (1..4) {
    my $email = $R->param("addr$x");
    next if !$email;
    if (int $u1->execute($email)) {
      $q1->execute($email);
      my ($yes) = $q1->fetchrow_array;
      print(p(txt($email.' : '. ($yes?'bouncing' : 'ok'))));
    }
  }

  print(p('Which e-mail addresses?'),
	start_form('bounce_email'),
	p(textfield('addr1')),
	p(textfield('addr2')),
	p(textfield('addr3')),
	p(textfield('addr4')),
	p({align => 'center'},
	  leaf('Go Back', 'front').hskip(4).submit('Toggle')),
	gat('form'));
}

1;
