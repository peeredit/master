# Copyright (C) 2003, 2004, 2005, 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::Dispatch;

use Apache2::RequestRec (); # for $r->content_type  #CGI#
use APR::Table ();
use Apache2::ServerRec ();
use Apache2::ServerUtil ();
use Apache2::RequestUtil ();
use Apache2::Response ();
use Apache2::RequestIO ();  # for print             #CGI#
use Apache2::Const -compile => qw(DECLINED OK REDIRECT);
use Apache2::SubProcess ();

use Fatal qw(mkdir);
use Locale::gettext;
use POSIX qw(setlocale LC_MESSAGES);
textdomain('ppx');

use Carp;  # CGI::Carp not working, why?
use CGI::Simple qw(-newstyle_urls -no_debug -upload -carp);
use CGI::Simple::Util;
use Mail::Address;
use Time::HiRes qw(gettimeofday tv_interval);

use PPX::Config;
use PPX::Util;

require PPX::Registry;

BEGIN {
  require PPX::XML;
  if (SYSTEM_TEST()) {
    PPX::XML::initialize(0,1);
  } else {
    PPX::XML::initialize(1,0);
  }
  Carp->import('verbose');
}

use PPX::HTML;

$CGI::Simple::POST_MAX = 1024 * 1024;

sub dispatch {
  my $pkg;
  set_member(undef);

  my $q_session = $DB->prepare_cached(<<SQL);
SELECT member_id, session_http_referer, session_payload,
  now() - session_mtime > '60 min' as expired
FROM session
WHERE session_id = ? and session_salt = ? limit 1
SQL
  my $u_session = $DB->prepare_cached(<<SQL);
UPDATE session SET session_mtime = now() WHERE session_id = ?
SQL
  
  my $u;
  my $expired;
  {
    # Check the cookie first. Why? Go through the cases:
    #
    # 1. There is a valid session in the cookie and in the URL and
    # there are different. Do we really want to allow two different
    # sessions in the same browser? No, better to switch to the
    # cookie's session.
    # 2. There is only a valid session in the cookie. Might as well
    # check the cookie first.
    # 3. There is only a valid session in the URL.
    #
    # Also, we need to detect whether a session belongs to a cookie
    # or the URL to determine whether to expire after a period of
    # inactivity. If the same session is available from the cookie
    # and from the URL then we should still consider it the cookie's
    # session.
    #
    for my $session ([1, split(/:/, ($R->cookie('PPX_SESSION') or '0:0'))],
		     [0, $R->param('session_id'), $R->param('session_salt')]) {
      my ($cookie, $session_id, $session_salt) = @$session;
      {
	no warnings 'numeric';
	$session_id = int ($session_id || 0);
	$session_salt = int ($session_salt || 0);
      }
      next if !$session_id;
      $q_session->execute($session_id, $session_salt);
      $u = $q_session->fetchrow_hashref;
      $q_session->finish;
      if ($u) {
	if (!$cookie and $u->{expired}) {
	  $expired = 1;
	  undef $u;
	  next;
	}
	$u->{session_id} = $session_id;
	$u->{session_salt} = $session_salt;
	last;
      }
    }
  }

  if ($expired) {
    $pkg = 'PPX::Expired';
  } elsif ($u) {
    $u_session->execute($u->{session_id});
    $pkg = 'PPX::Guest';
    set_member($u);
    if ($u->{member_id}) {
      my $q_member = $DB->prepare_cached('SELECT'.
get_member_properties().'FROM member_tz m WHERE member_id = ?');
      $q_member->execute($u->{member_id});
      my $e = $q_member->fetchrow_hashref();
      $q_member->finish;
      for my $k (keys %$e) {
	$u->{$k} = $e->{$k};
      }
      if ($u->{stale}) {
	my $u_hitstamp = $DB->prepare_cached('update member
set member_hitstamp = now() where member_id = ?');
	$u_hitstamp->execute(member());
      }
      $pkg = determine_member_class($u)
    }
  }

  if (!$pkg) {
    my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO session (session_http_referer) values (?)
SQL
    $i1->execute(substr($ENV{HTTP_REFERER} || '?',0,1000));
    my $session_id = currval('session', 'session_id');
    my $q1 = $DB->prepare_cached('SELECT session_salt from session where session_id = ?');
    $q1->execute($session_id);
    my ($session_salt) = $q1->fetchrow_array;
    $q1->finish;
    set_session_id({ session_id => $session_id,
		     session_salt => $session_salt });
    $pkg = 'PPX::Guest';
  }

  my $err = $R->cgi_error;
  if ($err) {
    banner('Error');
    if ($err =~ /Request entity too large/) {
      print div({ class => 'error' },
		txt('You probably attempted to upload a file which is
too big.'));
    } elsif ($err =~ /No REQUEST_METHOD/) {
      # ignore broken robots
    } else {
      warn $err;
      print div({ class => 'error' }, txt($err));
    }
    return 1;
  }

  my $leaf = $R->param('leaf') || 'front';
  $leaf =~ tr/[a-z0-9_]//cd;

  if ($leaf eq 'isa' or $leaf eq 'can' or !$pkg->can($leaf)) {
    $leaf = 'front';
  }

  eval {
    my $t0 = [gettimeofday];
    set_member('leaf', $leaf)
      if member();
    $pkg->$leaf();
    my $elapsed = tv_interval($t0);
    my $i2 = $DB->prepare_cached(<<SQL);
INSERT INTO page_log (page_log_session_id, page_log_member_id,
  page_log_leaf, page_log_prev_leaf, page_log_elapsed)
VALUES (?,?,?,?,?)
SQL
    $i2->execute(member('session_id'), member(), $leaf,
		 scalar($R->param('previous_leaf')), $elapsed.' sec')
      if defined member('session_id');
  };
  if ($@) {
    warn $DBI::lasth->{Statement}
      if $DBI::lasth->{Statement};
    warn "$pkg->$leaf: $@";
    warn $R->Dump;
    $DB->rollback;
    eval {
      banner(gettext('Error'));
      print div({class => 'error'},
		txt(gettext('An internal error occurred. Please try again.')));
    };
    0;
  } else {
    1;
  }
}

sub handler {
  my $r = shift;

  if (!defined $BaseDir) {
    $BaseDir = $r->dir_config('PPXBaseDir');
    die '$BaseDir not set' if !$BaseDir;
    bindtextdomain('ppx', "$BaseDir/locale");
    register_library_images("$BaseDir/htdocs/img");
  }
  
  # This is not needed anymore because static files are not
  # under, URL-wise, our URL. This is more efficient. Why
  # not arrange for static files to avoid triggering perl?
  #
  # only handle the directory specified in the apache config.
  # return declined to let Apache serve regular files.
  #return Apache2::Const::DECLINED
  #  unless $r->filename eq $BaseDir;

  connect_to_database()
    if !$DB;

  binmode STDOUT, ':utf8';

  $HasBanner = 0;

#  warn "dumping header";
#  $r->headers_in->do(sub { warn $_[0].'->'.$_[1] });

  $R = CGI::Simple->new($r);

  setlocale(LC_MESSAGES, $R->param('lang') || 'C');

  my $ok;
  for my $retry (1..2) {
    $ok = eval { dispatch() };
    if ($@) {
      if ($@ =~ /no connection to the server/ or
	  $@ =~ /server closed the connection unexpectedly/) {
	connect_to_database();
	next;
      }
      warn $DBI::lasth->{Statement}
	if $DBI::lasth->{Statement};
      $@ =~ s/\n+$//;
      warn $@;
    }
    last;
  }

  if ($HasBanner) {
    eval {
      print_postscript();
      print(gat('body', 'html'));
      PPX::HTML::end();
    };
    if ($@) {
      warn $DBI::lasth->{Statement}
	if $DBI::lasth->{Statement};
      warn $@;
      warn $R->Dump;
    }
  }

  Apache2::Const::OK;    #CGI#
}

1;
