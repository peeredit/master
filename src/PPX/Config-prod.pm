# Copyright (C) 2003, 2004, 2005, 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::Config;

# performance
use constant SYSTEM_TEST => 0;

# database
require DBI;
use constant DB_BASE => 'ppx';	# must be created already
use constant DB_HOST => 'localhost';
#use constant DB_PORT => 3306;
use constant DB_USER => 'www-data';
use constant DB_PASS => '';

# cron jobs
use constant SITE => 'http://peeredit.us';
use constant CGI_URL => '/run';
use constant MAILDIR => '/home/ppx/Maildir';

# e-mail
use constant EMAIL_INJECT => '/usr/sbin/sendmail';
use constant EMAIL_FROM => 'pex@peeredit.us';
use constant EMAIL_TO_OVERRIDE => '';

# important constants
use constant EMAIL_LEN => 100;
use constant PASSWORD_LEN => 20;
use constant NOVALUE => '- : -';

use constant PROG_ODT2TXT => '/usr/bin/odt2txt';
use constant PROG_WDIFF => '/usr/bin/wdiff';
use constant PROG_DIFF => '/usr/bin/diff';

use constant ETC_URL => '/style/';

our ($DB, $R, $BaseDir);  # $R is the current apache request object

sub connect_to_database {
  $DB = DBI->connect('DBI:Pg:dbname='.DB_BASE, '', '',
		     { RaiseError => 1, PrintError => 0, AutoCommit => 1,
		       pg_enable_utf8 => 1 });
#  $DB->do(q[SET CLIENT_ENCODING TO 'UTF8']); #unnecessary?
}

our @ISA = qw(Exporter);
our @EXPORT = qw(SYSTEM_TEST
                 DB_BASE DB_HOST DB_USER DB_PASS
		 SITE CGI_URL MAILDIR
		 EMAIL_FROM EMAIL_TO_OVERRIDE EMAIL_INJECT
                 NOVALUE EMAIL_LEN PASSWORD_LEN
		 ETC_URL PROG_ODT2TXT PROG_WDIFF PROG_DIFF
		 $DB $R $BaseDir
connect_to_database
                 );
1;
