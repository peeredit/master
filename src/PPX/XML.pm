########################################################################
# Writer.pm - write an XML document.
# Copyright (c) 1999 by Megginson Technologies.
# Copyright (c) 2003, 2004, 2007 Joshua Nathaniel Pritikin (heavily modified)
# No warranty.  Commercial and non-commercial use freely permitted.
#
########################################################################

package PPX::XML;

require 5.004;

use strict;
use warnings;
use Carp;
use IO::Handle;

our $VERSION = "0.4.1";
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(xmlDecl doctype endDoc pi comment
		    startTag emptyTag endTag characters dataElement);


########################################################################
# Constructor.
########################################################################

#
# Public constructor.
#
# This actually does most of the work of the module: it defines closures
# for all of the real processing, and selects the appropriate closures
# to use based on the value of the UNSAFE parameter.  The actual methods
# are just stubs.
#

our $initialized;

sub initialize {
  my ($unsafe, $newlines) = @_;

  return
    if $initialized;
  $initialized = 1;

  my $nl = '';
  if ($newlines) {
    $nl = "\n";
  }

  my @elementStack = ();
  my $elementLevel = 0;

  #
  # Private method to show attributes.
  #
  my $showAttributes = sub {
    my $atts = $_[0];
    my $i = 1;
    my $buf = '';
    while ($atts->[$i]) {
      my $aname = $atts->[$i++];
      my $value = _escapeLiteral($atts->[$i++]);
      $buf .= " $aname=\"$value\""
    }
    $buf;
  };

				# Method implementations: the SAFE_
				# versions perform error checking
				# and then call the regular ones.
  my $end = sub {};

  my $SAFE_end = sub {
    my @copy = @elementStack;
    @elementStack = ();
    $elementLevel = 0;
    if (@copy) {
      croak("Document ended with unmatched start tag(s): @copy");
    }
  };

  my $pi = sub {
    my ($target, $data) = (@_);
    my $buf = '';
    if ($data) {
      $buf .= "<?$target $data?>";
    } else {
      $buf .= "<?$target?>";
    }
    if ($elementLevel == 0) {
      $buf .= "\n";
    }
    $buf;
  };

  my $SAFE_pi = sub {
    my ($name, $data) = (@_);
    if ($name =~ /xml/i) {
      carp("Processing instruction target begins with 'xml'");
    }

    if ($name =~ /\?\>/ || $data =~ /\?\>/) {
      croak("Processing instruction may not contain '?>'");
    } else {
      &{$pi};
    }
  };

  my $comment = sub {
    my $data = $_[0];
    my $buf = '';
    $buf .= "<!-- $data -->";
    if ($elementLevel == 0) {
      $buf .= "\n";
    }
    $buf;
  };

  my $SAFE_comment = sub {
    my $data = $_[0];
    if ($data =~ /--/) {
      carp("Interoperability problem: \"--\" in comment text");
    }

    if ($data =~ /-->/) {
      croak("Comment may not contain '-->'");
    } else {
      &{$comment};
    }
  };

  my $startTag = sub {
    my $name = $_[0];
    my $buf = '';
    $elementLevel++;
    push @elementStack, $name;
    $buf .= "<$name";
    $buf .= &{$showAttributes}(\@_);
    $buf .= ">";
    $buf;
  };

  my $SAFE_startTag = sub {
    my $name = $_[0];

    _checkAttributes(\@_);

    &{$startTag};
  };

  my $emptyTag = sub {
    my $name = $_[0];
    my $buf = '';
    $buf .= "<$name";
    $buf .= &{$showAttributes}(\@_);
    $buf .= "/>";
    $buf;
  };

  my $characters = sub {
    my $data = $_[0];
    if ($data =~ /[\&\<\>]/) {
      $data =~ s/\&/\&amp\;/g;
      $data =~ s/\</\&lt\;/g;
      $data =~ s/\>/\&gt\;/g;
    }
    $data
  };

  my $SAFE_emptyTag = sub {
    my $name = $_[0];

    _checkAttributes(\@_);

    &{$emptyTag};
  };

  my $endTag = sub {
    my $name = $_[0];
    my $currentName = pop @elementStack;
    $name = $currentName unless $name;
    $elementLevel--;
    my $buf = '';
    $buf .= "</$name>";
    $buf;
  };

  my $SAFE_endTag = sub {
    my $name = $_[0];
    my $oldName = $elementStack[$#elementStack];
    if ($elementLevel <= 0) {
      warn("End tag \"$name\" does not close any open element");
    } elsif ($name && ($name ne $oldName)) {
      carp("Attempt to end element \"$oldName\" with \"$name\" tag");
    } else {
      &{$endTag};
    }
  };

  my $SAFE_characters = sub {
    my $data = $_[0];
    if ($data =~ /(\<\/|\/\>)/) {
      carp "Possible XML tag embedded in |$data|";
    }
    &$characters($data);
  };

				# Assign the correct closures based on
				# the UNSAFE parameter
  if ($unsafe) {
    *endDoc = $end;
    *pi = $pi;
    *comment = $comment;
    *startTag = $startTag;
    *emptyTag = $emptyTag;
    *endTag = $endTag;
    *characters = $characters;
  } else {
    *endDoc = $SAFE_end;
    *pi = $SAFE_pi;
    *comment = $SAFE_comment;
    *startTag = $SAFE_startTag;
    *emptyTag = $SAFE_emptyTag;
    *endTag = $SAFE_endTag;
    *characters = $SAFE_characters;
  }
}



########################################################################
# Public methods
########################################################################

sub xmlDecl {
  my ($encoding, $standalone) = (@_);
  if ($standalone && $standalone ne 'no') {
    $standalone = 'yes';
  }
  $encoding = "UTF-8" unless $encoding;
  my $buf='';
  $buf .= "<?xml version=\"1.0\"";
  if ($encoding) {
    $buf .= " encoding=\"$encoding\"";
  }
  if ($standalone) {
    $buf .= " standalone=\"$standalone\"";
  }
  $buf .= "?>\n";
  $buf;
}

sub doctype {
  my ($name, $publicId, $systemId) = (@_);
  my $buf = '';
  $buf .= "<!DOCTYPE $name";
  if ($publicId) {
    $buf .= " PUBLIC \"$publicId\" \"$systemId\"";
  } elsif ($systemId) {
    $buf .= " SYSTEM \"$systemId\"";
  }
  $buf .= ">\n";
  $buf;
}

#
# Write a simple data element.
#
sub dataElement {
  my ($name, $data, %atts) = (@_);
  join('',
       startTag($name, %atts),
       characters($data),
       endTag($name))
}


########################################################################
# Private functions.
########################################################################

#
# Private: check for duplicate attributes.
# Note - this starts at $_[1], because $_[0] is assumed to be an
# element name.
#
sub _checkAttributes {
  my %anames;
  my $i = 1;
  while ($_[$i]) {
    my $name = $_[$i];
    $i += 2;
    if ($anames{$name}) {
      croak("Two attributes named \"$name\"");
    } else {
      $anames{$name} = 1;
    }
  }
}

#
# Private: escape an attribute value literal.
#
sub _escapeLiteral {
  my $data = $_[0];
  Carp::cluck 'undef' if !defined $data;
  if ($data =~ /[\&\<\>\"]/) {
    $data =~ s/\&/\&amp\;/g;
    $data =~ s/\</\&lt\;/g;
    $data =~ s/\>/\&gt\;/g;
    $data =~ s/\"/\&quot\;/g;
  }
  return $data;
}

1;
__END__

########################################################################
# POD Documentation
########################################################################

=head1 NAME

XML::Writer - Perl extension for writing XML documents.

=head1 SYNOPSIS

  use XML::Writer;
  use IO;

  my $output = new IO::File(">output.xml");

  my $writer = new XML::Writer(OUTPUT => $output);
  $writer->startTag("greeting", 
                    "class" => "simple");
  $writer->characters("Hello, world!");
  $writer->endTag("greeting");
  $writer->end();
  $output->close();


=head1 DESCRIPTION

XML::Writer is a helper module for Perl programs that write an XML
document.  The module handles all escaping for attribute values and
character data and constructs different types of markup, such as tags,
comments, and processing instructions.

By default, the module performs several well-formedness checks to
catch errors during output.  This behaviour can be extremely useful
during development and debugging, but it can be turned off for
production-grade code.

Additional support is available for a simplified data mode with no
mixed content: newlines are automatically inserted around elements and
elements can optionally be indented based as their nesting level.


=head1 METHODS

=head2 Writing XML

=over 4

=item new([$params])

Create a new XML::Writer object:

  my $writer = new XML::Writer(OUTPUT => $output, NEWLINES => 1);

Arguments are an anonymous hash array of parameters:

=over 4

=item OUTPUT

An object blessed into IO::Handle or one of its subclasses (such as
IO::File); if this parameter is not present, the module will write to
standard output.

=item NEWLINES

A true or false value; if this parameter is present and its value is
true, then the module will insert an extra newline before the closing
delimiter of start, end, and empty tags to guarantee that the document
does not end up as a single, long line.  If the paramter is not
present, the module will not insert the newlines.

=item UNSAFE

A true or false value; if this parameter is present and its value is
true, then the module will skip most well-formedness error checking.
If the parameter is not present, the module will perform the
well-formedness error checking by default.  Turn off error checking at
your own risk!

=item DATA_MODE

A true or false value; if this parameter is present and its value is
true, then the module will enter a special data mode, inserting
newlines automatically around elements and (unless UNSAFE is also
specified) reporting an error if any element has both characters and
elements as content.

=item DATA_INDENT

A numeric value; if this parameter is present, it represents the
indent step for elements in data mode (it will be ignored when not in
data mode).

=back

=item end()

Finish creating an XML document.  This method will check that the
document has exactly one document element, and that all start tags are
closed:

  $writer->end();

=item xmlDecl([$encoding, $standalone])

Add an XML declaration to the beginning of an XML document.  The
version will always be "1.0".  If you provide a non-null encoding or
standalone argument, its value will appear in the declaration (and
non-null value for standalone except 'no' will automatically be
converted to 'yes').

  $writer->xmlDecl("UTF-8");

=item doctype($name, [$publicId, $systemId])

Add a DOCTYPE declaration to an XML document.  The declaration must
appear before the beginning of the root element.  If you provide a
publicId, you must provide a systemId as well, but you may provide
just a system ID.

  $writer->doctype("html");

=item comment($text)

Add a comment to an XML document.  If the comment appears outside the
document element (either before the first start tag or after the last
end tag), the module will add a carriage return after it to improve
readability:

  $writer->comment("This is a comment");

=item pi($target [, $data])

Add a processing instruction to an XML document:

  $writer->pi('xml-stylesheet', 'href="style.css" type="text/css"');

If the processing instruction appears outside the document element
(either before the first start tag or after the last end tag), the
module will add a carriage return after it to improve readability.

The $target argument must be a single XML name.  If you provide the
$data argument, the module will insert its contents following the
$target argument, separated by a single space.

=item startTag($name [, $aname1 => $value1, ...])

Add a start tag to an XML document.  Any arguments after the element
name are assumed to be name/value pairs for attributes: the module
will escape all '&', '<', '>', and '"' characters in the attribute
values using the predefined XML entities:

  $writer->startTag('doc', 'version' => '1.0',
                           'status' => 'draft',
                           'topic' => 'AT&T');

All start tags must eventually have matching end tags.

=item emptyTag($name [, $aname1 => $value1, ...])

Add an empty tag to an XML document.  Any arguments after the element
name are assumed to be name/value pairs for attributes (see startTag()
for details):

  $writer->emptyTag('img', 'src' => 'portrait.jpg',
                           'alt' => 'Portrait of Emma.');

=item endTag([$name])

Add an end tag to an XML document.  The end tag must match the closest
open start tag, and there must be a matching and properly-nested end
tag for every start tag:

  $writer->endTag('doc');

If the $name argument is omitted, then the module will automatically
supply the name of the currently open element:

  $writer->startTag('p');
  $writer->endTag();

=item dataElement($name, $data [, $aname1 => $value1, ...])

Print an entire element containing only character data.  This is
equivalent to

  $writer->startTag($name [, $aname1 => $value1, ...]);
  $writer->characters($data);
  $writer->endTag($name);

=item characters($data)

Add character data to an XML document.  All '<', '>', and '&'
characters in the $data argument will automatically be escaped using
the predefined XML entities:

  $writer->characters("Here is the formula: ");
  $writer->characters("a < 100 && a > 5");

You may invoke this method only within the document element
(i.e. after the first start tag and before the last end tag).

In data mode, you must not use this method to add whitespace between
elements.


=back

=head1 ERROR REPORTING

With the default settings, the XML::Writer module can detect several
basic XML well-formedness errors:

=over 4

=item *

Lack of a (top-level) document element, or multiple document elements.

=item *

Unclosed start tags.

=item *

Misplaced delimiters in the contents of processing instructions or
comments.

=item *

Misplaced or duplicate XML declaration(s).

=item *

Misplaced or duplicate DOCTYPE declaration(s).

=item *

Mismatch between the document type name in the DOCTYPE declaration and
the name of the document element.

=item *

Mismatched start and end tags.

=item *

Attempts to insert character data outside the document element.

=item *

Duplicate attributes with the same name.

=back

During Namespace processing, the module can detect the following
additional errors:

=over 4

=item *

Attempts to use PI targets or element or attribute names containing a
colon.

=item *

Attempts to use attributes with names beginning "xmlns".

=back

To ensure full error detection, a program must also invoke the end
method when it has finished writing a document:

  $writer->startTag('greeting');
  $writer->characters("Hello, world!");
  $writer->endTag('greeting');
  $writer->end();

This error reporting can catch many hidden bugs in Perl programs that
create XML documents; however, if necessary, it can be turned off by
providing an UNSAFE parameter:

  my $writer = new XML::Writer(OUTPUT => $output, UNSAFE => 1);


=head1 AUTHOR

David Megginson, david@megginson.com

=head1 SEE ALSO

XML::Parser

=cut
