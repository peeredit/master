# Copyright (C) 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::DB;
use Carp;
use PPX::Config;

use constant EMAIL_ACTIVATE => 1;
use constant EMAIL_ADDRESS  => 2;
use constant EMAIL_LOGIN    => 3;

use constant ASSIGN_FOR_EDITING   => 1;
use constant ASSIGN_FOR_SCREENING => 2;
use constant ASSIGN_FOR_VETTING   => 3;

use constant FLAG_OK              => 1;
use constant FLAG_SPAM            => 2;
use constant FLAG_PROHIBITED      => 3;
use constant FLAG_MISCATEGORIZED  => 4;
use constant FLAG_CONFUSED        => 5;

use constant LATE_STYLE_DEFAULT   => 1;
use constant LATE_STYLE_NONE      => 2;
use constant MINIMUM_LATE_HOURS   => 24;

use constant HOURS_BEFORE_SCREENING_ESCALATION => 0; # increase later
use constant HOURS_UNTIL_DEFAULT_SCREENING => 12;
use constant SCREENINGS_PER_MANUSCRIPT => 3;

use constant MEMBER_INITIAL_BALANCE => 2500;

use constant ADULT_AGE            => 18;

use constant WRITING_LEVEL =>
  ['beginner',
   '9th to 12th grade level',
   'college level',
   'graduate or professional'];

use constant NUM_WRITING_LEVELS => @{ &WRITING_LEVEL };

use constant REVISION_QUALITY =>
  ['sophistry, bluffing, detrimental',
   'not thorough, some sparse corrections',
   'helpful, many good suggestions',
   'meticulous, insightful, outstanding'];

use constant MAX_REVISION_QUALITY => $#{ &REVISION_QUALITY };
use constant MIN_SCREENER_QUALITY => 2;
use constant MIN_VETTING_QUALITY => 2;
use constant MIN_ESCROW_SENIORS => 50;

use constant SENIORITY_THRESHOLD => 2;

use constant MAX_DAYS_ALLOWANCE => 7;
use constant MAX_DAYS_EXTENSION => 5;

use constant MAX_VETTING_ASSIGNMENTS => 10;

use constant MARGIN_REQUIREMENT => .2;

use constant MAX_LATE_DIVISION => 4;

use constant CRITERIA_UNIT_KEYWORD    => 0;
use constant CRITERIA_UNIT_HOURS      => 1;
use constant CRITERIA_UNIT_WLEVEL     => 2;
use constant CRITERIA_UNIT_WC         => 3;
use constant CRITERIA_UNIT_ADULT      => 4;
use constant NUM_CRITERIA_UNIT_TYPES  => 5;

use constant LANG_ENGLISH             => 1;

our @ISA = qw(Exporter);
our @EXPORT = qw(EMAIL_ACTIVATE EMAIL_ADDRESS EMAIL_LOGIN
FLAG_OK FLAG_SPAM FLAG_PROHIBITED FLAG_MISCATEGORIZED FLAG_CONFUSED
WRITING_LEVEL REVISION_QUALITY MAX_REVISION_QUALITY NUM_WRITING_LEVELS
MARGIN_REQUIREMENT SCREENINGS_PER_MANUSCRIPT ADULT_AGE
cancel_overdue_screenings SENIORITY_THRESHOLD MAX_DAYS_ALLOWANCE
MAX_DAYS_EXTENSION

CRITERIA_UNIT_KEYWORD CRITERIA_UNIT_HOURS CRITERIA_UNIT_WLEVEL
CRITERIA_UNIT_WC CRITERIA_UNIT_ADULT HOURS_BEFORE_SCREENING_ESCALATION
NUM_CRITERIA_UNIT_TYPES MAX_LATE_DIVISION LANG_ENGLISH
MAX_VETTING_ASSIGNMENTS MIN_VETTING_QUALITY MIN_SCREENER_QUALITY
ASSIGN_FOR_EDITING ASSIGN_FOR_SCREENING ASSIGN_FOR_VETTING
MIN_ESCROW_SENIORS MEMBER_INITIAL_BALANCE HOURS_UNTIL_DEFAULT_SCREENING
LATE_STYLE_DEFAULT LATE_STYLE_NONE MINIMUM_LATE_HOURS);

sub cancel_overdue_screenings {
  my ($member_id) = @_;
  my @arg;
  my $restrict = '';
  if ($member_id) {
    $restrict = 'AND member_id = ?';
    push @arg, $member_id;
  }
  my $u1 = $DB->prepare_cached(<<"SQL");
UPDATE assignment
SET assignment_cancelled = now(), assignment_archived = 't',
  assignment_value = -assignment_value
WHERE assignment_archived = 'f' AND
  assignment_ctime + interval '1 hour' < now() AND
  assignment_for = ? $restrict
SQL
  $u1->execute(ASSIGN_FOR_SCREENING, @arg);
}

1;
