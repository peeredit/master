# Copyright (C) 2003, 2004, 2005, 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::HTML;
use PPX::XML qw(xmlDecl doctype endDoc pi comment
		startTag emptyTag endTag characters dataElement);
use Carp;

our @ISA = qw(Exporter);
our @EXPORT = qw(tag gat elem txt emp hskip label
		 br hr register_library_images libimg img line_break
		 textfield radio checkbox hidden submit upload
		 textarea password popup_menu span gen_id script
		 p pre sup Sub li tt b i a div big small th td Tr blockquote
		);

my $ID = 0;

sub start {
  my $lang = 'en-US';
  $ID = 0;
  join('',
#       xmlDecl('utf-8'),
       doctype('html',
		   '-//W3C//DTD XHTML 1.0 Strict//EN',
		   'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'),
       tag('html', xmlns => 'http://www.w3.org/1999/xhtml',
	   lang => $lang, 'xml:lang' => $lang));
}
sub end {
  endDoc();
}

# shorten names
BEGIN {
  *tag  = \&startTag;
  *elem = \&dataElement;
  *txt  = \&characters;
  *emp  = \&emptyTag;
}

sub gat { join '', map { endTag($_) } @_ }

# ----------------------------------

sub gen_id { 'id' . ++$ID }

sub hskip {
  my $z;
  if (@_) { $z = shift; } else { $z = 1; }
  my $buf='';
  for (my $x=0; $x < $z; $x++) {
    $buf .= '&nbsp;';
  }
  $buf;
}

sub br   { emp 'br', @_ }
sub hr   { emp 'hr', @_ }

our %UsingImage;
sub register_library_images($) {
  my ($dir) = @_;
  open my $fh, "$dir/INDEX" or die "open $dir/INDEX: $!";
  while (defined(my $l = <$fh>)) {
    $l =~ s/\#.*$//;
    $l =~ s/^\s+//;
    $l =~ s/\s+$//;
    next if !$l;
    my @l = split /\t/, $l;
    my ($im, $alt, $permission) = @l;
    $UsingImage{$im} = [$alt, $permission];
  }
}

sub libimg {
  my $file = shift;
  if ($file =~ m,/,) {
    carp "libimg $file ignored";
    return '';
  }
  my $info = $UsingImage{$file};
  if (!$info) {
    carp "libimg $file missing";
    return '';
  }
  my ($alt, $perm) = @$info;
  emp('img', src => 'img/' . $file, alt => $alt, @_);
}

sub img {
  my $uri = shift;
  my $alt = shift;
  emp 'img', src => $uri, alt => $alt, @_;
}

# make tags work like CGI.pm tags
sub define_tag($) {
  no strict 'refs';
  my ($orig_tag) = @_;
  my $tag = lc $orig_tag;
  *{ $orig_tag } = sub {
    my @attr;
    if (ref $_[0] eq 'HASH') {
      @attr = %{ shift @_ };
    }
    if (!ref $_[0]) {
      for my $str (@_) {
	if (!defined $str) {
	  carp "Undefined string in $orig_tag";
	  $str = 'UnDeF';  # make it visible
	}
      }
      join('', tag($tag, @attr), @_, gat($tag))
    } elsif (ref $_[0] eq 'ARRAY') {
      my $buf = '';
      my $l = shift @_;
      for my $i (@$l) {
	$buf .= tag($tag, @attr) . $i . gat($tag);
      }
      $buf;
    }
  };
}

BEGIN {
  for my $tag (qw(p pre sup Sub li tt b i a div span big small th td
		  Tr blockquote)) {
    define_tag $tag
  }
}

sub textfield {
  my $name = shift;
  emp 'input', type=>'text', name=>$name, @_;
}

sub script {
  my $body = shift;
  tag('script', type=>'text/javascript', defer=>1). $body . gat('script')
}

sub label {
  my $attr = {};
  $attr = shift
    if ref $_[0];
  my ($id, $html) = @_;
  if ($html =~ /^\s/) {
    carp "Label starts with whitespace $html";
  }
  join('', tag('label', for => $id, %$attr), $html, gat('label'))
}

sub upload {
  my $name = shift;
  emp('input', type=>'file', name=>$name, size=>16)
}

sub radio {
  my ($value, $group_name, $checked, @rest) = @_;
  push @rest, (checked => 1)
    if $checked;
  my $has_id;
  for (my $x=0; $x < @rest; $x += 2) {
    if ($rest[$x] eq 'id') {
      $has_id = 1;
      last;
    }
  }
  if (!$has_id) {
    push @rest, id => $value;
  }
  emp 'input', type=>'radio', name=>$group_name, value=>$value, @rest;
}

sub checkbox {
  my $name = shift;
  my $checked = shift;
  my @c;
  @c = (checked => 1)
    if $checked;
  emp 'input', type=>'checkbox', id=>$name, name=>$name, @c, @_;
}

sub password {
  my $name = shift;
  emp 'input', type=>'password', name=>$name, @_;
}

sub popup_menu {
  my $name = shift;
  my $val = shift;
  my $default = shift;
  my $map = shift;

  my @op;
  for my $v (@$val) {
    my $val = $map? $map->{$v} : $v;
    my @sel;
    @sel = (selected=>'yes')
      if (defined $default and $default eq $val);
    push @op, tag('option', value=>$val, @sel) . $v . gat('option');
  }

  tag('select', name => $name, @_) . join('', @op) . gat('select')
}

sub textarea {
  my ($name, $default, $rows, $cols, @rest) = @_;
  $default = '' if !defined $default;
  join('',
       tag('textarea', name=>$name, rows=>$rows, cols=>$cols, @rest),
       $default,
       gat('textarea'));
}

{
  my $middot = '&middot;';
  sub submit {
    my $val = shift;
    # Different browsers handle the name differently so don't
    # bother with a name.
    join('', tag('button', type => 'submit', @_),
	 "$middot $val $middot", gat('button'));
  }
}

sub hidden {
  my $buf ='';
  while (@_) {
    my $k = shift;
    my $v = shift;
    $buf .= emp 'input', type=>'hidden', name=>$k, value=>$v;
  }
  $buf;
}

sub line_break($) {
  my ($s) = @_;
  my $buf = '';
  my @line = split /(\n)/, $s;
  for my $l (@line) {
    if ($l eq "\n") {
      $buf .= br;
    } else {
      $buf .= txt $l;
    }
  }
  $buf;
}

1;
