# Copyright (C) 2007 Joshua Nathaniel Pritikin

use strict;
use warnings;
package PPX::Guest::Impl;
require XML::RSS;

sub escape_xml { # move to library? TODO
  $_[0] =~ s/\&/\&amp\;/g;
  $_[0] =~ s/\</\&lt\;/g;
  $_[0] =~ s/\>/\&gt\;/g;
  $_[0]
}

sub feed {
  my $member_id = $R->param('member_id');
  my $criteria_id = $R->param('criteria_id');

  my $q1 = query_available_manuscripts($member_id, $criteria_id);
  return if !$q1;

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT criteria_name FROM criteria WHERE criteria_id = ?
SQL
  $q2->execute($criteria_id);
  my ($name) = $q2->fetchrow_array;
  $q2->finish;

  my $rss = XML::RSS->new(version => '2.0');
  $rss->channel(title          => 'PEX: '.$name,
		link           => SITE,
		language       => 'en',  # should grab criteria TODO
		description    => 'This newsfeed lists
manuscripts which are currently available for editing.
Since manuscripts are reviewed by at most five people,
you must act quickly if you wish secure an assignment.',
#		rating         => '(PICS-1.1 "http://www.classify.org/safesurf/" 1 r (SS~~000 1))',
		webMaster      => EMAIL_FROM
	       );
  while (defined(my $m = $q1->fetchrow_hashref)) {
    # This link doesn't actually work unless you have a valid cookie. TODO
    my $guid = (SITE.CGI_URL.'?'.
		join(';', 'leaf=show_manuscript', "id=$m->{manuscript_id}",
		     'for=edit', "member_id=$member_id"));

    my $abstract = '';
    $abstract = p('Abstract: '.$m->{manuscript_abstract})
      if $m->{manuscript_abstract};

    my $context = '';
    $context = p('Context and Instructions: '.$m->{manuscript_context})
      if $m->{manuscript_context};

    my $body = join('',
		    $context,
		    p('Genre: '.$m->{manuscript_genre}),
		    p('Words: '.$m->{manuscript_wc}),
		    p('Audience: '.WRITING_LEVEL->[$m->{manuscript_writing}-1]),
		    $abstract);

    $rss->add_item(permaLink => $guid,
		   title => escape_xml($m->{manuscript_title}),
		   description => escape_xml($body),
		   pubDate => $m->{pubDate});
  }

  print $rss->as_string;
}

package PPX::Member::Impl;

sub find_manuscript {
  my ($class) = @_;
  my $criteria_id = $R->param('criteria_id');
  my $form = $R->param('form');
  my $want_email;
  if ($form and $form eq 'find_manuscript') {
    $want_email = $R->param('email')? 't':'f';
  }
  if ($criteria_id) {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE member SET criteria_id = ? WHERE member_id = ?
SQL
    eval {
      $u1->execute($criteria_id, member());
    };
    if (!$@ or $@ =~ /violates foreign key/) {
      #ok
    } else { die }
    if (!$@ and defined $want_email) {
      my $u2 = $DB->prepare_cached(<<SQL);
UPDATE member_criteria SET member_criteria_email = ?
WHERE member_id = ? and criteria_id = ?
SQL
      $u2->execute($want_email, member(), $criteria_id);
    }
  } else {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE member
SET criteria_id = (SELECT criteria_id FROM member_criteria
                   WHERE member_id = ? ORDER BY random() LIMIT 1)
WHERE member_id = ? AND (criteria_id IS NULL OR
  criteria_id NOT IN (SELECT criteria_id FROM member_criteria
                      WHERE member_id = ?))
SQL
    if (int $u1->execute(member(), member(), member())) {
      my $q1 = $DB->prepare_cached(<<SQL);
SELECT criteria_id FROM member WHERE member_id = ?
SQL
      $q1->execute(member());
      set_member('criteria_id', $q1->fetchrow_array);
      $q1->finish;
    }
    $criteria_id = member('criteria_id');
  }

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT c.criteria_id, criteria_name,
  (SELECT m.member_criteria_email FROM member_criteria m
   WHERE m.member_id = ? AND m.criteria_id = c.criteria_id)
FROM criteria c
WHERE c.criteria_id IN
  (SELECT m.criteria_id FROM member_criteria m WHERE m.member_id = ?)
ORDER BY criteria_name, c.criteria_id
SQL
  $q2->execute(member(), member());
  my @cri;
  my %cmap;
  my %email;
  while (my ($k,$v,$email) = $q2->fetchrow_array) {
    push @cri, $v;
    $cmap{$v}=$k;
    $criteria_id = $k if !defined $criteria_id;
    $email{$k} = 1
      if $email;
  }

  if (!defined $criteria_id) {
    return $class->choose_criteria();
  }

  my $q1 = query_available_manuscripts(member(), $criteria_id);
  if (!$q1) {
    return $class->choose_criteria();
  }
  my @m;
  while (defined(my $m = $q1->fetchrow_hashref)) { push @m, $m }

  banner('Find Manuscript');

  my @feed = ('leaf=feed', 'member_id='.member(), 'criteria_id='.$criteria_id);

  my @new;
  if (member('member_admin')) {
    push @new, td(leaf('New', 'edit_criteria'));
  }

  print(start_form('find_manuscript'),
	hidden(form=>'find_manuscript'),
	tag('table', style => 'float: right; font-size: 70%; padding: .5ex; border: thin dashed pink; background: yellow;'),
	Tr(td(checkbox('email', $email{$criteria_id},
		       onChange => 'event.target.form.submit()')),
	   td(label('email', 'E-mail Notification'))),
	Tr(td(tag('a', type=>'application/rss+xml',
		  href=> SITE.CGI_URL.'?'.join(';',@feed)),
	      libimg('feed.png'), gat('a')),
	   td(a({ href => '/static/faq.html' }, 'Newsfeed'))),
	gat('table'),
	tag('table'),
	tag('tr'),
	join(td(hskip(4)),
	     td(leaf('Choose', 'choose_criteria')),
	     td(popup_menu('criteria_id', \@cri, $criteria_id, \%cmap,
			   onChange => 'event.target.form.submit()')),
	     td(leaf('Edit', 'edit_criteria', id => $criteria_id)),
	     @new),
	gat('tr','table'),
	gat('form'),
       );

  if (!@m) {
    print(p('There are no manuscripts in progress at this time.'),
	  p(leaf('Register Manuscript', 'reg_manuscript').
	 ' Please register a manuscript.'),
	  p({align => 'center'}, leaf('Go Back', 'front')));
    return;
  }

  print(p(txt('The following manuscripts were recently submitted:')),
	tag('table'),
	Tr(th('Genre'),th(hskip(2)),
	   th('Title'), th(hskip(2)),
	   th('Words'), th(hskip(2)),
	   th('Due'), th(hskip(2)),
	   th('Open')));
  for my $m (@m) {
# TODO need icon to represent vulgar
    my $title = leaf_plain(truncate_to_chars($m->{manuscript_title}, 60),
			  'show_manuscript', id=>$m->{manuscript_id},
			   for=>'edit');
    print(Tr(td(truncate_to_chars($m->{manuscript_genre}, 20)),td(),
	     td($title),td(),
	     td({ align=>'right'}, $m->{manuscript_wc}),td(),
	     td({ align=>'right'},
		format_hours($m->{manuscript_hours_remain})), td(),
	     td({ align=>'right'}, $m->{manuscript_open}? 'Yes' : 'No')));
  }
  print(gat('table'),
	p({align => 'center'}, leaf('Go Back', 'front')));
}

sub choose_criteria {
  my ($class, $from) = @_;

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT criteria_id FROM member_criteria WHERE member_id = ?
SQL
  $q1->execute(member());
  my %like;
  while (my ($id) = $q1->fetchrow_array) { $like{$id} = 1 }

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT criteria_id, criteria_name
FROM criteria
WHERE member_id = ?
ORDER BY criteria_hits_per_hour, criteria_name
SQL
  $q2->execute(member());
  my @priv;
  while (defined(my $c = $q2->fetchrow_hashref)) { push @priv, $c }

  my $safe = '';
  $safe = 'AND criteria_kids_safe'
    if !member('member_adult');

  my $q3 = $DB->prepare_cached(<<SQL);
SELECT criteria_id, criteria_name
FROM criteria
WHERE member_id IS NULL $safe
ORDER BY criteria_name
SQL
  $q3->execute;
  my @pub;
  while (defined(my $c = $q3->fetchrow_hashref)) { push @pub, $c }

  if (!@priv and !@pub) {   # database completely empty
    return $class->edit_criteria();
  }

  banner('Choose Criteria');

  print(p('Which manuscript criteria are of interest to you? '),
	start_form('record_criteria_subset'),
	hidden($from? (from => $from):()));

  my @feed = ('leaf=feed', 'member_id='.member());
  my @lists = (\@priv, \@pub);
  for my $section (0..1) {
    my $cri = $lists[$section];
    next if !@$cri;

    if ($section == 0) {
      print(div({class => 'section'}, gettext('Your Criteria')));
    } else {
      print(div({class => 'section'}, gettext('Preset Criteria')));
    }

    print(tag('table'));

    my $col = int((@$cri + 2) / 3);
    for (my $x=0; $x < $col; $x++) {
      print(tag('tr'));
      my @td;
      for my $c ($cri->[$x*3], $cri->[$x*3 + 1], $cri->[$x*3 + 2]) {
	next if !defined $c;
	my $id = $c->{criteria_id};
	push @td,
	  td(checkbox($id, $like{$id}).' ',
	     label($id, txt($c->{criteria_name})).' ',
	     tag('a', type=>'application/rss+xml',
		 href=> SITE.CGI_URL.'?'.join(';',@feed, "criteria_id=$id")),
	     libimg('feed.png'),
	     gat('a'));
      }
      print(join(td(hskip(4)), @td),
	    gat('tr'));
    }
    print(gat('table'));
  }
  print(p(small(a({ href => '/static/faq.html' }, 'Note that each criteria is
also available as an RSS feed.'))),
	p({ align => 'center'}, submit(gettext('Update'))),
	gat('form'));
}

sub record_criteria_subset {
  my ($class) = @_;

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT criteria_id FROM member_criteria WHERE member_id = ?
SQL
  my $d1 = $DB->prepare_cached(<<SQL);
DELETE FROM member_criteria WHERE member_id = ? AND criteria_id = ?
SQL
  my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO member_criteria (member_id, criteria_id) values (?,?)
SQL
  $q1->execute(member());
  my %cur;
  while (my ($id) = $q1->fetchrow_array) { $cur{$id} = 1 }

  my $count=0;
  $DB->begin_work;
  for my $id ($R->param) {
    if ($id =~ /^\d+$/ ) {
      if ($cur{$id}) {
	$count += 1;
	delete $cur{$id};
      } else {
	$count += int $i1->execute(member(), $id);
      }
    }
  }
  for my $id (keys %cur) { $d1->execute(member(), $id) }
  $DB->commit;

  set_member(has_chosen_criteria => 1)
    if $count;

  if (($R->param('from') || '') eq 'front') {
    $class->front();
  } else {
    $class->find_manuscript();
  }
}

sub query_my_criteria {
  my ($id) = @_;
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT member_id, criteria_name
FROM criteria
WHERE criteria_id = ? AND (member_id = ? OR member_id IS NULL)
SQL
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT criteria_unit_type, criteria_unit_rank, criteria_unit_data1
FROM criteria_unit
WHERE criteria_id = ?
ORDER BY criteria_unit_rank, criteria_unit_type
SQL
  $q1->execute($id, member());
  my $c = $q1->fetchrow_hashref;
  $q1->finish;
  $c->{criteria_id} = $id;
  if (!$c->{member_id} and !member('member_admin')) {
    my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO criteria (language_id, member_id, criteria_name) values (?,?,?)
SQL
    my $i2 = $DB->prepare_cached(<<SQL);
INSERT INTO criteria_unit (criteria_id, criteria_unit_type,
  criteria_unit_rank, criteria_unit_data1, criteria_unit_data2)
SELECT ?, criteria_unit_type, criteria_unit_rank, criteria_unit_data1,
  criteria_unit_data2
FROM criteria_unit
WHERE criteria_id = ?
SQL
    my $i3 = $DB->prepare_cached(<<SQL);
INSERT INTO member_criteria (member_id, criteria_id) values (?,?)
SQL
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE member SET criteria_id = ? WHERE member_id = ?
SQL
    $c->{criteria_name} = "my $c->{criteria_name}";
    $DB->begin_work;
    $i1->execute(LANG_ENGLISH, member(), $c->{criteria_name});
    my $new_id = currval('criteria', 'criteria_id');
    $i2->execute($new_id, $id);
    $i3->execute(member(), $new_id);
    $u1->execute($new_id, member());
    $DB->commit;
    $c->{criteria_id} = $new_id;
  }
  my @u;
  if ($c) {
    $q2->execute($id);
    while (defined(my $u = $q2->fetchrow_hashref)) { push @u, $u };
  }
  ($c, \@u);
}

sub print_criteria_unit_editor {
  my ($err, $type, $rank, $data) = @_;

  my $inf = "\x{221e}";
  my $rank_html =
    join('', tag('td'),
	 popup_menu("r$type", [1 .. NUM_CRITERIA_UNIT_TYPES, $inf],
		    defined $rank? $rank : $inf),
	 gat('td'));

  print(tag('tr', style => 'padding-top: 2ex; padding-bottom: 2ex'));
  my @cr = (onChange => "change_rank(event.target.form.r$type)");
  if ($type == CRITERIA_UNIT_KEYWORD) {
    print(td('Keywords (in genre, title, or abstract):',br,
	     textarea('keyword', $data||'', 5, 65, @cr),
	     br.hskip(1)),
	     td(), $rank_html);
  } elsif ($type == CRITERIA_UNIT_HOURS) {
    print(td(lsty($err,'hours','Minimum hours remaining:').' '.
	     textfield('hours', value => format_hours($data || 1), @cr),
	     br.hskip(1)),
	  td(), $rank_html);
  } elsif ($type == CRITERIA_UNIT_WLEVEL) {
    $data = 0 if !defined $data;
    print(td('Intended audience:',br,
	     join(br, (map { radio($_,'skill', $data == $_, @cr).' '.
			       label($_,WRITING_LEVEL->[$_ - 1]) }
		       reverse 1 .. NUM_WRITING_LEVELS)),
	     br.hskip(1)),
	  td(), $rank_html);
  } elsif ($type == CRITERIA_UNIT_WC) {
    print(td(lsty($err,'wc','Word count:').' '.
	     textfield('wc', value => $data || '', @cr),br,
	     small(lsty($err, 'wc', 'You may specify a range like
1000-2500. If you only want a minimum then use 1000-. Similarly,
you can use -5000 for a maximum of 5000 words.')),
	     br.hskip(1)),
	  td(), $rank_html);
  } elsif ($type == CRITERIA_UNIT_ADULT) {
    if (member('member_adult')) {
      $data = 0 if !defined $data;
      print(td(radio('a0', 'adult', $data == 0, @cr),
	       label('a0', 'Only manuscripts which are safe for all ages'),br,
	       radio('a1', 'adult', $data == 1, @cr),
	       label('a1', 'Only adult manuscripts'),
	      br.hskip(1)),
	    td(), $rank_html);
    }
  } else {
    warn "Unknown criteria unit type $type";
  }
  print(gat('tr'));
}

sub edit_criteria {
  my ($class, $err) = @_;
  $err ||= {};

  banner('Edit Criteria');

  my ($c,$unit) = ({}, []);
  my $criteria_html = '';
  my $id = $R->param('id');
  if ($id) {
    ($c,$unit) = query_my_criteria($id);
    $id = $c->{criteria_id}; #might have changed
    $criteria_html = hidden(id => $id);
  }

  if ($err->{msg}) {
    print(div({ class => 'error' }, $err->{msg}))
  }

  print(start_form('update_criteria'), $criteria_html,
	p(gettext('Language'),': ',
	  popup_menu('lang', ['English'], 'English'),
	  ' ',a({ href => '/static/faq.html' }, '?')),
	p(gettext('Name'),': ',
	  textfield('name', value => $c->{criteria_name}||'')));

  if (member('member_admin')) {
    print(p(checkbox('public', !defined $c->{member_id}),
	   ' '.label('public', 'Public')));
  }

  print(p("Search criteria are built from a collection of constraints.
Constraints of first rank are manditory. A manuscript
will be excluded if it doesn't satisfy a constraint of first rank.
Second and lower rank constraints determine the order in which
manuscripts are sorted.
To annul a constraint, set the rank to \x{221e} (infinity)."),
	tag('table'),
	Tr(join(th(hskip(2)),
	       th('Constraint'),
	       th('Rank'))));

  print script('
function change_rank(obj) {
  if (obj.selectedIndex == obj.length - 1) {
    obj.selectedIndex = 1;
  }
}');

  my %pending;
  for (my $u=0; $u < NUM_CRITERIA_UNIT_TYPES; $u++) { $pending{$u} = 1 }
  for my $u (@$unit) {
    delete $pending{ $u->{criteria_unit_type} };
    print_criteria_unit_editor($err,
			       $u->{criteria_unit_type},
			       $u->{criteria_unit_rank},
			       $u->{criteria_unit_data1});
  }
  for my $type (sort keys %pending) {
    print_criteria_unit_editor($err, $type);
  }

  print(gat('table'),
	tag('p', align => 'center'));

  print(leaf('Delete', 'delete_criteria', id=>$id,
	     owner => $c->{member_id}||0),
	hskip(4))
    if ($id and (($c->{member_id}||0) == member() or
		 member('member_admin')));
  print(submit(gettext('Update')),
	gat('p'),
	gat('form'));
}

sub delete_criteria {
  my ($class) = @_;

  my ($id) = $R->param('id');
  my ($owner) = $R->param('owner');
  my $sql='';
  my @arg;
  if (!member('member_admin')) {
    $sql = 'and member_id = ?';
    @arg = (member());
  }
  my $d1 = $DB->prepare_cached(<<"SQL");
delete from criteria where criteria_id = ? $sql
SQL
  $d1->execute($id, @arg);

  $class->find_manuscript();
}

sub update_criteria {
  my ($class) = @_;
  my (%err, @msg);

  my ($id) = $R->param('id');
  if (!$id) {
    my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO criteria (language_id, member_id) values (?,?)
SQL
    my $i2 = $DB->prepare_cached(<<SQL);
INSERT INTO member_criteria (member_id, criteria_id) values (?,?)
SQL
    $DB->begin_work;
    $i1->execute(LANG_ENGLISH, member());
    $id = currval('criteria', 'criteria_id');
    $i2->execute(member(), $id);
    $DB->commit;
    $R->param(id => $id);
  }

  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE criteria SET criteria_name = ?, criteria_kids_safe = ?, member_id = ?
WHERE criteria_id = ? AND (member_id IS NULL or member_id = ?)
SQL
  my $d1 = $DB->prepare_cached(<<SQL);
DELETE FROM criteria_unit WHERE criteria_id = ?
SQL
  my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO criteria_unit (criteria_id, criteria_unit_type,
  criteria_unit_rank, criteria_unit_data1, criteria_unit_data2)
VALUES (?,?,?,?,?)
SQL

  my $safe = 1;

  $DB->begin_work;
  $d1->execute($id);
  for (my $u=0; $u < NUM_CRITERIA_UNIT_TYPES; $u++) {
    my $rank = $R->param("r$u");
    if ($u == CRITERIA_UNIT_ADULT and !member('member_adult')) {
      #ok
    } else {
      next if ($rank !~ /^\d+$/);
    }
    if ($u == CRITERIA_UNIT_KEYWORD) {
      my $raw = $R->param('keyword');
      $i1->execute($id, $u, $rank, $raw, keyword_to_query($raw));
    } elsif ($u == CRITERIA_UNIT_HOURS) {
      my $hours = parse_duration($R->param('hours'));
      if ($hours < 1) {
	$err{hours} = 1;
	push @msg, "You can't require less than 1 hour.";
      } else {
	$i1->execute($id, $u, $rank, $hours, undef);
      }
    } elsif ($u == CRITERIA_UNIT_WLEVEL) {
      $i1->execute($id, $u, $rank, $R->param('skill'), undef);
    } elsif ($u == CRITERIA_UNIT_WC) {
      my $wc = $R->param('wc');
      $wc =~ s/\s//g;
      if ($wc =~ /^(\d+)?\-(\d+)?$/) {
	my $min = $1;
	my $max = $2;
	if ($min and $max and $min >= $max) {
	  $err{wc}=1;
	  push @msg, 'The minimum word-count must be less than the maximum word-count.'
	} elsif (!$min and !$max) {
	  next;
	}
	$i1->execute($id, $u, $rank, $wc, undef);
      } else {
	$err{wc}=1;
	push @msg, 'Please follow the instructions for specifying
the word-count constraint.';
      }
    } elsif ($u == CRITERIA_UNIT_ADULT) {
      if (member('member_adult')) {
	my $adult = int($R->param('adult') eq 'a1');
	$safe = int(!$adult and $rank == 1);
	$i1->execute($id, $u, $rank, $adult, undef);
      }
    } else {
      warn "Unknown criteria unit type $u";
    }
  }

  my $owner = member();
  if (member('member_admin') and $R->param('public')) {
    $owner = undef;
  }
  $u1->execute(lc substr($R->param('name'), 0, 40), $safe, $owner,
	       $id, member());
  $DB->commit;

  if (@msg) {
    $err{msg} = join_remedy(@msg);
    return $class->edit_criteria(\%err);
  }

  $class->find_manuscript();
}

sub criteria_unsub {
  my ($class) = @_;

  my $id = $R->param('criteria_id');
  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE member_criteria SET member_criteria_email = 'f'
WHERE member_id = ? and criteria_id = ?
SQL
  $u1->execute(member(), $id);

  $class->find_manuscript();
}

1;
