use strict;
use warnings;

package PPX::Test;
use Carp;
use PPX::Config;
use Time::HiRes qw(usleep);
require Exporter;
require LWP;
require HTML::Parser;
require HTML::TreeBuilder;
require HTML::Form;
require HTTP::Request;
require Mail::Box::Manager;
require HTTP::Cookies;

use constant LOG_ERR   => 0;
use constant LOG_WARN  => 1;
use constant LOG_DEBUG => 2;

our @ISA = 'Exporter';
our @EXPORT = qw($form0 $matter $matter_id $Jar
                 clean_mailbox recv_mail find_link click_link talk
                 email_to_id drop_member extract_by_id extract_div_error
time_shift get_link_targets create_manuscript fix_member_criteria
                 $LogLevel LOG_ERR LOG_WARN LOG_DEBUG Log);

our $LogLevel = LOG_ERR;
our ($tree, @link, @form, $form0, $matter, $matter_id);
our $Jar = HTTP::Cookies->new(); #hide_cookie2 => 1 ?

package HTML::Element;

sub extract_text {
  my ($h) = @_;
  my @t;
  for my $e ($h, $h->descendants) {
    for my $k ($e->content_list) {
      next if ref $k;
      push @t, $k;
    }
  }
  join "\n", @t;
}

package PPX::Test;

sub Log($$) {
  my ($l, $msg) = @_;
  return if $l > $LogLevel;
  $msg .= "\n" if $msg !~ /\n$/;
  print $msg;
}

sub Log_($$) {
  my ($l, $msg) = @_;
  return if $l > $LogLevel;
  print $msg;
}

sub _scan_mailbox {
  my ($box, $to, $subject) = @_;
  my @m;
  for my $msg ($box->messages) {
    my @to = $msg->to;
    next if @to != 1;
    next if $to[0]->address ne $to;
    next if $msg->subject !~ /$subject/i;
    push @m, $msg;
  }
  @m;
}

sub clean_mailbox {
  my ($to, $subject) = @_;
  my $mgr    = Mail::Box::Manager->new;
  my $box = $mgr->open(folder => MAILDIR,
		       access => 'rw',
		       remove_when_empty => 0);
  my @m = _scan_mailbox($box, $to, $subject);
  if (@m) {
    $_->delete for @m;
    $box->write;
  }
}

sub recv_mail {
  my ($to, $subject) = @_;
  my $body;
  my $mgr    = Mail::Box::Manager->new;
  Log_ LOG_DEBUG, "Waiting for email '$subject' ";
  while (1) {
    my $box = $mgr->open(folder => MAILDIR,
			  access => 'rw',
			  remove_when_empty => 0);
    my @m = _scan_mailbox($box, $to, $subject);
    if (!@m) {
      $box->close;
      usleep(300);
      Log_ LOG_DEBUG, '.';
      next;
    }
    $body = $m[0]->decoded;
    Log(LOG_ERR, "Multiple matches for $subject: ".
	join(', ', map { $_->subject } @m))
      if @m > 1;
    $_->delete for @m;
    $box->write;
    Log LOG_DEBUG, " ok";
    last;
  }
  $body;
}

sub extract_links {
  my ($resp) = @_;
  my @l;
  my $grab;
  my $label;
  my $start = sub {
    my ($tag, $attr) = @_;
    if ($tag eq 'a') {
      my $href = $attr->{href};
      return if substr($href,0,length CGI_URL) ne CGI_URL;
      die $grab if defined $grab;
      $grab = SITE . $href;
      $label = '';
    }
  };
  my $text = sub {
    return if !$grab;
    my ($name) = @_;
    $label .= $name;
  };
  my $end = sub {
    my ($tag) = @_;
    if ($tag eq '/a' and $grab) {
#      warn "$label ".join(',',map {sprintf('%x', ord($_))} split / */, $label);
      $label =~ s/\x{a0}/ /g;
      push @l, [$label, $grab];
      undef $grab;
    }
  };
  my $p = HTML::Parser->new(start_h => [$start, 'tag, attr'],
			    text_h => [$text, 'dtext'],
			    end_h => [$end, 'tag']);
  $p->report_tags('a');
  $p->parse($resp->content);
  $p->eof;
#  for my $l (@l) { warn $l->[0] }
  @l
}

sub find_link {
  my ($pat) = @_;
  my @m;
  for my $l (@link) {
    push @m, $l if $l->[0] =~ /\Q$pat\E/i;
  }
  if (!@m) {
    return;
  } elsif (@m > 1) {
    my %u;
    for my $l (@m) { $u{ $l->[1] } = 1 }
    if (keys %u > 1) {
      confess "$pat matches ".join(', ', map { $_->[0] } @m)
    }
  }
  $m[0][1];
}

sub time_shift {
  my ($hours) = @_;
  $DB->begin_work;
  # Optimize these into single sql statements
  for my $col (qw(manuscript_submitted manuscript_withdrawn
		  manuscript_approved manuscript_notified)) {
    $DB->do("update manuscript set $col = $col + interval '1 hour' * ?
where $col IS NOT NULL", {}, -$hours);
  }
  for my $col (qw(assignment_ctime assignment_revtime[1] assignment_revtime[2]
		  assignment_cancelled)) {
    $DB->do("update assignment set $col = $col + interval '1 hour' * ?
where $col IS NOT NULL", {}, -$hours);
  }
  $DB->commit;
}

sub click_link {
  my $link = find_link($_[0]);
  croak "Can't find $_[0]" if !$link;
  HTTP::Request->new(GET => $link);
}

sub extract_by_id {
  my ($id) = @_;
  my $subtree = $tree->look_down(id=>$id);
  if ($subtree) {
    return $subtree->extract_text;
  } else { undef }
}

sub extract_div_error {
  my $subtree = $tree->look_down(_tag=>'div', class=>'error');
  if ($subtree) {
    return $subtree->extract_text;
  } else { undef }
}

#use Data::Dumper;
{
  my $ua = LWP::UserAgent->new;
  sub talk {
    my ($req) = @_;
    if (!ref $req) {
      $req = HTTP::Request->new(GET => $req);
    }
    $Jar->add_cookie_header($req);
#    warn Dumper($req);
    my $resp = $ua->request($req);
    $Jar->extract_cookies($resp);
#    warn Dumper($resp);
    if (!$resp->is_success) {
      die $resp->error_as_HTML;
    }
    $tree->delete if $tree;
    $tree = HTML::TreeBuilder->new_from_content($resp->content);
    $tree->elementify;
    @form = HTML::Form->parse($resp->content, SITE);
    $form0 = $form[0];
    @link = extract_links($resp);

    my $subtree = $tree->look_down(_tag => 'div', class=>'matter');
    if ($subtree) {
      $matter_id = $subtree->attr('id');
      $matter = $subtree->extract_text;
    } else {
      $matter = $tree->extract_text;
      if (length $matter <= 1) {
	Log LOG_ERR, 'No response, server crashed?'
      } else {
	Log LOG_ERR, "div class=>matter not found in $matter";
      }
    }
  }
}

sub get_link_targets {
  my %uniq;
  for my $to (map { $_->[1] } @link) { $uniq{$to} = 1 }
  keys %uniq;
}

sub email_to_id {
  my ($email) = @_;
  my ($id) = $DB->selectrow_array('select member_id from member where member_email = ?', {}, $email);
  $id;
}

sub drop_member {
  my ($id) = @_;
  return if !$id;
  $DB->do('delete from member where member_id = ?', {}, $id);
}

$|=1;
open STDOUT, ">&STDERR" or die "Can't dup STDERR: $!";
binmode STDOUT, ':utf8';

$SIG{__DIE__} = sub {
  die @_ if $^S || !defined $^S;
  my $pos = '';
  if ($form0) {
    $form0->dump;
  }
  if ($matter) {
    $matter =~ s/ +/ /g;
    $matter =~ s/\n+/\n/g;
    $pos .= "[$matter_id]: ".substr($matter,0,1024)."\n";
  }
  die "$pos@_";
};

sub create_manuscript {
  my (%opt) = @_;

  talk click_link('register manuscript');

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title FROM manuscript
WHERE manuscript_title LIKE 'Manuscript%'
ORDER BY manuscript_title DESC
LIMIT 1;
SQL

  $q1->execute;
  my ($prev) = $q1->fetchrow_array;
  $q1->finish;

  my $title = do {
    if ($prev and $prev =~ /^Manuscript (\d+)\s/) {
      sprintf 'Manuscript %04d Title', 1+$1;
    } else {
      'Manuscript 0001 Title';
    }
  };

  $form0->value('title', $title);
  $form0->value('genre', 'incoherent rambling');
  $form0->value('abstract', $opt{abstract});
  $form0->value('due', '4h');          # 6criteria depends on this number
  $form0->value('spellok', 'on');
  my @m = $opt{file}? @{ $opt{file} } : ('1.txt');
  $form0->value('manuscript', 'documents/'.$m[int rand @m]);
  $form0->value('min_rev', 1 + int rand 4);
  my @level = $form0->find_input('level')->possible_values;
  $form0->value('level', $level[int rand @level]);
#  $form0->value('level', 2);
  talk $form0->click;
  talk click_link('continue');
  if ($matter_id eq 'Member::front') {
    my $err = extract_div_error();
    if ($err =~ /margin requirement/) {
      # ok
    } else { warn $err }
  } elsif ($matter_id eq 'Member::any_pending_work') {
    $opt{clear}->();
  } else {
    talk $form0->click;
  }
}

sub fix_member_criteria {
  my ($email) = @_;
  $DB->do('delete from member_criteria where member_id = ?', {}, email_to_id($email));
  $DB->do('insert into member_criteria (member_id, criteria_id, member_criteria_email) values (?,
(select criteria_id from criteria where criteria_name = ?), ?)', {}, email_to_id($email), 'all', 'f');
}

1;
