# Copyright (C) 2007, 2008 Joshua Nathaniel Pritikin

use strict;
use warnings;
package PPX::Periodic;
use PPX::Config;
use PPX::DB;
use PPX::Util;

use constant MAX_DAYS_PENDING => 4;

our @ISA = 'Exporter';
our @EXPORT = qw(adjust_manuscript_hours notify_manuscript_ready
estimate_member_parameters update_criteria_popularity
notify_new_manuscript archive_assignments_for_withdrawn
redress_cancelled_assignments estimate_review_quality
archive_completed_assignment refresh_ml_summary
escalate_late_screening archive_completed_manuscript
propagate_quality_ratings notify_manuscript_not_submitted
approve_pending_manuscripts tidy_manuscripts give_late_warning
notify_best_review_available archive_2review_assignment);

sub notify_best_review_available {
  my $count=0;
  my $u1 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_best_notified = 't' WHERE manuscript_id = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_title
FROM manuscript
WHERE manuscript_archived AND
  manuscript_withdrawn IS NULL AND
  NOT manuscript_best_notified
SQL
  $q1->execute;
  while (my ($id, $title) = $q1->fetchrow_array) {
    my ($best_id, $bestq) = get_best_review($id);
    next if !defined $best_id;

    my $short_title = truncate_to_chars($title, 40);
    my $q2 = $DB->prepare(<<SQL);
SELECT member_id, m.member_email
FROM (SELECT *
      FROM assignment a
      WHERE a.assignment_cancelled IS NULL AND
        assignment_for != ? AND
        manuscript_id = ? AND
       (a.assignment_revtime[1] IS NOT NULL OR
        a.assignment_revtime[2] IS NOT NULL)) AS data
NATURAL JOIN member m
WHERE
  extract_assignment_quality(data.*) < ?
SQL
    $q2->execute(ASSIGN_FOR_SCREENING, $id, $bestq);
    while (my ($member_id, $email) = $q2->fetchrow_array) {
      my $page = "leaf=show_revision;manuscript_id=$id;reviewer_id=$best_id";
      my $link = get_email_link(EMAIL_LOGIN, $member_id, $page);
      my $qual = REVISION_QUALITY->[$bestq];
      sendmail_no_msgstamp($email, "Re: $short_title", qq[
The best review was rated "$qual."

You are welcome to study this review in order that you may improve
your reviews in the future. You may use the following link:

  $link
]);
      ++$count;
    }
    $u1->execute($id);
  }
  $count;
}

sub adjust_manuscript_hours { # TODO simplify to one SQL update
  my $count=0;
  my $u2 = $DB->prepare(<<SQL);
UPDATE manuscript
SET manuscript_hours =
  LEAST(manuscript_hours + (1+manuscript_orig_hours)/2,
    COALESCE(manuscript_max_hours, manuscript_orig_hours + ?))
WHERE manuscript_id = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_orig_hours,
  extract_hours_from_interval(manuscript_due, now()) as remaining
FROM manuscript_tm m
WHERE manuscript_submitted IS NOT NULL and now() < manuscript_due AND
  (SELECT COUNT(*) FROM assignment a
   WHERE a.manuscript_id = m.manuscript_id AND
     a.assignment_cancelled IS NULL AND a.assignment_for != ?)
   < m.manuscript_min_reviews
SQL
  $q1->execute(ASSIGN_FOR_SCREENING);
  while (defined(my $m = $q1->fetchrow_hashref)) {
    my $orig = $m->{manuscript_orig_hours};
    my $min = int((1 + $orig)/2);
    if ($m->{remaining} < $min) {
      $u2->execute(MAX_DAYS_EXTENSION * 24, $m->{manuscript_id});
      ++$count;
    }
  }
  $count;
}

sub notify_manuscript_not_submitted {
  my $u1 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_invite_sent = now() WHERE manuscript_id = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_title, mem.member_id, member_email
FROM manuscript NATURAL JOIN member mem
WHERE manuscript_submitted IS NULL AND
  extract(days from now() - manuscript_cdate) > 1 AND
  manuscript_invite_sent IS NULL
SQL
  $q1->execute;
  my $count = 0;
  while (my ($id, $title, $member_id, $email) = $q1->fetchrow_array) {
    my $short_title = truncate_to_chars($title, 40);
    my $link = get_email_link(EMAIL_LOGIN, $member_id,
			      "leaf=presubmit;id=$id");
    sendmail_no_msgstamp($email, "Re: $short_title", "
Your manuscript titled:

  $title

is registered with the exchange but is not yet submitted. To attract
reviews, you must submit this manuscript. If you wish to submit your
manuscript now and you have sufficient word-count, you may use the
following link:

  $link

Be sure to click the 'Submit' button at the bottom of the page if
you wish to proceed.
");
    $u1->execute($id);
    ++$count;
  }
  $count;
}

sub give_late_warning {
  my $u1 = $DB->prepare(<<SQL);
UPDATE assignment SET assignment_late_warning = 't'
WHERE manuscript_id = ? AND member_id = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT a.member_id, member_email, a.manuscript_id,
  (SELECT manuscript_title FROM manuscript m
   WHERE m.manuscript_id = a.manuscript_id) AS manuscript_title
FROM assignment a NATURAL JOIN member
WHERE assignment_revtime[1] IS NULL AND NOT assignment_late_warning AND
  assignment_cancelled IS NULL AND assignment_for != ? AND
  (SELECT NOT manuscript_is_closed AND manuscript_due < now() AND
     manuscript_withdrawn IS NULL
   FROM manuscript_tm m
   WHERE m.manuscript_id = a.manuscript_id)
SQL
  $q1->execute(ASSIGN_FOR_SCREENING);
  my $count = 0;
  while (defined (my $asn = $q1->fetchrow_hashref)) {
    my $short_title = truncate_to_chars($asn->{manuscript_title}, 40);
    my $link = get_email_link(EMAIL_LOGIN, $asn->{member_id},
			      "leaf=address_assignment;id=$asn->{manuscript_id}");
    sendmail_no_msgstamp($asn->{member_email}, "Re: $short_title", qq[
With respect to manuscript:

  $asn->{manuscript_title}

You volunteered to give feedback yet no feedback has been received.
You are now late. You may use the following link:

  $link

Please give your prompt attention to this matter. If you are having
trouble figuring out what to do, don't hesitate to reply to this
e-mail for help.

Maybe you couldn't find any errors in the original manuscript and
therefore you didn't provide any feedback. Actually you must provide
some feedback, even if only "Great job. I really enjoyed it." Do reply
to this e-mail if this is the case.
]);
    $u1->execute($asn->{manuscript_id}, $asn->{member_id});
    ++$count;
  }
  $count;
}

sub notify_manuscript_ready {
  my $u1 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_notified = now()
WHERE manuscript_id = ?
SQL
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_title, member_id, member_email
FROM manuscript_tm m NATURAL JOIN member
WHERE manuscript_due < now() AND manuscript_notify AND
  manuscript_withdrawn IS NULL AND NOT manuscript_archived AND
  (manuscript_notified IS NULL OR
   (extract_hours_from_interval(now(),COALESCE(manuscript_notified,now())) >= ? AND
    (SELECT COUNT(*) FROM assignment a
     WHERE a.manuscript_id = m.manuscript_id AND
     assignment_for != ? AND
     assignment_cancelled IS NULL AND
     ((assignment_revtime[1] IS NOT NULL AND assignment_quality[1] IS NULL) OR
      (assignment_revtime[2] IS NOT NULL AND assignment_quality[2] IS NULL))) > 0))
SQL
  $q1->execute(24, ASSIGN_FOR_SCREENING);
  my $sent=0;
  while (defined(my $m = $q1->fetchrow_hashref)) {
    ++$sent;
    my $link = get_email_link(EMAIL_LOGIN, $m->{member_id},
			      "leaf=show_assignments;id=$m->{manuscript_id}");
    sendmail_no_msgstamp($m->{member_email}, 'Re: '.truncate_to_chars($m->{manuscript_title}, 40), <<"EMAIL");
With respect to your manuscript titled:

  "$m->{manuscript_title}"

Reviewers' comments are now available. You may use the following
link to login:

  $link

Be sure to rate the quality of your reviews. You will continue
receiving reminders until you rate all of your reviews.
EMAIL
    $u1->execute($m->{manuscript_id});
  }
  $sent;
}

sub estimate_member_parameters {
  my $q1 = $DB->prepare(<<SQL);
SELECT member_lang_quality[?],
  (SELECT COALESCE(AVG(CAST(extract_assignment_quality(a.*) AS double precision) / ?), .5)
   FROM assignment a
   JOIN manuscript m ON
     (a.manuscript_id = m.manuscript_id and m.language_id = ml.language_id)
   WHERE a.assignment_for != ? AND a.assignment_archived AND
     (a.assignment_revtime[1] IS NOT NULL OR
      a.assignment_revtime[2] IS NOT NULL) AND
     a.assignment_cancelled IS NULL AND
     m.manuscript_writing = ? AND
     m.manuscript_archived AND manuscript_withdrawn IS NULL)
FROM language ml
WHERE language_id = ?
SQL
    my $u1 = $DB->prepare(<<SQL);
UPDATE language ml
SET member_lang_quality[?] = ?
WHERE language_id = ?
SQL
  $DB->begin_work;
  for my $wl (1 .. NUM_WRITING_LEVELS) {
    $q1->execute($wl, MAX_REVISION_QUALITY, ASSIGN_FOR_SCREENING, $wl,
		 LANG_ENGLISH);
    my ($cur, $new) = $q1->fetchrow_array;
    if (abs($cur - $new) > .05) {
      $u1->execute($wl, $new, LANG_ENGLISH);
    }
  }
  $DB->commit;
}

sub update_criteria_popularity {
  my $q1 = $DB->prepare(<<SQL);
SELECT criteria_id, criteria_hits,
  extract_hours_from_interval(now(), criteria_sampled) as hours
FROM criteria
WHERE extract_hours_from_interval(now(), criteria_sampled) > 24*7
SQL
  my $u1 = $DB->prepare(<<SQL);
UPDATE criteria set criteria_hits_per_hour = ?, criteria_hits = 0,
  criteria_sampled = now()
WHERE criteria_id = ?
SQL
  $q1->execute;
  while (defined(my $c = $q1->fetchrow_hashref)) {
    my $hph = $c->{criteria_hits} / $c->{hours};
    $u1->execute($hph, $c->{criteria_id});
  }
}

sub notify_new_manuscript {
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_title, manuscript_context, manuscript_genre,
  manuscript_wc, manuscript_writing, manuscript_abstract, member_id,
  manuscript_min_quality, manuscript_max_late, manuscript_escrowed,
  language_id, manuscript_vulgar,
  manuscript_eligible_for_vetting
    AS can_vet,
  COALESCE(extract_hours_from_interval(now(), manuscript_invite_sent) > 24, 'f')
    AS reminder
FROM manuscript_tm m
WHERE manuscript_approved IS NOT NULL AND
  manuscript_submitted + interval '10 min' < now() AND
  manuscript_withdrawn IS NULL AND
  (manuscript_invite_sent IS NULL OR
   extract_hours_from_interval(now(), manuscript_invite_sent) > 24) AND
  now() < manuscript_due AND
  (SELECT COUNT(*) FROM assignment a
   WHERE a.manuscript_id = m.manuscript_id AND
     a.assignment_cancelled IS NULL AND a.assignment_for != ?)
   < m.manuscript_min_reviews
SQL
  $q1->execute(ASSIGN_FOR_SCREENING);
  my $c1 = $DB->prepare(<<SQL);
CREATE TEMP TABLE manuscript_criteria ON COMMIT DROP AS
SELECT criteria_id FROM criteria
WHERE manuscript_criteria_match(criteria_id, ?)
SQL
  my $c2 = $DB->prepare(<<SQL);
CREATE TEMP TABLE member_notify ON COMMIT DROP AS
SELECT m.member_id,
  (SELECT criteria_id
   FROM member_criteria mc
   WHERE mc.member_id = m.member_id AND mc.member_criteria_email = 't' AND
     mc.criteria_id IN (SELECT mac.criteria_id FROM manuscript_criteria mac)
   LIMIT 1) as criteria_id
FROM member_tz m LEFT JOIN member_lang ml ON
  (ml.language_id = ? AND ml.member_id = m.member_id)
WHERE m.member_id != ? AND NOT m.member_bouncing AND
  (EXTRACT(year FROM now()) - m.member_birth > ? OR ?) AND
  ((? AND
   NOT EXISTS(SELECT * FROM assignment a
              WHERE a.member_id = m.member_id AND a.manuscript_id = ?) AND
  100 * member_late <= ? AND
  (CASE WHEN COALESCE(member_lang_samples[?],0) > ?
    THEN round(?*ml.member_lang_quality[?]) ELSE 0 END) >= ? AND
  NOT (? AND is_member_in_escrow(?, ?, m.member_id)))
    OR
  (? AND COALESCE(member_lang_samples[?],0) < ? AND
   ? NOT IN
       (SELECT anym.member_id
        FROM assignment a
          JOIN manuscript anym ON (a.manuscript_id = anym.manuscript_id)
        WHERE a.member_id = m.member_id)))
SQL
  my $q2 = $DB->prepare(<<SQL);
SELECT m.member_id, m.member_email, c.criteria_id, c.criteria_name
FROM member_notify mn
JOIN criteria c ON (c.criteria_id = mn.criteria_id)
JOIN member m ON (m.member_id = mn.member_id)
WHERE mn.criteria_id IS NOT NULL
SQL
  my $u1 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_invite_sent = now() WHERE manuscript_id = ?
SQL
  my $count = 0;
  while (defined(my $m = $q1->fetchrow_hashref)) {
    my $manuscript_id = $m->{manuscript_id};
    $DB->begin_work;
    $c1->execute($manuscript_id);
    $c2->execute(LANG_ENGLISH, $m->{member_id}, ADULT_AGE,
		 !$m->{manuscript_vulgar}?'t':'f',
		 't', $manuscript_id,
		 $m->{manuscript_max_late}, $m->{manuscript_writing},
		 SENIORITY_THRESHOLD, MAX_REVISION_QUALITY,
		 $m->{manuscript_writing}, $m->{manuscript_min_quality},
		 $m->{manuscript_escrowed}, $m->{language_id},
		 $m->{manuscript_writing},
                 $m->{can_vet}, $m->{manuscript_writing}, SENIORITY_THRESHOLD,
                 $m->{member_id});
    $q2->execute;
    $u1->execute($manuscript_id);
    $DB->commit;

    my @body;
    push @body, "This manuscript is still available for editing!\n"
      if $m->{reminder};
    push @body, "Context and Instructions:\n$m->{manuscript_context}\n"
      if $m->{manuscript_context};
    push(@body, 'Genre: '.$m->{manuscript_genre},
	 'Title: '.$m->{manuscript_title},
	 'Words: '.$m->{manuscript_wc},
	 'Audience: '.WRITING_LEVEL->[$m->{manuscript_writing}-1]);
    push @body, "Abstract:\n$m->{manuscript_abstract}\n"
      if $m->{manuscript_abstract};

    my $common_body = join("\n", @body);

    while (my ($member_id, $email, $criteria_id, $criteria_name) =
	   $q2->fetchrow_array) {
      my $short_title = truncate_to_chars($m->{manuscript_title}, 40);

      my $proof =
	get_email_link(EMAIL_LOGIN, $member_id,
		       "leaf=show_manuscript;id=$manuscript_id;for=edit");
      my $unsub = get_email_link(EMAIL_LOGIN, $member_id,
				 "leaf=criteria_unsub;criteria_id=$criteria_id");

      sendmail_no_msgstamp($email, "[PEX: $criteria_name] ".$short_title,
			   $common_body."\n\nEDIT $proof
\nUNSUBSCRIBE $unsub
");
      ++$count;
    }
  }
  $count;
}

sub archive_assignments_for_withdrawn {
  # keep in synch with PPX::Manuscipt::redress_withdrawl
  int $DB->do(<<SQL, {}, ASSIGN_FOR_SCREENING, MAX_DAYS_PENDING);
UPDATE assignment a
SET assignment_archived = 't', assignment_value =
 (SELECT manuscript_wc *
    ROUND(4*
      extract_hours_from_interval(m.manuscript_withdrawn, assignment_ctime)
    / GREATEST(1,extract_hours_from_interval(m.manuscript_due, assignment_ctime))) / 4
  FROM manuscript_tm m
  WHERE manuscript_id = a.manuscript_id)
WHERE assignment_for != ? AND NOT assignment_archived AND
  (SELECT extract(days from now() - manuscript_withdrawn)
   FROM manuscript
   WHERE manuscript_id = a.manuscript_id) > ?
SQL
}

sub redress_cancelled_assignments {
  # keep in synch with PPX::Manuscript::redress_cancellation
  int $DB->do(<<SQL, {}, ASSIGN_FOR_SCREENING, MAX_DAYS_PENDING);
UPDATE assignment a
SET assignment_prefer_late = 'f', assignment_value =
 -(SELECT manuscript_wc *
    ROUND(4*
      extract_hours_from_interval(assignment_cancelled, assignment_ctime)
    / GREATEST(1,extract_hours_from_interval(manuscript_due, assignment_ctime))) / 4
  FROM manuscript_tm m
  WHERE m.manuscript_id = a.manuscript_id)
WHERE assignment_for != ? AND assignment_cancelled IS NOT NULL AND
  assignment_value IS NULL AND
  (SELECT manuscript_withdrawn IS NULL
   FROM manuscript m
   WHERE m.manuscript_id = a.manuscript_id) AND
  (SELECT extract(days from now() - assignment_cancelled)
   FROM manuscript
   WHERE manuscript_id = a.manuscript_id) > ?
SQL
}

sub estimate_review_quality {
  # keep in synch with PPX::Manuscript::show_revision
  my $u1 = $DB->prepare(<<SQL);
UPDATE assignment a
SET assignment_quality[?] = ROUND(? * COALESCE(
  (SELECT member_lang_quality[(SELECT manuscript_writing
                               FROM manuscript m
                               WHERE m.manuscript_id = a.manuscript_id)]
   FROM member_lang ml
   WHERE ml.member_id = a.member_id AND
     ml.language_id = (SELECT m.language_id FROM manuscript m
                       WHERE m.manuscript_id = a.manuscript_id)),
  (SELECT member_lang_quality[(SELECT manuscript_writing
                               FROM manuscript m
                               WHERE m.manuscript_id = a.manuscript_id)]
   FROM language ml
   WHERE ml.language_id = (SELECT m.language_id FROM manuscript m
                           WHERE m.manuscript_id = a.manuscript_id))))
WHERE assignment_quality[?] IS NULL AND assignment_revtime[?] IS NOT NULL AND
  (SELECT extract(days from now() - manuscript_close_time)
   FROM manuscript_tm
   WHERE manuscript_id = a.manuscript_id) > ?
SQL
  my $got = 0;
  for my $version (1..2) {
    $got +=
      int $u1->execute($version, MAX_REVISION_QUALITY, $version, $version,
		       MAX_DAYS_PENDING);
  }
  $got
}

sub archive_2review_assignment {
  # keep in synch with PPX::Manuscript::address_assignment
  int $DB->do(<<SQL, {}, 1, ASSIGN_FOR_SCREENING, MAX_DAYS_PENDING*2);
UPDATE assignment a
SET assignment_archived = 't',
  assignment_prefer_late =
    COALESCE(assignment_prefer_late, assignment_quality[1] <= ?)
WHERE assignment_quality[1] IS NOT NULL AND
  assignment_quality[2] IS NOT NULL AND
  NOT assignment_archived AND assignment_for != ? AND
  (SELECT extract(days from now() - manuscript_close_time)
   FROM manuscript_tm
   WHERE manuscript_id = a.manuscript_id) > ?
SQL
}

sub archive_completed_assignment {
  # keep in synch with PPX::Manuscript::address_assignment
  int $DB->do(<<SQL, {}, ASSIGN_FOR_SCREENING, MAX_DAYS_PENDING*2);
UPDATE assignment a
SET assignment_archived = 't'
WHERE (assignment_quality[1] IS NULL OR
       assignment_quality[2] IS NULL) AND
  NOT assignment_archived AND assignment_for != ? AND
  (SELECT extract(days from now() - manuscript_close_time)
   FROM manuscript_tm
   WHERE manuscript_id = a.manuscript_id) > ?
SQL
}

sub archive_completed_manuscript {
  # keep in synch with PPX::Manuscript::show_assignments
  int $DB->do(<<SQL, {}, MAX_DAYS_PENDING*2);
UPDATE manuscript m
SET manuscript_archived = 't'
WHERE NOT manuscript_archived AND
  (SELECT extract(days from now() - manuscript_close_time)
   FROM manuscript_tm mt
   WHERE mt.manuscript_id = m.manuscript_id) > ? AND
  NOT EXISTS(SELECT * from assignment a
             WHERE a.manuscript_id = m.manuscript_id AND
               NOT assignment_archived)
SQL
}

sub refresh_ml_summary {
  $DB->begin_work;
  $DB->do('delete from ml_summary');
  my $i1 = $DB->prepare(<<SQL);
INSERT INTO ml_summary (language_id, ml_summary_writing, ml_summary_late,
  ml_summary_quality, ml_summary_count)
SELECT language_id, writing, late, quality, count(*)
FROM
  (SELECT l.language_id, wl as writing,
    ROUND(? * member_late) AS late,
    (SELECT ROUND(? * ml.member_lang_quality[wl])
     FROM member_lang ml WHERE ml.member_id = m.member_id AND
     ml.language_id = l.language_id and member_lang_samples[wl] >= ?)
    AS quality
  FROM member_tz m CROSS JOIN language l, CAST(? as integer) AS wl
  WHERE extract(days from now() - m.member_hitstamp) < 90)
  AS data
WHERE quality IS NOT NULL
GROUP BY language_id, writing, late, quality;
SQL
  my $i2 = $DB->prepare(<<SQL);
INSERT INTO ml_summary (language_id, ml_summary_writing, ml_summary_late,
  ml_summary_quality, ml_summary_count)
SELECT language_id, writing, late, -1, count(*)
FROM
  (SELECT l.language_id, wl as writing, ROUND(? * member_late) AS late
  FROM member_tz m CROSS JOIN language l, CAST(? as integer) AS wl
  WHERE extract(days from now() - m.member_hitstamp) < 90 AND
  COALESCE((SELECT member_lang_samples[wl] FROM member_lang ml
     WHERE ml.member_id = m.member_id AND
       ml.language_id = l.language_id), 0) < ?)
  AS data
GROUP BY language_id, writing, late;
SQL
  for my $wl (1 .. NUM_WRITING_LEVELS) {
    $i1->execute(MAX_LATE_DIVISION, MAX_REVISION_QUALITY,
		 SENIORITY_THRESHOLD, $wl);
    $i2->execute(MAX_LATE_DIVISION, $wl, SENIORITY_THRESHOLD);
  }
  $DB->commit;
}

sub escalate_late_screening {
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id FROM manuscript
WHERE NOT manuscript_escalated AND manuscript_submitted IS NOT NULL AND
  manuscript_flagged IS NULL AND
  extract_hours_from_interval(now(), manuscript_submitted) >= ?
SQL
  $q1->execute(HOURS_BEFORE_SCREENING_ESCALATION);
  while (my ($id) = $q1->fetchrow_array) {
    escalate_screening($id);
  }
}

sub approve_pending_manuscripts {
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id FROM manuscript
WHERE manuscript_submitted IS NOT NULL AND manuscript_flagged IS NULL AND
  extract_hours_from_interval(now(), manuscript_submitted) >= ?
SQL
  $q1->execute(HOURS_UNTIL_DEFAULT_SCREENING);
  while (my ($id) = $q1->fetchrow_array) {
    $DB->do('SELECT flag_manuscript(?,?)', {}, $id, FLAG_OK);
    manuscript_flagged($id, FLAG_OK);
    print "manuscript $id approved without screening\n";
  }
}

sub tidy_manuscripts {
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_notify, member_id, member_email,
  manuscript_title
FROM manuscript_tm NATURAL JOIN member
WHERE manuscript_is_closed AND NOT manuscript_close_tidy
SQL
  my $q2 = $DB->prepare(<<SQL);
SELECT member_id FROM assignment
WHERE manuscript_id = ? AND
  assignment_revtime[1] IS NOT NULL AND
  assignment_revtime[2] IS NOT NULL
SQL
  my $u2 = $DB->prepare(<<SQL);
UPDATE assignment SET assignment_prefer_late = 'f'
WHERE manuscript_id = ? AND assignment_revtime[1] IS NOT NULL AND
  assignment_revtime[2] IS NULL AND
  assignment_for != ? AND assignment_cancelled IS NULL
SQL
  my $u3 = $DB->prepare(<<SQL);
UPDATE assignment SET assignment_prefer_late = 't'
WHERE manuscript_id = ? AND assignment_revtime[1] IS NULL AND
  assignment_for != ? AND assignment_cancelled IS NULL
SQL
  my $u4 = $DB->prepare(<<SQL);
UPDATE assignment SET assignment_quality[2] = 0,
  assignment_value = -ABS(assignment_value), assignment_archived = 't'
WHERE manuscript_id = ? AND assignment_cancelled IS NULL AND
  assignment_revtime[1] IS NULL AND assignment_revtime[2] IS NULL AND
  assignment_for != ?
SQL
  my $q3 = $DB->prepare(<<SQL);
SELECT COUNT(*) FROM assignment
WHERE manuscript_id = ? AND assignment_revtime[2] IS NOT NULL AND
  assignment_quality[2] IS NULL
SQL
  my $u5 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_close_tidy = 't'
WHERE manuscript_id = ?
SQL
  my $u6 = $DB->prepare(<<SQL);
UPDATE manuscript SET manuscript_notified = now()
WHERE manuscript_id = ?
SQL
  $q1->execute;
  my $count=0;
  while (my ($id, $notify, $member_id, $member_email, $title)
	 = $q1->fetchrow_array) {
    $DB->begin_work;
    $q2->execute($id);
    while (my ($rid) = $q2->fetchrow_array) {
      next if !defined $BaseDir; # skip for regression testing
      my $rdir = get_assignment_directory($id, $rid);
      my @arg = map { sprintf("$rdir/rev%d.html", $_) } (1..2);
      system(join(' ', PROG_WDIFF, @arg, '>/dev/null')); # danger, no quoting
      if ($? & 127) {
	warn "wdiff died $?";
	next;
      } else {
	my $status = $? >> 8;
	if ($status == 2) {
	  warn "wdiff unhappy";
	} elsif ($status == 0) {
	  $DB->do(<<SQL, {}, $id, $rid);
UPDATE assignment SET assignment_revtime[2] = NULL, assignment_quality[2] = NULL
WHERE manuscript_id = ? AND member_id = ?
SQL
	}
      }
    }
    $u2->execute($id, ASSIGN_FOR_SCREENING);
    $u3->execute($id, ASSIGN_FOR_SCREENING);
    $u4->execute($id, ASSIGN_FOR_SCREENING);
    $q3->execute($id);
    my ($new) = $q3->fetchrow_array;
    if ($new and $notify) {
      my $link = get_email_link(EMAIL_LOGIN, $member_id,
				"leaf=show_assignments;id=$id");
      sendmail_no_msgstamp($member_email,
			   'Re: '.truncate_to_chars($title, 40), <<"EMAIL");
With respect to your manuscript titled:

  "$title"

This manuscript is now closed. At least one additional late review
is available.  You may use the following link to login:

  $link

Be sure to rate the quality of all your reviews.
EMAIL
      $u6->execute($id);
    }
    $u5->execute($id);
    $DB->commit;
    ++$count;
  }
  $count;
}

sub propagate_quality_ratings {
  my $q1 = $DB->prepare(<<SQL);
SELECT manuscript_id, manuscript_update_quality(manuscript_id)
FROM manuscript m
WHERE NOT manuscript_propagated AND manuscript_archived AND
  manuscript_withdrawn IS NULL AND
  NOT EXISTS(SELECT * from assignment a
             WHERE a.manuscript_id = m.manuscript_id AND
               NOT assignment_archived)
SQL
  $q1->execute;
  while (my ($id, $touch) = $q1->fetchrow_array) {
    print "$touch\tmanuscript_update_quality($id)\n";
  }
}

1;
