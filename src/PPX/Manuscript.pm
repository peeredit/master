# Copyright (C) 2007, 2008 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::Member::Impl;
use PPX::TZ;
use PPX::Captcha;

use constant CONTEXT_WORDS => 80;
use constant GENRE_WORDS => 6;
use constant TITLE_WORDS => 25;
use constant ABSTRACT_WORDS => 120;

use constant EDITING_STYLE_HINTS =>
  p('Small corrections should be made directly at the site of the
error. Place short comments between sentences or paragraphs.
Insert longer discussion at the top, before the original manuscript.
If possible, preserve the majority of the manuscript in original form.
If you reorder paragraphs then refrain from also
making small corrections within a paragraph since these small
changes can be hard to notice in wake a large scale changes.
Once you submit your edits, you will have an opportunity to review and
refine them.');

use constant EDITING_BUTTON_HINTS =>
  p(' Assistance is provided by the buttons on the right side
of the screen. '.i('Original').' shows the original manuscript without your
changes. '.i('Changes').' shows the original with your changes highlighted.
'.i('Revised').' shows your changes without reference to the original. ',
i('Changes').' is the default.');


sub query_tz1 {
  banner(gettext('Time Zone'));

  print(p(txt('For the purpose of determining your time zone,
in which continental area are you located?')),
	start_form('query_tz2'),
	tag('table'));

  my @c = sort keys %TZ;
  my $col = int((@c+1)/2);
  for (my $x=0; $x < $col; $x++) {
    my $c1 = $c[$x];
    my $c2 = $c[$x + $col];
    print(tag('tr'));
    print(td(radio($c1, 'continent')),
	  td(label($c1, gettext($c1))),
	  td(hskip(4)));
    if ($c2) {
      print(td(radio($c2, 'continent')),
	    td(label($c2, gettext($c2))));
    }
    print(gat('tr'));
  }
  print(gat('table'),
	p({align => 'center'}, submit(gettext('Next'))),
	gat('form'));
}

sub query_tz2 {
  my $continent = $R->param('continent');
  return query_tz1() if !$TZ{$continent || ''};

  banner(gettext('Time Zone'));

  my @c = @{ $TZ{$continent} };
  my $col = int((@c+3)/4);
  
  print(p(txt('For the purpose of determining your time zone,
which city is closest to your location?')),
	start_form('front'),
	tag('table'));

  for (my $x=0; $x < $col; $x++) {
    print(tag('tr'));
    for my $c ($c[$x], $c[$x + $col], $c[$x + 2*$col], $c[$x + 3*$col]) {
      next if !$c;
      print(td(radio($c, 'city')),
	    td(label($c, gettext($c))),
	    td(hskip(4)));
    }
    print(gat('tr'));
  }
  print(gat('table'),
        hidden(continent => $continent),
	p({align => 'center'},
	  leaf(gettext('Back'), 'query_tz1'), hskip(4),
	  submit(gettext('Next'))),
	gat('form'));
}

sub query_my_manuscript {
  my ($id) = @_;
  confess if !$id;

  my $q1 = $DB->prepare_cached(<<SQL);
select * from manuscript
where member_id = ? and manuscript_id = ? and manuscript_submitted IS NULL
SQL
  $q1->execute(member(), $id);
  my $m = $q1->fetchrow_hashref;
  $q1->finish;
  $m;
}

sub get_manuscript_sample {
  my ($id, $lines) = @_;
  my $dir = get_job_directory($id);
  open my $fh, "$dir/orig.txt" or do {
    warn "open $dir/orig.txt: $!";
    return 'There was an error accessing your manuscript.';
  };
  binmode $fh, ':utf8';
  my $sample='';
  for (my $x=1; $x <= $lines; $x++) {
    my $l = <$fh>;
    last if !defined $l;
    $sample .= sprintf "%02d: %s", $x, $l;
  }
  $sample;
}

sub _wcfield {
  my ($big, $size, $err, $key, $title, $default, $maxlen) = @_;
  my $id = gen_id();
  $default ||= '';

  my @wc = split /\s+/, $default||'';
  my $wc = @wc;
  my $func = 'wordcount';
  #
  # This kind of a hack but genre needs special treatment to
  # ignore punctuation.
  #
  $func .= '_genre'
    if $key eq 'genre';
  my @script = (onKeyUp => "$func(event, '$id', $maxlen, '$title')",
		onChange => "$func(event, '$id', $maxlen, '$title')");
  my $input = do {
    if ($big) {
      p(textarea($key, $default, $size, 80, @script));
    } else {
      p(textfield($key, size => $size, value => $default, @script));
    }
  };

  my @e;
  @e = (class => 'error')
    if $err->{$key};
  join('', p(span({ @e, id=>$id }, $title." ($wc of $maxlen words)")), $input);
}

sub reg_manuscript {
  my ($class, $err) = @_;
  $err ||= {};

  banner(gettext('Register Manuscript'));

  my $m = {};
  my $manuscript_id = '';
  my $id = $R->param('id');
  if ($id) {
    $m = query_my_manuscript($id);
    $manuscript_id = hidden(id => $id);
  }

  my $context_id = gen_id();
  my $context_len = CONTEXT_WORDS;
  my $genre_id = gen_id();
  my $genre_len = GENRE_WORDS;
  my $title_id = gen_id();
  my $title_len = TITLE_WORDS;
  my $abstract_id = gen_id();
  my $abstract_len = ABSTRACT_WORDS;

  if ($err->{msg}) {
    print(div({ class => 'error' }, $err->{msg}))
  } elsif ($m->{manuscript_flagged}) {
    if ($m->{manuscript_flagged} == FLAG_PROHIBITED) {
      if (member('member_adult')) {
	print(div({ class => 'error' }, 'Screening revealed that this
manuscript contains vulgar or obscene material which may be inappropriate
for youths. You must tick the appropriate checkbox below before you
resubmit.'));
	$err->{adult} = 1;
      } else {
	print(div({ class => 'error' }, 'Screening revealed that this
manuscript contains vulgar or obscene material which may be inappropriate
for youths. You are not allowed to submit this manuscript.'));
      }
    } elsif ($m->{manuscript_flagged} == FLAG_MISCATEGORIZED) {
      print(div({ class=>'error' }, 'Screening revealed that the
context, genre, title, or abstract are misleading and inappropriately
represent your manuscript. Please resolve this problem before you
resubmit.'));
    }
  }

  my $vulgar = '';
  if (member('member_adult')) {
    $vulgar =
      p(checkbox('adult', $m->{manuscript_vulgar}),' ',
	label('adult', lsty($err,'adult','Contains vulgar or obscene material
which may be inappropriate for youths.')),
	' If you neglect to mark an obscene manuscript as such,
your account may be suspended.');
  }

  for my $spec (['','\s+'], ['_genre', '[\s\/\,]+']) {
    print script(sprintf q[
function wordcount%s(ev, id, maximum, template)
{
  var content = ev.target.value;
  var count;
  if (content.length == 0) {
    count = 0;
  } else {
    content = content.split(/%s/);
    count = content.length;
  }
  if (count > maximum) {
    ev.target.style.color = 'red';
  } else {
    ev.target.style.color = 'black';
  }
  var elem = findElement(id);
  elem.textContent = template.concat(' (', content.length, ' of ', maximum, ' words)');
}], @$spec);
  }

  my @e = (class => 'error');

  my $context = $m->{manuscript_context} ||
    'Does the manuscript have a clear purpose?
Is the manuscript organized in a logical way?
Does the organization make sense?
Are there places where more details, examples, or specifics are needed?';

  print(start_form('edit_manuscript'), $manuscript_id,
	div({class => 'section'}, gettext('Background')),
	p(gettext('Language'),': ',
	  popup_menu('lang', ['English'], 'English'),
	  ' ',a({ href => '/static/faq.html' }, '?')),
	p(txt('
Please provide some context for your writing project. Who is your
intended audience? Do you have specific guidelines to follow?  What do
you hope to accomplish with your writing? If you are seeking a
specific kind of feedback then be sure to mention it here.')),
	p(small('Remember: It makes no sense to ask for help with sentence
structure and grammar until you have thoroughly refined the
thesis or focus, purpose, organization, and development of your
manuscript. For more information about how to prioritize different
types of editing, refer to the ',
	  a({ href => 'http://owl.english.purdue.edu/handouts/general/gl_hocloc.html' },
	    'Purdue Online Writing Lab handout on Higher Order
Concerns (HOCs) and Lower Order Concerns (LOCs)'), '.')),

	_wcfield(1, 5, $err, 'context', 'Optional Context and Instructions',
		 $context, CONTEXT_WORDS),
	_wcfield(0, 60, $err, 'genre', 'Genre',
		 $m->{manuscript_genre}, GENRE_WORDS));

  print(div({ class => 'section'}, 'Particulars'),
	_wcfield(0, 80, $err, 'title', 'Title',
		 $m->{manuscript_title}, TITLE_WORDS),
	$vulgar,
	_wcfield(1, 6, $err, 'abstract', 'Optional Abstract',
		$m->{manuscript_abstract}, ABSTRACT_WORDS));

  my $due = '';
  if (!$err->{due} and $m->{manuscript_hours}) {
    $due = format_hours($m->{manuscript_hours});
  }
  my $maxtime = '';
  if (!$err->{maxtime} and $m->{manuscript_max_hours}) {
    $maxtime = format_hours($m->{manuscript_max_hours});
  }

  print(tag('table'));
  if (!$m->{manuscript_wc}) {
    print(Tr(td(upload('manuscript')),
	     td(),
	     td(lsty($err,'manuscript', 'Upload your manuscript.'),
	       ' '.ask_file_format('type'))));
  } else {
    my $sample = get_manuscript_sample($id, 12);
    my $msg = sprintf('You have uploaded a manuscript. It contains %d words.
The first 12 lines are shown below.',
		      $m->{manuscript_wc});
    print(Tr(td({ colspan => 3 }, $msg)),
	  Tr(td({ colspan => 3 }, pre({ style => 'border: gray solid thin;' },
				      txt($sample)))),
	  Tr(td(leaf('Download Manuscript', 'download', which=>'orig',
		     manuscript_id=>$id)),
	     td(),
	     td('You can verify that your manuscript was uploaded faithfully.')),
	  Tr(td(hskip(1))),
	  Tr(td(upload('manuscript')),
	     td(),
	     td(lsty($err,'manuscript', 'Replace the current manuscript.').
	       ' '.ask_file_format('type')),
	    ));
  }

  my $dir = get_job_directory($id)
    if $id;
  if (!$dir or !-e "$dir/orig.pdf") {
    print(Tr(td(upload('pdf')),
	     td(),
	     td(txt('Formatting, tables, and figures will be stripped
from your manuscript during upload. If some of these elements are important
then you may upload a PDF version of your manuscript.'))));
  } else {
    print(Tr(td(hskip(1))),
	  Tr(td(leaf('Download PDF', 'download', which=>'pdf',
		     manuscript_id=>$id)),
	     td(),
	     td('You can verify that your PDF was uploaded faithfully.')),
	  Tr(td(upload('pdf')),
	     td(),
	     td('Replace the current PDF.')
	    ),
	  Tr(td(hskip(1))));
  }

  my %wmap;
  for (my $x=1; $x <= NUM_WRITING_LEVELS; $x++)
    { $wmap{WRITING_LEVEL->[$x-1]} = $x }

  print(Tr(td(popup_menu('level', WRITING_LEVEL,
			 $m->{manuscript_writing}, \%wmap),
	     td(),
	     td('Your manuscript is intended for which audience?'))),
	Tr(td(checkbox('spellok', exists $m->{manuscript_spellok}?$m->{manuscript_spellok} : 0)),
	   td(),
	   td(label('spellok', lsty($err,'spellok',"I affirm that I have
run my manuscript through an automatic spellchecker and that there are
no overt spelling errors.")))),
	Tr(td('Minimum '.popup_menu('min_rev', [1..5],
			 ($m->{manuscript_min_reviews} || '2'))),
	   td(),
td({ rowspan=>2 },
   'How many reviews do you want? You usually want two or more reviews
to insure that at least one reviewer turns in a review on time and
also to be able to compare the feedback from reviewers.
On the other hand, be advised that the nominal
word count of your manuscript is multipled by the number of reviews
you receive for the purposes of word-count accounting.'),
	   ),
	Tr(td('Maximum '.popup_menu('max_rev', [2..5],
				    ($m->{manuscript_max_reviews} || '3'))),
	   td()),
	Tr(td('Editing '.textfield('due', value => $due, size => 10)),
	   td(hskip(1)),
	   td(lsty($err,'due','How much time will you allow for editing?
Use 4h 2d notation meaning 4 hours and 2 days (52 hours). Perhaps a good rule
of thumb is to allocate at least 2 hours per 500 words. Allow at least 2
days if you want to give the reviewer the opportunity to sleep with
your manuscript under his or her pillow.'))),
	Tr(td('Deadline '.textfield('maxtime', value => $maxtime, size=>10)),
	   td(hskip(1)),
	   td(lsty($err,'maxtime',
		   'When do you need the reviews back, finished or not?
Due to our present paucity of volunteer reviewers, your nominal
time allowance may be extended until the minimum number of reviews accrue.
If no review is received by the deadline then you will receive no reviews.'))),
	gat('table'),
	tag('p', align => 'center'));
  print(leaf('Delete', 'delete_manuscript', id=>$id),
	hskip(4))
    if $id;
  print(submit(gettext('Update')),
	gat('p'),
	gat('form'));
}

sub download {
  my ($class) = @_;
  my $manuscript_id = $R->param('manuscript_id');
  my $reviewer_id = $R->param('reviewer_id');

  return $class->front()
    if (!member('member_flag_admin') and
	!is_author_or_reviewer($manuscript_id, $reviewer_id));

  my $which = $R->param('which');
  my $link = do {
    if ($which eq 'orig') {
      my $dir = get_job_directory($manuscript_id);
      create_download_link("$dir/orig.txt");
    } elsif ($which =~ /^r/) {
      my $rdir = get_assignment_directory($manuscript_id, $reviewer_id);
      if ($which eq 'r1') {
	create_download_link("$rdir/rev1.txt");
      } elsif ($which eq 'r2') {
	create_download_link("$rdir/rev2.txt");
      } else {
	warn $which;
      }
    } elsif ($which eq 'pdf') {
      my $dir = get_job_directory($manuscript_id);
      create_download_link("$dir/orig.pdf");
    } else {
      warn $which;
    }
  };
  print $R->redirect($link);
}

sub manuscript_check_fields {
  my ($msg, $err, $genre, $title, $due, $maxtime, $wc, $spellok) = @_;
  if (!$genre) {
    $err->{genre}=1;
    push @$msg, txt('Please indicate a genre such as whether your
manuscript is scientific, academic, historical, fiction, or
poetry and the relevant subtype(s).');
  }
  if (!$title) {
    $err->{title} = 1;
    push @$msg, txt('Enter a title.');
  }
  if (!$due) {
    $err->{due} = 1;
    push @$msg, txt('How much time will you allow for editing?');
  } elsif ($due > MAX_DAYS_ALLOWANCE * 24) {
    $err->{due} = 1;
    my $n = MAX_DAYS_ALLOWANCE;
    push @$msg, "The maximum allowable time is $n days.";
  }
  if ($maxtime and $due and $maxtime > $due + 24 * MAX_DAYS_EXTENSION) {
    $err->{maxtime} = 1;
    my $n = MAX_DAYS_EXTENSION;
    push @$msg, "The maximum allowable time is $n days more than the
initial allowance.";
  }
  if (!$wc) {
    $err->{manuscript} = 1;
    push @$msg, txt('You must provide the actual manuscript.')
  }
  if (!$spellok) {
    $err->{spellok} = 1;
    push @$msg, "Did you spellcheck your manuscript? If so, tick the
corresponding checkbox below.";
  }
}

sub ask_file_format {
  my ($key) = @_;
  join('','You may indicate the file format:',
       br,
       join(br,
	    radio('auto', $key, 1).' '.
	    label('auto', 'auto detect'),
	    radio('txt', $key).' '.
	    label('txt','text, UTF-8, LaTeX, CrossMark, Creole, markdown, etc.'),
	    radio('odt', $key).' '.
	    label('odt', 'OpenOffice ODT'),
	    radio('doc', $key).' '.
	    label('doc', 'RTF or Microsoft DOC (Office 2007 is not supported yet)')))
}

sub receive_text_upload {
  my ($msg, $filename, $type, $dir, $deststem, $wrap) = @_;
  my $ext = 'txt';

  # inspect the filename
  if (!$filename) {
    push @$msg, txt('Please specify which file to upload.');
    return 1;
  } elsif ($type eq 'auto' and $filename =~ /\.(\w+?)$/) {
    $ext = lc $1;
    if ($ext =~ /^doc|rtf|odt|txt|tex$/) {
      # ok
    } elsif ($ext eq 'pdf') {
      push @$msg, txt('PDF format is unacceptable for a manuscript.
You may submit the manuscript in plain text (LaTeX, UTF8, CrossMark,
Creole, markdown, etc.), OpenOffice ODT, or Microsoft DOC
formats only.');
      return 1;
    } else {
      warn "Unrecognized extension .$ext";
    }
  } elsif ($type ne 'auto') {
    $ext = $type;
  }

  my $ok = $R->upload($filename, "$dir/$deststem-raw.$ext");
  if (!$ok) {
    warn $R->cgi_error;
    return 1;
  }

  my $mp = $R->_mod_perl_request;
  if ($ext eq 'doc') {
    my $exe = $BaseDir.'/var/script/catdocwrapper';
    my $out_fh =
      $mp->spawn_proc_prog($exe,["$dir/$deststem-raw.$ext", "$dir/$deststem.txt"]);
    my $st = spawn_wait($exe, $out_fh);
    if ($st) {
      push @$msg, txt('An error occurred while extracting your
Microsoft DOC file. Can you upload a text file instead?');
      return 1;
    }
  } elsif ($ext eq 'odt') {
    my $exe = PROG_ODT2TXT;
    my $out_fh =
      $mp->spawn_proc_prog($exe,['--encoding=UTF-8', "$dir/$deststem-raw.$ext",
				 "--output=$dir/$deststem.txt"]);
    my $st = spawn_wait($exe, $out_fh);
    if ($st) {
      push @$msg, txt('An error occurred while extracting your
OpenOffice ODT file. Can you upload a text file instead?');
      return 1;
    }
  } else {
    my $bad = eval {
      convert_to_text("$dir/$deststem-raw.$ext", "$dir/$deststem.txt", $wrap);
    };
    if ($@) {
      if ($@ =~ /utf|unicode|character/i) {
	push @$msg, txt('Your text file is not encoded in UTF-8.
Please recode to UTF-8 and try uploading again.');
      } else {
	warn $@;
      }
    }
    return $bad
      if $bad;
  }
  return 1 if html_escape_file("$dir/$deststem");
  0; #no error
}

sub html_escape_file {
  my ($stem) = @_;
  open my $in, "$stem.txt" or do {
    warn "open $stem.txt: $!";
    return 1;
  };
  binmode $in, ':utf8';
  open my $out, ">$stem.html" or do {
    warn "open >$stem.html: $!";
    return 1;
  };
  binmode $out, ':utf8';
  while (defined(my $l = <$in>)) {
    print $out txt($l);
  }
  close($out) or do {
    warn "close >$stem.html: $!";
    return 1;
  };
  0
}

sub edit_manuscript {
  my ($class) = @_;
  my (@err,%err);

  my $id = $R->param('id');
  my $m;
  if (!$id) {
    $DB->begin_work;
    my $i1 = $DB->prepare_cached('insert into manuscript (member_id) values (?)');
    $i1->execute(member());
    $id = currval('manuscript', 'manuscript_id');
    $DB->commit;
    $m = {};
  } else {
    $m = query_my_manuscript($id);
  }
  my $lang = LANG_ENGLISH; #$R->param('lang'); ignore for now
  my $context = truncate_to_words($R->param('context')||'', CONTEXT_WORDS);
  my $raw_genre = $R->param('genre')||'';
  $raw_genre =~ s/[[:punct:]]/ /g;
  my $genre = truncate_to_words($raw_genre, GENRE_WORDS);
  my $title = truncate_to_words($R->param('title')||'', TITLE_WORDS);
  my $adult = 0;
  $adult = $R->param('adult')
    if member('member_adult');
  my $abstract = truncate_to_words($R->param('abstract')||'', ABSTRACT_WORDS);
  my $due = parse_duration $R->param('due');
  my $maxtime = parse_duration $R->param('maxtime');
  if ($maxtime and $due and $maxtime < $due) {
    $maxtime = $due;
  }
  my $min_rev = $R->param('min_rev');
  my $max_rev = $R->param('max_rev');
  if ($min_rev > $max_rev) { $max_rev = $min_rev }
  if ($max_rev < 2) { $max_rev = 2 }
  my $manuscript = $R->param('manuscript');
  my $pdf = $R->param('pdf');
  my $wc = $m->{manuscript_wc} || 0;
  my $wlevel = $R->param('level');
  if ($wlevel < 1 or $wlevel > NUM_WRITING_LEVELS) {
    $wlevel = 1;
  }
  my $spellok = $R->param('spellok')? 1 : 0;

  manuscript_check_fields(\@err,\%err,$genre,$title, $due, $maxtime,
			  $wc || $manuscript, $spellok);

  if ($manuscript) {
    my $dir = get_job_directory($id);

    my $type = $R->param('type');
    $err{manuscript} =
      receive_text_upload(\@err,$manuscript,$type, $dir, 'orig', 1);

    if (!$err{manuscript}) {
      # better to spawn wc, no? TODO
      open my $fh, "$dir/orig.txt" or warn "open $dir/orig.txt: $!";
      binmode $fh, ':utf8';
      my $ms = join '' , <$fh>;
      $ms =~ s/^\s+//; #remove leading whitespace
      my @junk = split /\s+/, $ms;
      $wc = @junk;
    }
  }
  if ($pdf) {
    my $dir = get_job_directory($id);
    my $ok = $R->upload($pdf, "$dir/orig.pdf");
    if (!$ok) {
      warn $R->cgi_error;
    }
  }

  my $u1 = $DB->prepare_cached(<<SQL);
update manuscript set language_id=?, manuscript_context=?,
  manuscript_genre=?, manuscript_title=?, manuscript_vulgar=?,
manuscript_abstract=?, manuscript_hours=?, manuscript_max_hours=?,
manuscript_min_reviews=?, manuscript_wc=?, manuscript_writing=?,
manuscript_spellok=?, manuscript_max_reviews=?
where member_id = ? and manuscript_id = ? and manuscript_submitted IS NULL
SQL
  $u1->execute($lang, $context, $genre, $title, $adult?'t':'f', $abstract,
	       $due, $maxtime, $min_rev, $wc, $wlevel, $spellok,
	       $max_rev, member(), $id);

  if (@err) {
    $err{msg} = join_remedy(@err);
    $R->param(id => $id);
    return $class->reg_manuscript(\%err);
  }

  if ($manuscript) {
    banner('Manuscript');

    my $sample = get_manuscript_sample($id, 20);
    my $msg = sprintf('You have uploaded a manuscript. It contains %d words.
The first 20 lines are shown below.', $wc);
    print(tag('table'),
	  Tr(td({ colspan => 3 }, $msg)),
	  Tr(td(gettext('Genre').':'), td(hskip(2)), td(txt($genre))),
	  Tr(td(gettext('Title').':'), td(), td(txt($title))),
	  Tr(td({ colspan => 3 }, pre({ style => 'border: gray solid thin;' },
				      txt($sample)))),
	  gat('table'),
	  p({align => 'center'},
	    leaf(gettext('Continue'), 'presubmit', id => $id)))
  } else {
    $class->presubmit();
  }
}

sub set_timezone {
  my $city = $R->param('city');
  if ($R->param('continent') and $city) {
    $city =~ s/ /_/g;
    my $q = $DB->prepare_cached(<<SQL);
select tz_id from tz where tz_continent = ? and tz_city = ?
SQL
    $q->execute($R->param('continent'), $city);
    my ($tz_id) = $q->fetchrow_array;
    $q->finish;
    if ($tz_id) {
      $DB->do('update member set tz_id = ? where member_id = ?',
	      {}, $tz_id, member());
      my $q2 = $DB->prepare_cached(<<SQL);
SELECT member_tz, current_time at time zone member_tz as member_time
from member_tz where member_id = ?
SQL
      $q2->execute(member());
      my ($tz, $time) = $q2->fetchrow_array;
      $q2->finish;
      set_member('member_tz', $tz);
      set_member('member_time', $time);
    }
  }
}

sub query_num_screening_assignments {
  my $member_id = member();

  # This includes both archived and not archived on purpose.
  # We also count cancelled assignments. Why? Because it should
  # be a rare event and otherwise, somebody could go on screening
  # hundreds of manuscripts (rather slowly but it is vaguely a
  # security threat).
  #
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT COUNT(*) FROM assignment
WHERE assignment_for = ? AND member_id = ? AND
  COALESCE(
  ((SELECT manuscript_submitted
    FROM manuscript
    WHERE manuscript_submitted IS NOT NULL and member_id = ?
    ORDER BY manuscript_submitted LIMIT 1) < assignment_ctime), 't')
SQL

  $q2->execute(ASSIGN_FOR_SCREENING, $member_id, $member_id);
  my ($screened) = $q2->fetchrow_array;
  $q2->finish;

  $screened
}

sub query_available_for_screening {
  return () if !member('member_adult');

  my $i = query_member_quality_and_late(member(), LANG_ENGLISH);
  my @ok;
  for (my $x=1; $x <= NUM_WRITING_LEVELS; $x++) {
    my $ok = ($i->{"q_valid$x"} and $i->{"quality$x"} >= MIN_SCREENER_QUALITY);
    push @ok, $ok? 't':'f';
  }

  my $q3 = $DB->prepare_cached(<<SQL);
SELECT extract_hours_from_interval(now(), manuscript_submitted) AS pending,
  manuscript_id, manuscript_title, manuscript_genre, manuscript_wc
FROM manuscript m
WHERE manuscript_submitted IS NOT NULL AND m.member_id != ? AND
  manuscript_flagged IS NULL AND
  (SELECT COUNT(a.member_id) FROM assignment a
    WHERE assignment_for = ? AND assignment_cancelled IS NULL AND
      a.manuscript_id = m.manuscript_id) < ? AND
  NOT EXISTS(SELECT * FROM assignment a
             WHERE a.manuscript_id = m.manuscript_id AND a.member_id = ?) AND
  (ARRAY[?::boolean,?,?,?])[m.manuscript_writing]
ORDER BY manuscript_submitted
LIMIT 25
SQL

  my @m;
  $q3->execute(member(), ASSIGN_FOR_SCREENING, SCREENINGS_PER_MANUSCRIPT,
	       member(), @ok);
  while (defined(my $m = $q3->fetchrow_hashref)) { push @m, $m }
  \@m;
}

sub any_pending_work {
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT * FROM (
SELECT a.manuscript_id, a.member_id, manuscript_title,
  extract(days from now() - COALESCE(manuscript_close_time,
                                     manuscript_withdrawn))
    AS days
FROM assignment a JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE assignment_cancelled IS NULL AND NOT assignment_archived AND
  assignment_for != ? AND a.member_id = ? AND (
  COALESCE(assignment_quality[1] IS NOT NULL AND
           assignment_quality[2] IS NOT NULL AND
           extract(days from now() - manuscript_close_time) >= 3, FALSE) OR
  COALESCE(assignment_value IS NULL AND
           extract(days from now() - manuscript_withdrawn) >= 3, FALSE))

UNION ALL

SELECT m.manuscript_id, NULL, manuscript_title,
  extract(days from now() - manuscript_close_time) as days
FROM manuscript_tm m
WHERE manuscript_is_closed AND manuscript_withdrawn IS NULL AND
  extract(days from now() - manuscript_close_time) >= 3 AND
  member_id = ? AND
  EXISTS(SELECT * FROM assignment a
         WHERE a.manuscript_id = m.manuscript_id AND
           assignment_for != ? AND (
  (assignment_cancelled IS NOT NULL AND assignment_value IS NULL) OR
  (assignment_revtime[1] IS NOT NULL AND assignment_quality[1] IS NULL) OR
  (assignment_revtime[2] IS NOT NULL AND assignment_quality[2] IS NULL)))
) AS data
ORDER BY manuscript_title, days
SQL
  $q1->execute(ASSIGN_FOR_SCREENING, member(), member(), ASSIGN_FOR_SCREENING);
  my @l;
  while (defined(my $l = $q1->fetchrow_hashref)) { push @l,$l }
  return if !@l;

  banner('Unfinished Work');

  print(p('You must archive all pending work before registering more
manuscripts or accepting more editing assignments.'),
	tag('table'),
	Tr(join(th(hskip(2)),
		th('Days Old'),
		th('Title'))));

  for my $l (@l) {
    my $url = do {
      my $title = txt(truncate_to_chars($l->{manuscript_title}, 60));
      my $page = $l->{member_id}? 'address_assignment' : 'show_assignments';
      leaf_plain($title, $page, id => $l->{manuscript_id});
    };
    print(Tr(join(td(),
		  td({ align => 'right' }, $l->{days}),
		  td($url))));
  }

  print(gat('table'));
  1;
}

sub member_is_spammer {
  my $q5 = $DB->prepare_cached(<<SQL);
SELECT COUNT(*) FROM manuscript
WHERE manuscript_flagged = ? AND manuscript_archived AND
  extract_hours_from_interval(now(), manuscript_withdrawn) < ? AND
  member_id = ?
SQL
  $q5->execute(FLAG_SPAM, 24 * 30, member());
  my ($spammer) = $q5->fetchrow_array;
  $q5->finish;
  $spammer;
}

sub presubmit {
  my ($class) = @_;
  my @msg;
  my $err = {};

  my $id = $R->param('id');
  my $m = query_my_manuscript($id);

  manuscript_check_fields(\@msg,$err,$m->{manuscript_genre},
			  $m->{manuscript_title}, $m->{manuscript_hours},
			  $m->{manuscript_max_hours},
			  $m->{manuscript_wc}, $m->{manuscript_spellok});
  if (@msg) {
    $err->{msg} = join_remedy(@msg);
    return $class->reg_manuscript($err);
  }

  if (member('member_adult')) {
    my $have = query_num_screening_assignments();
    my $avail = query_available_for_screening();
    if ($have < SCREENINGS_PER_MANUSCRIPT*2 and
	@$avail >= SCREENINGS_PER_MANUSCRIPT*4) {
      find_screening_needed1($class, <<MSG, $have, $avail);
Before you can submit more manuscripts, you need to help
screen manuscripts submitted by other members.
MSG
      return;
    }
  }

  return if any_pending_work();

  my $balance = query_current_balance();
  my $estimate = $m->{manuscript_min_reviews} * $m->{manuscript_wc};
  my $short_mar;
  if ($balance < $estimate * MARGIN_REQUIREMENT) {
    $short_mar = $estimate * MARGIN_REQUIREMENT - $balance;
  }
  my $climit = member('member_credit_limit');
  my $short_cr;
  if ($balance + $climit < $estimate) {
    $short_cr = $estimate - ($balance + $climit);
  }
  if ($short_cr and $short_mar) {
    if ($short_cr > $short_mar) { undef $short_mar } else { undef $short_cr }
  }
  if ($short_mar) {
    push @msg, sprintf('The estimated word-count cost of your manuscript
is %d (%d words * %d reviewers). The margin requirement is %02d%%
which is %d words (%02d%% of %d). You cannot submit this manuscript
until you raise your balance by at least %d. You can raise your word-count
balance by editing other manuscripts.',
       $estimate, $m->{manuscript_wc}, $m->{manuscript_min_reviews},
		       100*MARGIN_REQUIREMENT, int($estimate * MARGIN_REQUIREMENT),
		       100*MARGIN_REQUIREMENT, $estimate, $short_mar);
  }
  if ($short_cr) {
    push @msg, sprintf('The estimated word-count cost of your manuscript
is %d (%d words * %d reviewers). This cost exceeds your balance
plus credit limit (%d balance + %d credit limit = %d) by %d. You cannot
submit this manuscript until you raise your balance by at least %d.
You can raise your word-count balance by editing other manuscripts.',
		      $estimate, $m->{manuscript_wc}, $m->{manuscript_min_reviews},
		      $balance, $climit, $balance + $climit, $short_cr,
		      $short_cr);
  }

  if (@msg) {
    $err->{msg} = join_remedy(@msg);
    return $class->front($err);
  }

  #
  # Now the manuscript is ready to submit. Now decide whether the
  # classification is okay and how to restrict the reviewers.
  #

  my $qal = query_member_quality_and_late(member(), LANG_ENGLISH,
					  $m->{manuscript_writing});

  my ($assign_late, $assign_total, $q_valid, $quality, $late) =
    ($qal->{assignments_late}, $qal->{assignments_total},
     $qal->{q_valid}, $qal->{quality}, $qal->{late});

  if ($R->param('final')) {
    if (member_is_spammer()) {
      eval_captcha(\@msg, $err);
    }
    my $q = $q_valid? int($R->param('q') || 0)-1 : 0;
    $q = 0 if $q < 0;
    my $l = 100 * int($R->param('l') || 0) / MAX_LATE_DIVISION;

    # We should bail out if @msg. Let's ignore that for now.

    if (1) {
      if (@msg) {
	$err->{msg} = join_remedy(@msg);
      }
      if (!defined $q or $q > $quality) { $q = $quality }
      if (!defined $l or $l < $late) { $l = $late } # TODO too lenient
      $DB->do(<<SQL, {}, int $q, int $l, member(), $id);
UPDATE manuscript SET manuscript_submitted = now(),
  manuscript_flagged = NULL, manuscript_escalated = 'f',
  manuscript_min_quality = ?, manuscript_max_late = ?
WHERE member_id = ? AND manuscript_id = ? AND manuscript_submitted is NULL
SQL
      return $class->front($err);
    }
  }

  my $safe = member('member_adult')? '' : 'AND criteria_kids_safe';

  my $q1 = $DB->prepare_cached(<<"SQL");
SELECT criteria_name
FROM criteria, CAST(? AS integer) AS m_id
WHERE manuscript_criteria_match(criteria_id, m_id) AND
  member_id IS NULL
  $safe
ORDER BY criteria_name
SQL
  $q1->execute($id);
  my @cri;
  while (my ($cname) = $q1->fetchrow_array) { push @cri, $cname }

  my $q4 = $DB->prepare_cached(<<SQL);
SELECT ml_summary_late, ml_summary_quality+1, ml_summary_count
FROM ml_summary
WHERE language_id = ? AND ml_summary_writing = ?
SQL
  $q4->execute(LANG_ENGLISH, $m->{manuscript_writing});
  my $pop;
  while (my ($l, $q, $c) = $q4->fetchrow_array) {
    $pop->[$l][$q] = $c;
  }

  banner('Submit Manuscript', onload => 'recalc()');

  if (@msg) {
    $err->{msg} = join_remedy(@msg);
    print(div({ class => 'error' }, $err->{msg}));
  }

  my $mhrs = $m->{manuscript_max_hours};
  if (!$mhrs) {
    $mhrs = $m->{manuscript_hours} + 24 * MAX_DAYS_EXTENSION;
  }
  print(start_form('presubmit'),
	hidden(final => 1, id => $id),
	p('Your manuscript is not yet submitted. Please verify that all
the details are in order. If everything looks okay then press the "Submit"
button at the bottom of the page.'),
	div({ class => 'section'}, 'Manuscript'),
	p('Title: '. txt($m->{manuscript_title})),
	p('Audience: '. txt(WRITING_LEVEL->[$m->{manuscript_writing}-1])),
	p('Matching criteria: ', join('; ', map { txt($_) } @cri)),
	p('Reviews: ', $m->{manuscript_min_reviews}, ' - ',
	  $m->{manuscript_max_reviews}),
	p('Your reviews will be available as early as '.
	  format_hours($m->{manuscript_hours}).
	  ' but no later than '.format_hours($mhrs).'.'),
       );

  if ($q_valid) {
  print(div({ class => 'section'}, 'Reviewer Constraints'),
	p(sprintf('Your late assignments: %d of %d (%d%%)',
		  $assign_late, $assign_total, $late)),
	tag('p'),
	'Your editing quality: ');

  my $lstep = int(($late + 100/MAX_LATE_DIVISION - 1)*MAX_LATE_DIVISION / 100);
  print txt(REVISION_QUALITY->[$quality]);

  print(gat('p'),
	tag('table'),
	tag('tr'),
	td('Potential Reviewers: '. span({ id => 'total' }, '0')),
       );

  for (my $p=0; $p <= MAX_LATE_DIVISION; $p++) {
    print(th(label("l$p", hskip(1).
		   sprintf('%d%%', 100 * $p/MAX_LATE_DIVISION). hskip(1))));
  }

  my $border = 'solid gray medium';
  print(td({ style => "border-left: $border; border-right: $border;" },
	   'Late'),
	gat('tr'));

  my $first = $R->param('q');
  for (my $q=1+MAX_REVISION_QUALITY; $q >= 0; $q--) {
    my $qlabel = $q == 0? 'new members' : REVISION_QUALITY->[$q-1];
    print(tag('tr'),
	  th(label("q$q", txt($qlabel))));
    for (my $l=0; $l <= MAX_LATE_DIVISION; $l++) {
      my $c = $pop->[$l][$q] || 0;
      if ($q == $quality+1 and $l == $lstep) { --$c } # remove self
      $c = 0 if $c < 0;  # stats might be stale
      print(td({ id => "c($l,$q)", align => 'right' }, $c.hskip(2)));
    }
    my @dis;
    if ($q > 1+$quality) { @dis = (disabled => 1) }
    if (!defined $first and (!@dis or $q == 0)) { $first = $q }
    print(td(radio($q, 'q', defined $first && $q == $first, @dis, id=>"q$q",
		   onChange => 'recalc()')),
	  gat('tr'));
  }
  print(tag('tr'),
	td({ style => "border-top: $border; border-bottom: $border;" },
	   'Review Quality'));
  $first = $R->param('l');
  for (my $l=0; $l <= MAX_LATE_DIVISION; $l++) {
    my @dis;
    if ($l < $lstep) { @dis = (disabled => 1) }
    if (!defined $first and (!@dis or $l == MAX_LATE_DIVISION)) { $first = $l }
    print(td({ align=>'right' },
	     radio($l, 'l', defined $first && $l == $first, @dis, id => "l$l",
		   onChange => 'recalc()').hskip(2)));
  }

  print(gat('tr'),
	gat('table'),
	tag('p'),
	'Members who have completed an insufficient number of
assignments are not included on the chart.');
  if ($quality >= MIN_VETTING_QUALITY) {
    print ' You will receive free reviews from new members for
the purpose of vetting.';
  }
  print gat('p');
  }

  if (member_is_spammer()) {
    print(tag('table'),
	  captcha_table_row($err),
	  gat('table')) ;
  }
  print(p({ align => 'center' },
	  leaf('Go Back', 'front'), hskip(4),
	  leaf('Change', 'reg_manuscript', id => $id), hskip(4),
	  submit(gettext('Submit'))),
	gat('form'));

  print script('
function recalc() {
  var f = document.presubmit;
  var late;
  var qual;
  for (i=0; i < f.l.length; i++){
    if (f.l[i].checked) late = i;
  }
  for (i=0; i < f.q.length; i++){
    if (f.q[i].checked) qual = i;
  }
  var total = 0;
  for (lx=0; lx < f.l.length; lx++){
    for (qx=0; qx < f.q.length; qx++){
      var id = "c";
      var el = findElement(id.concat("(", lx, ",", qx, ")"));
      if (lx <= late && (f.q.length - qx - 1) <= qual) {
        el.style.background = "yellow";
        total = total + parseInt(el.textContent);
      } else {
        el.style.background = "white";
      }
    }
  }
  var telem = findElement("total");
  telem.textContent = total;
}
');
}

sub delete_manuscript {
  my ($class) = @_;
  my $id = $R->param('id');
  $DB->do('delete from manuscript where member_id = ? and manuscript_id = ? and manuscript_submitted is NULL', {}, member(), $id);
  $class->front();
}

sub no_excessive_screening {
  print(p('You have completed your share of the screening work.
You will not be asked to do more screening until you submit
another manuscript.'));
}

sub find_screening_needed {
  my ($class) = @_;

  my $have = query_num_screening_assignments();
  my $avail = query_available_for_screening();

  if ($have >= SCREENINGS_PER_MANUSCRIPT*3) {
    banner('Find Manuscript');
    no_excessive_screening();
    print(p({align => 'center'}, leaf('Go Back', 'front')));
    return;
  }
  if (@$avail == 0) {
    banner('Find Manuscript');
    print(p('There are currently no manuscripts in need of screening.'),
	  p({align => 'center'}, leaf('Go Back', 'front')));
    return;
  }

  find_screening_needed1($class, '', $have, $avail);
}

sub find_screening_needed1 {
  my ($class, $msg, $have, $avail) = @_;

  banner('Find Manuscript');

  if ($msg) {
    print(div({class=>'error'}, $msg));
  }

  print(p("To insure the quality of manuscripts,
every manuscript must be screened
for spam, miscategorization, and prohibited content
before being made available for editing.
Your turn to help in this process has arrived."),
	p('
Once you
start, you are required to make your decision within one hour.
You will not be permitted to both screen and edit a manuscript
so you should try to select manuscripts for screening which
you are not interested in editing.
As compensation for screening, you will receive 1% of the word count.'));

  print(tag('table'),
	Tr(join(th(hskip(2)),
		map {th($_)} qw(Pending Genre Title Words))));
  for my $m (@$avail) {
    my $title = leaf_plain(truncate_to_chars($m->{manuscript_title}, 60),
			  'show_manuscript', id=>$m->{manuscript_id},
			   for => 'screen');
    print(Tr(join(td(),
		  td({ align=>'center'}, format_hours($m->{pending})),
		  td(truncate_to_chars($m->{manuscript_genre}, 20)),
		  td($title),
		  td({ align=>'right'}, $m->{manuscript_wc})
		 )));
  }

  print(gat('table'),
	p({align => 'center'}, leaf('Go Back', 'front')));
}

sub show_manuscript {
  my ($class) = @_;
  my $id = $R->param('id');
  my $for = $R->param('for');

  if ($for ne 'edit' and !member('member_adult')) {
    return $class->front;
  }

  my $kid_safe = member('member_adult')?'':q[manuscript_vulgar = 'f' AND];
  my $state = do {
    if ($for eq 'edit') { 'manuscript_approved IS NOT NULL' }
    else { 'manuscript_flagged IS NULL' }
  };

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_context, manuscript_genre, manuscript_title,
  manuscript_vulgar, manuscript_abstract, manuscript_hours_remain,
  manuscript_wc, manuscript_writing,
  manuscript_late_hours(manuscript_late_style, manuscript_orig_hours) AS
    late_hours
FROM manuscript_tm
WHERE manuscript_id = ? AND
  $kid_safe
  manuscript_submitted IS NOT NULL AND
  manuscript_withdrawn IS NULL AND
  $state
SQL
  $q1->execute($id);
  my $m = $q1->fetchrow_hashref;
  $q1->finish;

  banner('Show Manuscript');

  if (!$m) {
    print(p('Manuscript not available.'),
	  p({align => 'center'},
	    leaf(gettext('Continue'), 'find_manuscript')));
    return;
  }

  my $action = $for eq 'edit'? 'edit' : 'screen';

  print(p(txt("Do you want to $action this manuscript?")),
	div({class => 'section'}, gettext('Background')));

  if ($m->{manuscript_context}) {
    print(p(gettext('Context and Instructions').': '.
	    txt($m->{manuscript_context})));
  }

  print(p(gettext('Genre').': '.txt($m->{manuscript_genre})),
	div({ class => 'section'}, 'Particulars'),
	tag('table'),
	Tr(td(gettext('Title').':'), td(), td(txt($m->{manuscript_title}))),
	Tr(td('Audience:'), td(),
	   td(WRITING_LEVEL->[$m->{manuscript_writing}-1])));

  if ($m->{manuscript_vulgar}) {
    print(Tr(td(),td(),
	     td(div({class=>'error'}, 'This manuscript contains vulgar or
obscene material which may be inappropriate for youths.'))));
  }

  if ($m->{manuscript_abstract}) {
    print(Tr(td(gettext('Abstract').': '), td(),
	     td(txt($m->{manuscript_abstract}))));
  }

  print(Tr(td(gettext('Words').':'), td(hskip(2)), td($m->{manuscript_wc})));
  if ($for eq 'edit') {
    # We only report # of hours information here (and not in the RSS
    # or email) because it becomes stale quickly.

    print(Tr(td(gettext('Due').':'), td(),
	     td(format_hours($m->{manuscript_hours_remain}))),
	  Tr(td(gettext('Late allowance').':'), td(),
	     td(format_hours($m->{late_hours}))));
  }
  print(gat('table'),
	p({align => 'center'},
	  leaf('Go Back', 'front'), hskip(4),
	  leaf(ucfirst($action), 'assign_manuscript',id=>$id, for=>$for)));
}

sub assign_manuscript {
  my ($class) = @_;
  my $id = $R->param('id');
  my $for = $R->param('for');

  my $constraints_ok;
  my $vetting_ok;

  if ($for eq 'screen') {
    return $class->front if !member('member_adult');

    my $have = query_num_screening_assignments();
    if ($have >= SCREENINGS_PER_MANUSCRIPT*3) {
      banner('Assignment');
      no_excessive_screening();
      print(p({align => 'center'}, leaf('Go Back', 'front')));
      return;
    }
  } else {
    return if any_pending_work();

    my $i = query_member_quality_and_late(member(), LANG_ENGLISH);
    my @arg;
    my @vet;
    for (my $x=1; $x <= NUM_WRITING_LEVELS; $x++) {
      push @arg, $i->{"q_valid$x"}? $i->{"quality$x"} : 0;
      push @vet, !$i->{"q_valid$x"}? 't':'f';
    }
    push @arg, int $i->{late};

    my $q1 = $DB->prepare_cached(<<SQL);
SELECT
 (ARRAY[?::smallint,?,?,?])[manuscript_writing] >= manuscript_min_quality AND
  ? <= manuscript_max_late AND
  NOT (manuscript_escrowed AND
       is_member_in_escrow(m.language_id, m.manuscript_writing, ?)),
  (ARRAY[?::boolean,?,?,?])[manuscript_writing] AND
  manuscript_eligible_for_vetting AND
   m.member_id NOT IN
     (SELECT anym.member_id
      FROM assignment a
        JOIN manuscript anym ON (a.manuscript_id = anym.manuscript_id)
      WHERE a.member_id = ?)
FROM manuscript_tm m
WHERE manuscript_id = ?
SQL
    $q1->execute(@arg, member(), @vet, member(), $id);
    ($constraints_ok, $vetting_ok) = $q1->fetchrow_array;
    $q1->finish;
    if (!$constraints_ok and !$vetting_ok) {
      warn "Attempt to assign $id for editing to member ".member();
      return $class->front();
    }
  }

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT assignment_ctime FROM assignment
WHERE member_id = ? AND manuscript_id = ?
SQL
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT assignment_for, COUNT(*), MIN(now() - assignment_ctime)
FROM assignment
WHERE manuscript_id = ? AND assignment_cancelled IS NULL
GROUP BY assignment_for
SQL
  my $q3 = $DB->prepare_cached(<<SQL);
SELECT manuscript_max_reviews, manuscript_wc,
  manuscript_due + interval '1 minute' < now()
FROM manuscript_tm
WHERE manuscript_id = ?
SQL
  my $i1 = $DB->prepare_cached(<<SQL);
INSERT INTO assignment (member_id, manuscript_id, assignment_value,
  assignment_for)
VALUES (?,?,?,?)
SQL
  $q3->execute($id);
  my ($reviews, $wc, $notime) = $q3->fetchrow_array;
  $q3->finish;

  if ($for eq 'screen') {
    undef $notime;
    $wc = int((99 + $wc) * .01);
  }

  if ($notime) {
    banner('Assignment');
    print(p(txt('The manuscript you selected for editing
will be due in less than 1 minute.')));
    return;
  }

  $DB->begin_work;
  $DB->do("LOCK TABLE assignment IN SHARE ROW EXCLUSIVE MODE");
  $q1->execute(member(), $id);
  my ($already_assigned) = $q1->fetchrow_array;
  $q1->finish;
  $q2->execute($id);
  my @asn;
  for my $f (ASSIGN_FOR_EDITING, ASSIGN_FOR_SCREENING, ASSIGN_FOR_VETTING) {
    $asn[$f] = [0];
  }
  while (my ($f, $assigned, $assign_interval) = $q2->fetchrow_array) {
    $asn[$f] = [$assigned, $assign_interval];
  }
  my $new;
  if (!$already_assigned) {
    my $asn_for = do {
      if ($for eq 'screen' and
	  $asn[ASSIGN_FOR_SCREENING][0] < SCREENINGS_PER_MANUSCRIPT) {
	ASSIGN_FOR_SCREENING;
      } elsif ($constraints_ok and $asn[ASSIGN_FOR_EDITING][0] < $reviews) {
	ASSIGN_FOR_EDITING;
      } elsif ($vetting_ok and
	       $asn[ASSIGN_FOR_VETTING][0] < MAX_VETTING_ASSIGNMENTS) {
	ASSIGN_FOR_VETTING;
      } else { undef }
    };
    if (defined $asn_for) {
      $new=1;
      $i1->execute(member(), $id, $wc, $asn_for);
    }
  }
  $DB->commit;

  if ($new or $already_assigned) {
    $class->address_assignment();
  }
  else {
    banner('Assignment');
    my $tried = do {
      if ($for eq 'screen') { ASSIGN_FOR_SCREENING }
      elsif ($constraints_ok) { ASSIGN_FOR_EDITING }
      else { ASSIGN_FOR_VETTING }
    };
    my $info = sprintf 'The manuscript you selected
now has the target number of reviewers. The most recent reviewer
was added %s ago.', $asn[$tried][1];
    print(p($info),
	  leaf('Continue', 'find_manuscript'));
  }
}

sub withdraw_manuscript {  # not undoable
  my ($class) = @_;
  my $id = $R->param('id');
  my $confirmed = $R->param('confirmed');

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT COUNT(*) FROM assignment
WHERE manuscript_id = ? AND assignment_cancelled IS NULL AND
  assignment_for != ?
SQL
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, manuscript_wc, manuscript_due < now() AS late
FROM manuscript_tm
WHERE manuscript_id = ? and member_id = ?
SQL
  my $q3 = $DB->prepare_cached(<<SQL);
SELECT a.member_id, member_email
FROM assignment a NATURAL JOIN member
WHERE a.manuscript_id = ? AND assignment_cancelled IS NULL AND
  assignment_for != ?
SQL
  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE manuscript
SET manuscript_submitted = NULL
WHERE manuscript_id = ? and member_id = ?
SQL
  my $u2 = $DB->prepare_cached(<<SQL);
UPDATE manuscript SET manuscript_withdrawn = now(), manuscript_archived = 't'
where manuscript_id = ? and member_id = ?
SQL
  my $u3 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_value = 0, assignment_prefer_late = 'f'
WHERE manuscript_id = ? AND assignment_cancelled IS NOT NULL
SQL
  my $u4 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_archived = 't'
WHERE manuscript_id = ? AND assignment_for = ?
SQL

  $q2->execute($id, member());
  my $m = $q2->fetchrow_hashref;
  $q2->finish;

  if ($m->{late}) {
    banner('Withdraw Manuscript');
    print(p(txt('It is now too late to withdraw your manuscript.')));
    return;
  }

  $DB->begin_work;
  $DB->do("LOCK TABLE assignment IN SHARE ROW EXCLUSIVE MODE");
  $q1->execute($id, ASSIGN_FOR_SCREENING);
  my ($assigned) = $q1->fetchrow_array;
  $q1->finish;
  if (!$assigned) {
    $u1->execute($id, member())
  } elsif ($confirmed) {
    $u2->execute($id, member());
  }
  $u3->execute($id);  # pardon cancelled assignments (is_screening or not)
  $u4->execute($id, ASSIGN_FOR_SCREENING);  # archive screeners
  $DB->commit;

  if (!$assigned or $confirmed) {
    if ($confirmed) {
      $q3->execute($id, ASSIGN_FOR_SCREENING);
      while (my ($member_id, $email) = $q3->fetchrow_array) {
	my $title = truncate_to_chars($m->{manuscript_title}, 30);
	my $url = "leaf=address_assignment;id=$id";
	my $link = get_email_link(EMAIL_LOGIN, $member_id, $url);
	my $body = sprintf('
The author of the manuscript titled "%s"
has cancelled your editing assignment. Please login to
record your word-count compensation by clicking the link below:

  %s

Please reply to this e-mail if you have any questions.', $title, $link);
	sendmail_no_msgstamp($email, "[withdrawn] $title", $body);
      }
    }

    return $class->front()
  }

  $class->show_assignments()
}

sub _choose_percent_words {
  my ($quarter, $wc) = @_;

  for (my $p=0; $p <= 4; $p++) {
    print(Tr(td(radio($p, 'quart', $p == $quarter)),
	     td(hskip(2)),
	     td({ align=>'right'}, label($p, sprintf("%d%%", 100*$p/4))),
	     td(hskip(2)),
	     td({ align=>'right'},
		label($p, sprintf("%d words",$wc*$p/4))))
	 );
  }
}

sub attempt_to_rescreen {
  print(p("Once you screen a manuscript, your decision is final.
You can't go back and change it. If you made a mistake, don't worry.
At least three people screen every manuscript."));
}

sub flag_manuscript {
  my ($class) = @_;

  my $id = $R->param('id');
  my $flag = $R->param('flag');
  my $detail = $R->param('detail');
  if ($detail and !$R->param('really')) {
    my $err = { msg => 'Please read the instructions carefully.' };
    return $class->address_assignment($err);
  }
  if ($detail) {
    my $ok = 1;
    for my $k (qw(context genre title abstract audience)) {
      if (!$R->param($k)) { $ok=0; last }
    }
    $flag = $ok? FLAG_OK : FLAG_MISCATEGORIZED;
  }

  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment
SET assignment_flag = ?, assignment_archived = 't'
WHERE assignment_archived = 'f' AND
  now() <= assignment_ctime + interval '1 hour' AND
  assignment_for = ? AND member_id = ? AND manuscript_id = ?
SQL
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT query_screening_status(?)
SQL
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT (manuscript_submitted IS NULL OR
        assignment_ctime < manuscript_submitted) AS withdrawn,
  assignment_cancelled IS NOT NULL AS cancelled
FROM assignment a
JOIN manuscript m ON (a.manuscript_id = m.manuscript_id)
WHERE a.member_id = ? AND a.manuscript_id = ?
SQL

  $DB->begin_work;
  cancel_overdue_screenings(member());
  my ($ok, $status);
  if ($flag) {
    $ok = int $u1->execute($flag, ASSIGN_FOR_SCREENING, member(), $id);
    if ($ok) {
      $q1->execute($id);
      ($status) = $q1->fetchrow_array;
      $q1->finish;
    }
  }
  $DB->commit;

  if (!$ok) {
    $q2->execute(member(), $id);
    my $why = $q2->fetchrow_hashref;
    $q2->finish;

    if ($why->{withdrawn}) {
      # oops, don't tell them :-/
      $status = 0;
    } elsif ($why->{cancelled}) {
      banner('Screening Error');
      print(p('More than one hour has elapsed since you agreed to
screen this manuscript. Screening assignments are automatically
cancelled after one hour.'));
      print(p({align => 'center'}, leaf('Go Back', 'front')));
      return;
    } else {
      banner('Screening Error');
      attempt_to_rescreen();
      print(p({align => 'center'}, leaf('Go Back', 'front')));
      return;
    }
  }

  manuscript_flagged($id, $status);

  $class->find_screening_needed();
}

sub _address_screening {
  my ($class, $err) = @_;

  my $id = $R->param('id');

  cancel_overdue_screenings(member());

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, manuscript_vulgar, manuscript_context,
  manuscript_genre, manuscript_abstract, manuscript_vulgar,
  manuscript_withdrawn IS NOT NULL AS withdrawn, assignment_flag,
  assignment_cancelled IS NOT NULL AS cancelled, assignment_archived,
  manuscript_writing
FROM assignment a
  JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE a.member_id = ? and a.manuscript_id = ?
SQL
  $q1->execute(member(), $id);
  my $asn = $q1->fetchrow_hashref;
  $q1->finish;

  banner('Screening');

  if ($asn->{withdrawn}) {
    print(p('The manuscript has been withdrawn.'),
	  p({align => 'center'}, leaf('Go Back', 'front')));
    return;
  }
  if ($asn->{cancelled}) {
    print(p('You have not addressed this manuscript within one hour.
Hence, your screening assignment was automatically cancelled.'),
	  p({align => 'center'}, leaf('Go Back', 'front')));
    return;
  }
  if ($asn->{assignment_archived}) {
    attempt_to_rescreen();
    return;
  }

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(div({ class => 'section' }, txt('Manuscript')),
	tag('pre'));

  my $jdir = get_job_directory($id);
  my $name = "$jdir/orig.html";
  if (!open my $fh, $name) {
    warn "open $name: $!";
    # We are in PRE mode, so omit the newline.
    print(div({ class => 'error' }, txt('There was an error accessing the manuscript or revision.')));
    return unless SYSTEM_TEST;
  } else {
    binmode $fh, ':utf8';
    for (my $x=1; defined(my $l = <$fh>); $x++) {
      printf "%03d: %s", $x, $l;
    }
  }
  print(gat('pre'));
  my $dir = get_job_directory($id);
  if (-e "$dir/orig.pdf") {
    print(p('Be sure to screen the PDF version as well.'),
	  p(leaf('Download PDF', 'download', which=>'pdf',
	     manuscript_id => $id, reviewer_id => member())));
  }
  print(div({ class => 'section' }, txt('Your Evaluation')),
	start_form('flag_manuscript'),
	hidden(id=>$id, detail=>1),
	tag('p'),
	tag('table'),
	Tr(join(th(hskip(2)),
		th('Specific Problem'),
		th(),
		th('Action'))),
	Tr(join(td(),
		td({colspan=>3}, 'Does the quality of the manuscript lead you
to doubt the good faith of the author? Does the manuscript
contain mostly garbage? For instance, does the manuscript
consist of the word "hello" repeated 1000 times?'),
		td({align=>'center'},
		   leaf('Spam', 'flag_manuscript',
			id=>$id, flag=>FLAG_SPAM)))));
  if (!$asn->{manuscript_vulgar}) {
    print(Tr(join(td(),
		  td({colspan=>3},
		     'Make a cursory inspection of the content. Does the
manuscript contain any vulgar or obscene material? Is there explicit
gore, extreme hate or fear, or lewd narrative?'),
		  td({align=>'center'},
		     leaf('Prohibited', 'flag_manuscript',
			  id=>$id, flag=>FLAG_PROHIBITED)))));
  }

  if ($asn->{manuscript_context}) {
    print(Tr(join(td(),
		  td(gettext('Context and Instructions').': '.
		     txt($asn->{manuscript_context})),
		  td(checkbox('context').' '.
		     label('context','Do you understand the context?
Are the instructions conductive to ethical conduct?')))));
  } else {
    print hidden('context'=>1);
  }

  print(Tr(join(td(),
		td(gettext('Genre').': '.txt($asn->{manuscript_genre})),
		td(checkbox('genre').' '.
		   label('genre','Is the genre accurate?')))),
	Tr(join(td(),
		td(gettext('Title').': '.txt($asn->{manuscript_title})),
		td(checkbox('title').' '.
		   label('title', 'Is the title fitting?')))));

  if ($asn->{manuscript_abstract}) {
    print(Tr(join(td(),
		  td(gettext('Abstract').': '.txt($asn->{manuscript_abstract})),
		  td(checkbox('abstract').' '.
		    label('abstract', 'Does the abstract sensibly summarize
the manuscript?')))));
  } else {
    print hidden('abstract'=>1);
  }

  print(Tr(join(td(),
		td(gettext('Audience').': '.
		   WRITING_LEVEL->[$asn->{manuscript_writing}-1]),
		td(checkbox('audience').' '.
		    label('audience', 'Does the manuscript appear to
be written for the designated audience?')))),
	Tr(join(td(),
		td(),
		td(checkbox('really').' '.
		    label('really', 'I affirm that I have faithfully evaluated this manuscript.')),
		td({align=>'center'},
		   submit('Submit')))),
	gat('table','p','form'));
}

sub _aa_withdrawn {
  my ($id, $asn) = @_;
  banner('Assignment Withdrawn');
  
  my $title = sprintf('The manuscript titled [%s] potentially worth %d words
has been withdrawn by the author.',
		      $asn->{manuscript_title}, $asn->{manuscript_wc});
  if ($asn->{assignment_archived}) {
    print(p(txt($title)),
	  p("You have taken $asn->{assignment_value} words as compensation."));
  } else {
    my $waste = sprintf('How many words will you take as compensation for the
wasted effort (approx. %s of %s) you have put into reviewing this manuscript?',
		      format_hours($asn->{withdrawn_hours}),
		      format_hours($asn->{assignment_hours}));
    print(p(txt($title)),
	  p(txt($waste)),
	  start_form('redress_withdrawl'),
	  hidden(id => $id),
	  tag('table'));
  
    my $quarter =
      int(4*(.2 + $asn->{withdrawn_hours}/$asn->{assignment_hours}));
    _choose_percent_words($quarter, $asn->{manuscript_wc});

    print(gat('table'),
	  p({align => 'center'}, submit(gettext('Submit'))),
	  gat('form'));
  }
}

sub set_assignment_value {
  my $id = $R->param('id');

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, manuscript_wc, assignment_value
FROM assignment a JOIN manuscript m ON (a.manuscript_id = m.manuscript_id)
WHERE a.member_id = ? AND a.manuscript_id = ? AND a.assignment_for = ?
SQL
  $q1->execute(member(), $id, ASSIGN_FOR_EDITING);
  my $asn = $q1->fetchrow_hashref;
  $q1->finish;

  banner('Set Assignment Value');

  my $title = sprintf('The manuscript titled [%s] is %d words long.',
		      txt($asn->{manuscript_title}), $asn->{manuscript_wc});
  print(p($title . ' Normally you retain 100% of this award to
enable you to submit manuscripts for editing in the future. Here you can
reduce the award amount. You might want to do this if you enjoyed reading
the manuscript so much that you want to encourage the author. Or perhaps
you are not interested in writing yourself but just love editing and
wish to encourage all writers.'),
	start_form('address_assignment'),
	hidden(id=>$id),
	tag('table'));
  _choose_percent_words(4, $asn->{manuscript_wc});
  print(gat('table'),
	p({align => 'center'}, submit(gettext('Submit'))),
	gat('form'));
}

sub address_assignment {
  my ($class, $err) = @_;
  $err ||= {};

  my $id = $R->param('id');

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT assignment_for = ? FROM assignment
WHERE manuscript_id = ? and member_id = ?
SQL
  $q2->execute(ASSIGN_FOR_SCREENING, $id, member());
  my ($screening) = $q2->fetchrow_array;
  $q2->finish;
  return _address_screening($class, $err)
    if $screening;

  my $quart = $R->param('quart');
  if (defined $quart) {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment a
SET assignment_value =
  ROUND(? * (SELECT manuscript_wc FROM manuscript m
             WHERE m.manuscript_id = a.manuscript_id) / 4.0)
WHERE assignment_value IS NOT NULL AND assignment_for = ? AND
  manuscript_id = ? AND member_id = ? AND assignment_cancelled IS NULL
SQL
    $u1->execute($quart, ASSIGN_FOR_EDITING, $id, member());
  }

  my $late = $R->param('late');
  if (defined $late) {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_prefer_late = ?
WHERE manuscript_id = ? AND member_id = ?
SQL
    $u1->execute($late, $id, member());
  }

  my $archive = $R->param('archive');
  if ($archive) {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_archived = 't'
WHERE manuscript_id = ? AND member_id = ? AND
  assignment_prefer_late IS NOT NULL
SQL
    $u1->execute($id, member());
    return $class->front();
  }

  my $q1 = $DB->prepare_cached(<<SQL);
select manuscript_title, manuscript_wc, manuscript_vulgar,
  assignment_contact, manuscript_is_closed, manuscript_context,
  COALESCE(assignment_revtime[1], assignment_revtime[2]) IS NOT NULL
    AS has_revision,
  assignment_quality[1] AS assignment_quality1,
  assignment_quality[2] AS assignment_quality2, assignment_prefer_late,
  assignment_ctime AT TIME ZONE ? AS assignment_ctime,
  manuscript_due AT TIME ZONE ? AS manuscript_due,
  manuscript_close_time AT TIME ZONE ? AS manuscript_close_time,
  manuscript_due < now() AS overdue,
  extract_hours_from_interval(manuscript_close_time, now())
    AS hours_till_close,
  manuscript_hours_remain, assignment_cancelled,
  extract_hours_from_interval(manuscript_withdrawn, assignment_ctime)
    AS withdrawn_hours,
  GREATEST(1,extract_hours_from_interval(manuscript_due, assignment_ctime))
    AS assignment_hours,
  extract_hours_from_interval(LEAST(now(), manuscript_close_time),
                              assignment_ctime) AS elapsed,
  (manuscript_withdrawn IS NOT NULL) AS withdrawn,
  manuscript_hours, assignment_archived, assignment_value, assignment_for,
  manuscript_archived
FROM assignment a
  JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE a.member_id = ? and a.manuscript_id = ?
SQL
  $q1->execute(member('member_tz'), member('member_tz'), member('member_tz'),
	       member(), $id);
  my $asn = $q1->fetchrow_hashref;
  $q1->finish;

  if ($asn->{assignment_cancelled} or $asn->{withdrawn}) {
    if ($asn->{assignment_cancelled} and $asn->{withdrawn}) {
      banner('Assignment Void');
      print(p(txt('You cancelled this assignment but then the
author withdrew the manuscript.')));
    } elsif ($asn->{withdrawn}) {
      _aa_withdrawn($id, $asn)
    } elsif ($asn->{assignment_cancelled}) {
      banner('Assignment Cancelled');
      print(p(txt('You have cancelled this assignment.')),
	    p({align=>'center'}, leaf('Go Back', 'front')));
    }
    return;
  }

  if ($R->param('set_contact') and
      $asn->{assignment_contact} != (($R->param('contact')||'') eq 'on')) {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_contact = ?
WHERE manuscript_id = ? and member_id = ?
SQL
    $u1->execute($R->param('contact')? 't':'f', $id, member());
  }

  banner('Address Assignment');

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  my $remain;
  if ($asn->{manuscript_is_closed}) {
    $remain = 'Closed.';
  } else {
    my ($tm,$h);
    if ($asn->{overdue}) {
      $tm = $asn->{manuscript_close_time};
      $h = $asn->{hours_till_close};
    } else {
      $tm = $asn->{manuscript_due};
      $h = $asn->{manuscript_hours_remain};
    }
    $remain = 'Due: '. substr($tm,0,16).br.format_hours($h).' remaining';
    $remain = span({class=>'error'}, $remain)
       if $asn->{overdue};
  }

  print(tag('table', width => '100%', style => 'background: yellow;'));
  print(Tr(td('Context and Instructions:'),
	   td(), td(txt($asn->{manuscript_context}))))
    if $asn->{manuscript_context};
  print(Tr(td('Title:'), td(hskip(4)),
	   td(txt($asn->{manuscript_title}))),
	gat('table'));
  print(tag('table'),
	tag('tr'),
	tag('td', width => '50%'),
	txt('Accepted: ').txt(substr($asn->{assignment_ctime},0,16)).br.
	txt(format_hours($asn->{elapsed}).' elapsed'),
	br,br,
	leaf('Download Original', 'download', which=>'orig',
	     manuscript_id => $id, reviewer_id => member()));

  my $dir = get_job_directory($id);
  if (-e "$dir/orig.pdf") {
    print(br,br,
	  leaf('Download PDF', 'download', which=>'pdf',
	     manuscript_id => $id, reviewer_id => member()));
  }
  print(br,br);

  my $assign_value = do {
    if (defined $asn->{assignment_value} and
	$asn->{assignment_for} == ASSIGN_FOR_EDITING) {
      my $value = "This assignment is worth $asn->{assignment_value} words.";
      if (! $asn->{assignment_archived}) {
	$value = leaf_plain($value, 'set_assignment_value', id=>$id);
      }
      $value.br().br();
    } else {
      ''
    }
  };

  my $quality;
  if (!$asn->{overdue}) {
    print(txt('It is best to cancel your assignment if you foresee
not having enough time. Once you cancel, the author of
the manuscript will decide whether to construe your cancellation
as a missed deadline. The earlier you cancel, the better your
chances of obtaining a pardon.'),
	  br,br,
          leaf('Cancel Assignment', 'shirk_assignment', id=>$id),
	 );
  } else {
    print $assign_value;

    if (defined $asn->{assignment_quality1}) {
      print(txt('The author judged the quality of your review as "'.
		txt(REVISION_QUALITY->[$asn->{assignment_quality1}]).'."'),
	    br,br);
      $quality = $asn->{assignment_quality1};
    }
    if (defined $asn->{assignment_quality2}) {
      print(txt('The author judged the quality of your late review as "'.
		txt(REVISION_QUALITY->[$asn->{assignment_quality2}]).'."'));

      if (!defined $quality or $asn->{assignment_prefer_late}) {
	$quality = $asn->{assignment_quality2};
      }
    }
  }
  print(gat('td'),
	td({ style => 'padding-right: 1em;' }),
	td({ style => 'border-right: thick dashed gray;'}),
	td({ style => 'padding-right: 1em;' }),
	tag('td'),
	$remain,
	br,br);

  if (!$asn->{overdue}) {
    print($assign_value);
  }
  if (!$asn->{manuscript_is_closed}) {
    print(leaf('Edit Online', 'edit_online', id=>$id),
	  br,br,
	  'OR',
	  br,br,
	  start_form('upload_revision'),
	  hidden(id=>$id),
	  upload('revision'),br,br,
	  ask_file_format('type'),br,br,
	  submit('Upload Revision'),
	  gat('form'),br
	 );
    if (!$asn->{has_revision}) {
      print('Note: Even if you detect no deficiency in the manuscript,
provide some feedback (e.g. "Great job.").');
    }
  } else {			# manuscript_is_closed
    print(txt('This manuscript is closed to further feedback.'),
	  br,br);
    if (defined $asn->{assignment_quality1} and
	defined $asn->{assignment_quality2}) {
      if (!defined $asn->{assignment_prefer_late} or $R->param('relate')) {
	print(txt('How do you want this assignment recorded?'),
	      start_form('address_assignment'),
	      hidden(id=>$id),
	      tag('table'),
	      Tr(td(radio(0, 'late')), td(hskip(2)),
		 td(label(0, txt('On time; '.REVISION_QUALITY->[$asn->{assignment_quality1}])))),
	      Tr(td(radio(1, 'late')), td(),
		 td(label(1, txt('Late; '.REVISION_QUALITY->[$asn->{assignment_quality2}])))),
	      gat('table'),
	      submit(gettext('Submit')),
	      gat('form'))
      } else {
	my $rec =
	  sprintf('You chose to record this assignment as %s; %s.',
		  do {
		    if (!$asn->{assignment_prefer_late}) {
		      ('on time',
		       REVISION_QUALITY->[$asn->{assignment_quality1}])
		    } else {
		      ('late',
		       REVISION_QUALITY->[$asn->{assignment_quality2}])
		    }
		  });
	if (!$asn->{assignment_archived}) {
	  print(leaf_plain(txt($rec),'address_assignment',id=>$id,relate=>1));
	} else {
	  print(txt($rec));
	}
      }
      print(br,br);
    }
  }
  if ($asn->{has_revision}) {
    print(leaf('Preview Revision', 'show_revision',
	       manuscript_id => $id, reviewer_id => member()));
  }
  if ($asn->{manuscript_archived} and $asn->{assignment_archived}) {
    my ($best_id, $bestq) = get_best_review($id, $quality, member());
    if ($bestq) {
      # If they click this before we send an email then we could avoid
      # sending an email. Such a circumstance is pretty unlikely though.
      print(br,br,
	   'The best review was rated "'.
	    REVISION_QUALITY->[$bestq].'."');
      if ($bestq == $quality) {
	# same quality
      } else {
	print(' You are welcome to
study this review in order that you may improve your reviews in the future.');
      }
      print(br,br,
	    leaf('Best Review', 'show_revision',
		 manuscript_id => $id, reviewer_id => $best_id));
    }
  }
  print(gat('td','tr','table'),
	tag('p', align => 'center'));
  if (defined $asn->{assignment_prefer_late} and
      !$asn->{assignment_archived}) {
    print(leaf('Archive', 'address_assignment', id=>$id, archive=>1).hskip(4));
  }
  print(leaf('Go Back', 'front'),
	gat('p'));
}

sub is_reviewer {
  my ($id) = @_;

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT count(*) FROM assignment
WHERE manuscript_id = ? and member_id = ? and assignment_for != ?
SQL
  $q1->execute($id, member(), ASSIGN_FOR_SCREENING);
  my $ok = $q1->fetchrow_array;
  $q1->finish;
  if (!$ok) {
    0;
  } else { 1 }
}

sub get_which_review {
  my ($id) = @_;

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT CASE WHEN manuscript_due < now() THEN 2 ELSE 1 END, manuscript_is_closed
FROM manuscript_tm
WHERE manuscript_id = ?
SQL
  $q2->execute($id);
  my ($version, $closed) = $q2->fetchrow_array;
  $q2->finish;

  if ($closed) {
    banner('Manuscript Closed');
    print(p(txt('You are out of time.')),
	  p(txt('The manuscript is closed to further feedback.')),
	  p({align=>'center'},
	    leaf('Continue', 'address_assignment', id => $id)));
    return;
  }
  $version;
}

sub edit_online {
  my ($class) = @_;

  my $id = $R->param('id');
  return $class->front() if !is_reviewer($id);

  my $version = get_which_review($id);
  return if !defined $version;

  my $prior = get_assignment_directory($id) . sprintf('/rev%d', $version);

  if ($R->param('review')) {
    open my $fh, ">$prior.txt" or do {
      warn "open >$prior.txt: $!";
      return $class->address_assignment();
    };
    print $fh $R->param('review');
    close $fh or do {
      warn "close >$prior.txt: $!";
    };
    html_escape_file($prior);
    return show_new_revision($class, $id, $version);
  }

  while (!-e "$prior.txt") {
    if (--$version == 1) {
      $prior = get_assignment_directory($id) . sprintf('/rev%d', $version);
    } else {
      $prior = get_job_directory($id) . '/orig';
      last
    }
  }

  banner('Edit Online');

  open my $fh, "$prior.txt" or do {
    warn "open $prior.txt: $!";
    print(div({ class => 'error' }, txt('An error was encountered
while accessing the manuscript or revision.')));
    return;
  };
  binmode $fh, ':utf8';
  my $blob = join '', <$fh>;

  print(EDITING_STYLE_HINTS,
	start_form('edit_online'),
	hidden(id => $id),
	textarea('review', $blob, 30, 80),
	p({align => 'center'}, submit(gettext('Submit'))),
	gat('form'));
}

sub upload_revision {
  my ($class) = @_;
  my @err;

  my $id = $R->param('id');
  return $class->front() if !is_reviewer($id);

  my $version = get_which_review($id);
  return if !defined $version;

  my $rev = sprintf 'rev%d', $version;

  my $dir = get_assignment_directory($id);
  my $filename = $R->param('revision');
  my $type = $R->param('type');
  if (receive_text_upload(\@err, $filename, $type, $dir, $rev)) {
    my %err;
    $err{msg} = join_remedy(@err);
    return $class->address_assignment(\%err);
  }
  show_new_revision($class, $id, $version);
}

sub show_new_revision {
  my ($class, $id, $version) = @_;

  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment
SET assignment_revtime[?] = now(), assignment_quality[?] = NULL
where manuscript_id = ? and member_id = ?
SQL
  $u1->execute($version, $version, $id, member());

  $R->param('manuscript_id' => $id);
  $R->param('reviewer_id' => member());
  $class->show_revision();
}

sub is_author_or_reviewer {
  my ($manuscript_id, $reviewer_id) = @_;

  my $q1;
  my $ok;
  if (!$reviewer_id) {
    $q1 = $DB->prepare_cached(<<SQL);
SELECT COUNT(*) FROM manuscript
WHERE manuscript_id = ? and member_id = ?
LIMIT 1
SQL
    $q1->execute($manuscript_id, member());
    $ok = $q1->fetchrow_array;
  } else {
    $q1 = $DB->prepare_cached(<<SQL);
SELECT a.member_id as reviewer, m.member_id as author
FROM assignment a
  JOIN manuscript m ON (m.manuscript_id = a.manuscript_id)
WHERE a.manuscript_id = ? AND a.member_id = ?
LIMIT 1
SQL
    $q1->execute($manuscript_id, $reviewer_id);
    my $who = $q1->fetchrow_hashref;
    if ($who) {
      if (member() == $who->{author}) { $ok='a' }
      elsif (member() == $who->{reviewer}) { $ok='r' }
      else {
	my $q2 = $DB->prepare_cached(<<SQL);
SELECT extract_assignment_quality(a.*)
FROM assignment a
WHERE manuscript_id = ? AND
  (SELECT manuscript_archived FROM manuscript WHERE manuscript_id = ?) AND
  member_id = ? AND assignment_archived AND
  assignment_for != ? AND
  assignment_cancelled IS NULL
SQL
        $q2->execute($manuscript_id, $manuscript_id, member(),
		     ASSIGN_FOR_SCREENING);
	my ($q) = $q2->fetchrow_array;
	$q2->finish;
	if ($q) {
	  my ($best_id) = get_best_review($manuscript_id, $q, member());
	  if ($best_id == $who->{reviewer}) { $ok='r' }
	}
      }
    }
  }
  $q1->finish;
  $ok;
}

sub shirk_assignment {
  my ($class) = @_;

  my $id = $R->param('id');
  my $u1 = $DB->prepare_cached(<<SQL);
update assignment a set assignment_cancelled = now(),
  assignment_archived = 't', assignment_value = NULL,
  assignment_revtime[1] = NULL
WHERE manuscript_id = ? AND member_id = ? AND a.assignment_for != ? AND
  now() < (SELECT manuscript_due FROM manuscript_tm m
           WHERE m.manuscript_id = a.manuscript_id);
SQL
  my $ok = $u1->execute($id, member(), ASSIGN_FOR_SCREENING);
  
  if ($ok) {
    $class->front();
  } else {
    $class->address_assignment();
  }
}

sub contact_reviewer {
  my ($class) = @_;
  my $manuscript_id = $R->param('manuscript_id');
  my $reviewer_id = $R->param('reviewer_id');

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT assignment_email_token, mem.member_email, m.manuscript_title
FROM assignment a
  JOIN manuscript m ON (a.manuscript_id = m.manuscript_id)
  JOIN member mem ON (m.member_id = mem.member_id)
WHERE assignment_contact = 't' and a.member_id = ? and a.manuscript_id = ?
  AND assignment_for != ? AND assignment_quality[1] IS NOT NULL
SQL
  $q1->execute($reviewer_id, $manuscript_id, ASSIGN_FOR_SCREENING);
  my $asn = $q1->fetchrow_hashref;
  $q1->finish;

  if (!$asn) {    # should be impossible
    return $class->show_revision();
  }

  my $tok = $asn->{assignment_email_token};
  if (!$tok) {
    $tok = gen_token(3);
    my $u1 = $DB->prepare_cached(<<SQL);
update assignment set assignment_email_token = ?
WHERE member_id = ? AND manuscript_id = ? AND assignment_for != ?
SQL
    $u1->execute($tok, $reviewer_id, $manuscript_id, ASSIGN_FOR_SCREENING);
  }

  my $addr = EMAIL_FROM;
  $addr =~ s/\@/-job-$tok\@/;

  my $body = sprintf(gettext('
With respect to your manuscript titled:

  %s

You may contact the reviewer by sending e-mail to:

  %s

or by simply replying to this message.'),
		     $asn->{manuscript_title}, $addr);

  sendmail_no_msgstamp($asn->{member_email},
		       'Re: '.truncate_to_chars($asn->{manuscript_title}, 60),
		       $body, $addr);

  banner('Contact Reviewer');

  my $m1 =
    sprintf(gettext('An e-mail has been sent to %s with instructions about how to contact your reviewer.'), $asn->{member_email});
  my $m2 = sprintf(gettext('If you do not receive any e-mail from us
within one or two hours then contact %s for assistance.'), EMAIL_FROM);

  print(br.div({ class => 'bigmsg' },
	       p(txt($m1)),
	       p(txt($m2))));
}

sub grok_diff_wait_status {
  my ($exe) = @_;
  if ($? == -1) {
    warn "Failed to execute $exe";
  } elsif ($? & 127) {
    warn sprintf "$exe died with signal %d", ($? & 127)
  } else {
    if ($? >> 8 == 2) {
      warn sprintf "$exe exit with %d", $? >> 8;
      return 1;
    }
  }
  0;
}

sub redress_cancellation {
  my ($class) = @_;
  
  my $manuscript_id = $R->param('manuscript_id');
  my $reviewer_id = $R->param('reviewer_id');
  my $quart = $R->param('quart');

  # keep in synch with PPX::Periodic::redress_cancelled_assignments
  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment
SET assignment_prefer_late = ?, assignment_value =
 -(select manuscript_wc from manuscript where manuscript_id = ?) * ? / 4
WHERE assignment_for != ? AND
  (SELECT manuscript_withdrawn IS NULL
   FROM manuscript
   WHERE manuscript_id = ?)
  AND
   manuscript_id = ? AND member_id = ?
SQL
  $u1->execute($R->param('late')? 't':'f', $manuscript_id, $quart,
	       ASSIGN_FOR_SCREENING, $manuscript_id, $manuscript_id,
	       $reviewer_id);

  $R->param('id' => $manuscript_id);
  $class->show_assignments();
}

sub _sr_cancelled {
  my ($manuscript_id, $reviewer_id, $asn) = @_;

  banner('Assignment Cancelled');

  my $info = sprintf(txt('After %s of %s, the reviewer has decided not
to edit your manuscript.'),
	  format_hours($asn->{cancelled_hours}),
	  format_hours($asn->{assignment_hours}));

  print(p($info),
	p(txt('How many words will you take as compensation?')),
	start_form('redress_cancellation'),
	hidden(manuscript_id => $manuscript_id, reviewer_id => $reviewer_id),
	tag('table'));

  my $quarter = int(4*(.2 + $asn->{cancelled_hours}/$asn->{assignment_hours}));
  _choose_percent_words($quarter, $asn->{manuscript_wc});

  print(Tr(td(checkbox('late')),
	   td(),
	   td({colspan=>3},
	      label('late', txt('Also record a missed deadline.')))),
	gat('table'),
	p({align => 'center'}, submit(gettext('Submit'))),
	gat('form'));
}

sub show_revision {
  my ($class) = @_;
  my $manuscript_id = $R->param('manuscript_id');
  my $reviewer_id = $R->param('reviewer_id');

  my $who = is_author_or_reviewer($manuscript_id, $reviewer_id);
  return $class->front()
    if !$who;

  my $version;
  if ($who eq 'a') {
    $version = $R->param('version') || 1;
  }

  my $quality = $R->param('quality');
  if (defined $quality and $who eq 'a') {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment SET assignment_quality[?] = ?
WHERE manuscript_id = ? and member_id = ? AND assignment_for != ? AND
  assignment_revtime[?] IS NOT NULL
SQL
    my @arg = ($version, $quality, $manuscript_id, $reviewer_id,
	       ASSIGN_FOR_SCREENING, $version);
    my $count = $u1->execute(@arg);
    if ($count != 1) { warn 'Wrong quality? '.join ' ', @arg }
  }

  my $jdir = get_job_directory($manuscript_id);
  my $rdir = get_assignment_directory($manuscript_id, $reviewer_id);

  my $show = $R->param('show') || 'wdiff';

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT assignment_quality[1] AS assignment_quality1,
  assignment_quality[2] AS assignment_quality2, assignment_email_token,
  assignment_contact, assignment_cancelled, manuscript_wc,
  manuscript_due < now() AS due, manuscript_archived, assignment_quality1_lock,
  extract_hours_from_interval(assignment_cancelled, assignment_ctime)
    AS cancelled_hours,
  GREATEST(1,extract_hours_from_interval(manuscript_due, assignment_ctime))
    AS assignment_hours,
  m.manuscript_is_closed,
  m.manuscript_withdrawn IS NOT NULL AS withdrawn
FROM assignment a
  JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE a.manuscript_id = ? and a.member_id = ? AND assignment_for != ?
SQL
  $q1->execute($manuscript_id, $reviewer_id, ASSIGN_FOR_SCREENING);
  my $asn = $q1->fetchrow_hashref;
  $q1->finish;

  return _sr_cancelled($manuscript_id, $reviewer_id, $asn)
    if $asn->{assignment_cancelled};

  banner('Show Revision');

  if ($who eq 'a' and !$asn->{due}) {
    # Should not reach here, but just in case.
    print(p('This assignment is not due yet.'),
	  div({align=>'center'}, leaf('Continue', 'front')));
    return;
  }

  if ($who eq 'a' and !defined $asn->{assignment_quality1} and
     -e "$rdir/rev1.txt" and $version == 2) {

    # This could be due to a dirty var/jobs area. If you load a new
    # database snapshot without periodic/scrub then you can get
    # random files all over the place.

    warn 'should not reach here, but just in case';
    $version = 1;
  }

  if ($who eq 'a' and $version == 2 and !$asn->{assignment_quality1_lock}) {
    $DB->do(<<SQL, {}, $manuscript_id, $reviewer_id);
UPDATE assignment SET assignment_quality1_lock = 't'
WHERE manuscript_id = ? and member_id = ?
SQL
  }

  my @arg = (manuscript_id=> $manuscript_id, reviewer_id=>$reviewer_id);
  push @arg, (version => $version)
    if $version;

  print(tag('table'),
	tag('tr'),
	tag('td'));

  if ($who eq 'a') {
    if ($asn->{withdrawn}) {
      print(p('This review may be incomplete since your manuscript was withdrawn.'),
	    div({align=>'center'},
		leaf('Go Back', 'show_assignments', id => $manuscript_id)));
    } elsif (!defined $asn->{"assignment_quality$version"} or
	$R->param('requality')) {
      print(txt("Examine the reviewer's feedback.
After studying the changes, rate the quality of this feedback."),
	    start_form('show_revision'),br,
	    hidden(@arg, show => $show),
	    join('',
		 map { radio($_,'quality').' '.
			     label($_,REVISION_QUALITY->[$_]).br}
		 reverse 0..3),
	    br,
	    div({align=>'center'},
		submit('Submit')),
	    gat('form'));
    } else {
      my $rating = sprintf(q[You rated the quality of the reviewer's
feedback as "%s."], REVISION_QUALITY->[$asn->{"assignment_quality$version"}]);

      my $contact ='';
      if (0 and $asn->{assignment_contact} and !$asn->{manuscript_is_closed}) {
	$contact =
	  join('',br,br,
	       tag('table'),
	       tag('tr'),
	       td(txt("If you have any questions about the reviewer's comments
then you may contact the reviewer by e-mail.")),
	       td(hskip(2)),
	       td(leaf('Contact Reviewer', 'contact_reviewer', @arg)),
	       td(hskip(2)),
	       gat('tr','table')
	      );
      }
      if (!$asn->{manuscript_archived} and
	  !($version == 1 and $asn->{assignment_quality1_lock})) {
	$rating = leaf_plain($rating, 'show_revision', requality=>1,
			     @arg, show => $show);
      }
      print($rating,
	    $contact,
	    br,br,
	    div({align=>'center'},
		leaf('Go Back', 'show_assignments', id => $manuscript_id)));
    }
  } else {
    print(p('This is what your revision will look like when viewed
by the author. Please take the time to examine it for clarity. Even
sage advice can benefit from good presentation.'),
          EDITING_STYLE_HINTS, EDITING_BUTTON_HINTS,
          p({align=>'center'},
	    leaf('Continue', 'address_assignment', id=>$manuscript_id)));
  }

  print(gat('td'),
	td({ align => 'center' },
	   tag('div', style => 'padding: 1em; border: thin dotted gray;'),
	   join(br.br,
		leaf('Original', 'show_revision', show => 'orig', @arg),
		leaf('Changes', 'show_revision', show => 'wdiff', @arg),
		leaf('Revised', 'show_revision', show => 'rev', @arg)),
	   gat('div'),
	  ),
	gat('tr','table'),
	hr,
	tag('pre'));

  if ($who eq 'r') {
    if (-e "$rdir/rev2.html") {
      $version = 2;
    } else {
      $version = 1;
    }
  }
  my $rev = sprintf("$rdir/rev%d.html", $version);

  my $diff_error;
  if ($show eq 'orig' or $show eq 'rev') {
    print(tag('div', style => 'float: right; margin: 1em; font-size: 70%; text-align: center;'),
	  leaf('Download', 'download', @arg,
	       which => ($show eq 'orig'? $show : 'r'.$version)),
	  gat('div'));

    my $name = $show eq 'orig'? "$jdir/orig.html" : $rev;
    open my $fh, $name or do {
      warn "open $name: $!";
      print(div({ class => 'error' }, txt('An error was encountered
while accessing the manuscript or revision.')));
      return;
    };
    binmode $fh, ':utf8';
    for (my $x=1; defined(my $l = <$fh>); $x++) {
      printf "%03d: %s", $x, $l;
    }      
  } elsif ($show eq 'wdiff') {
    my $mp = $R->_mod_perl_request;
    my @arg = ('-n',
	       '--start-insert', tag('span', class=>'insert'),
	       '--end-insert', gat('span'),
	       '--start-delete', tag('span', class=>'delete'),
	       '--end-delete', gat('span'),
	       "$jdir/orig.html", $rev);
    my $exe = PROG_WDIFF;
    my $out_fh = $mp->spawn_proc_prog($exe,\@arg);
    binmode $out_fh, ':utf8';
    while (1) {
      if (PERLIO_IS_ENABLED || IO::Select->new($out_fh)->can_read(10)) {
	my $l = <$out_fh>;
	last if !defined $l;
	my $bar = '  ';
	if ($l =~ /\</) {
	  $bar = span({style=>'background: yellow;'}, $bar);
	}
	# If the span extends to the end of the line then wdiff
	# appears to add a \r after the span. Maybe this
	# is a wdiff bug? Just use a big hammer:
	$l =~ s/[\r\n]//g;
	printf "%s%s\n", $bar, $l;
      }
    }
    wait;
    $diff_error = grok_diff_wait_status($exe)
  } else {
    my $mp = $R->_mod_perl_request;
    my @arg = ('-b', '-B', '--text', '--unified=4', '--minimal',
	       "$jdir/orig.html", $rev);
    my $exe = PROG_DIFF;
    my $out_fh = $mp->spawn_proc_prog($exe,\@arg);
    binmode $out_fh, ':utf8';
    my $x=0;
    while (1) {
      if (PERLIO_IS_ENABLED || IO::Select->new($out_fh)->can_read(10)) {
	my $l = <$out_fh>;
	last if !defined $l;
	next if ++$x <= 2;
	my $c = substr($l,0,1);
	if ($c eq ' ') {
	} elsif ($c eq '+') {
	  $l = span({class=>'insert'}, $l);
	} elsif ($c eq '-') {
	  $l =~ s/^-/ /; #otherwise looks funny
	  $l = span({class=>'delete'}, $l);
	} elsif ($c eq '@') {
	  $l = span({style=>'background: yellow;'}, $l);
	} else {
	  warn $c;
	}
	print $l;
      }
    }
    wait;
    $diff_error = grok_diff_wait_status($exe)
  }
  print(gat('pre'));
  if ($diff_error) {
    print(div({ class => 'error' }, txt('An error was encountered
while calculating differences.')));
  }

  if ($who eq 'r') {
    my $q2 = $DB->prepare_cached(<<SQL);
SELECT manuscript_is_closed FROM manuscript_tm
WHERE manuscript_id = ?
SQL
    $q2->execute($manuscript_id);
    my ($closed) = $q2->fetchrow_array;
    $q2->finish;

    if (!$closed) {
      print(hr,
            div({ class => 'section' }, txt('Revise')),
            tag('p'),
            tag('table'),
            Tr(td(leaf('Edit Online', 'edit_online', id=>$manuscript_id)),
               td(hskip(8).'OR'.hskip(8)),
               td(join('',
	               start_form('upload_revision'),
	               hidden(id=>$manuscript_id),
	               upload('revision'),br,br,
	               ask_file_format('type'),br,br,
	               submit('Upload Revision'),
	               gat('form')))),
            gat('table','p')
	   );
    }
  }
}

sub redress_withdrawl {
  my ($class) = @_;
  my $id = $R->param('id');
  my $quart = $R->param('quart');
  
  # keep in synch with PPX::Periodic::archive_old_transactions
  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE assignment
SET assignment_archived = 't', assignment_value =
 (select manuscript_wc from manuscript where manuscript_id = ?) * ? / 4
WHERE assignment_for != ? AND
  (SELECT manuscript_withdrawn IS NOT NULL
   FROM manuscript
   WHERE manuscript_id = ?)
  AND
   manuscript_id = ? AND member_id = ?
SQL
  $u1->execute($id, $quart, ASSIGN_FOR_SCREENING, $id, $id, member());

  $class->front();
}

sub show_assignments {
  my ($class) = @_;
  my $id = $R->param('id');

  my $enough = $R->param('enough');
  if ($enough) {
    my $u1 = $DB->prepare(<<SQL);
UPDATE manuscript
SET manuscript_min_reviews = GREATEST(1, LEAST(?, manuscript_min_reviews))
WHERE manuscript_id = ? AND member_id = ?
SQL
    $u1->execute($enough, $id, member());
  }

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, manuscript_max_reviews, manuscript_due < now() AS ready,
  manuscript_due AT TIME ZONE ? as manuscript_due,
  manuscript_hours_remain, manuscript_writing, manuscript_flagged,
  manuscript_withdrawn, manuscript_is_closed, manuscript_archived,
  manuscript_wc, manuscript_min_reviews
FROM manuscript_tm
where manuscript_id = ? and member_id = ?
SQL
  $q1->execute(member('member_tz'), $id, member());
  my $m = $q1->fetchrow_hashref;
  $q1->finish;

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT a.member_id, assignment_quality[1] AS assignment_quality1,
  assignment_quality[2] AS assignment_quality2,
  assignment_cancelled, assignment_value,
  assignment_prefer_late, assignment_revtime[1] AS assignment_rev1time,
  assignment_revtime[2] AS assignment_rev2time,
  COALESCE(assignment_revtime[1], assignment_revtime[2]) AS has_revision,
  extract_hours_from_interval(assignment_revtime[2], manuscript_due)
    AS hours_late, assignment_for
FROM assignment a
  JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE a.manuscript_id = ? AND m.member_id = ?
ORDER BY assignment_ctime
SQL
  $q2->execute($id, member());
  my $got=0;
  my @r;
  while (defined(my $r = $q2->fetchrow_hashref)) {
    ++$got if (!defined $r->{assignment_cancelled} and
	       $r->{assignment_for} != ASSIGN_FOR_SCREENING and
	       (!$m->{ready} or $r->{has_revision}));
    push @r, $r;
  }

  my @msg;
  my $archive = $R->param('archive');
  if ($archive and $m->{manuscript_is_closed}) {
    for (my $ax=1; $ax <= @r; $ax++) {
      my $asn = $r[$ax-1];
      next if $asn->{assignment_for} == ASSIGN_FOR_SCREENING;
      if ($asn->{assignment_cancelled}) {
	if (!defined $asn->{assignment_value} or
	    !defined $asn->{assignment_prefer_late}) {
	  push @msg, "Assign a penalty to reviewer #$ax.";
	}
      } elsif (($asn->{assignment_rev1time} and
	   !defined $asn->{assignment_quality1})
	  or ($asn->{assignment_rev2time} and
	      !defined $asn->{assignment_quality2})) {
	push @msg, "Rate the quality of feedback from reviewer #$ax.";
      }
    }
    if (!@msg) {
      my $u1 = $DB->prepare_cached(<<SQL);
update manuscript set manuscript_archived = 't' where manuscript_id = ?
SQL
      $u1->execute($id);
      return $class->front();
    }
  }

  banner('Reviews');

  print(div({ class => 'error' }, join_remedy(@msg)))
    if @msg;

  my $status = sprintf('Reviews: want %d, got %d (minimum %d)',
		       $m->{manuscript_max_reviews}, $got,
		       $m->{manuscript_min_reviews});
  if (grep { $_->{assignment_for} == ASSIGN_FOR_VETTING and
	       !defined $_->{assignment_cancelled } } @r) {
    $status =~ s/ /\x{a0}/g;
    $status = join('',
		   tag('table'),
		  Tr(td($status),
		     td(hskip(4)),
		     td(small('(As a competent reviewer, you have earned the
duty of vetting new members. The word-count for extra reviews will
not be deducted from your balance.)'))),
		   gat('table'));
  }

  print(p(txt('Title: '.$m->{manuscript_title})));

  if ($m->{manuscript_flagged} and $m->{manuscript_flagged} == FLAG_SPAM) {
    print(p('This manuscript was flagged as spam.'),
	  p('If you believe this occurred by error then you can resubmit.'));
    return;
  }
  
  print(p('Intended audience: '.
	  txt(WRITING_LEVEL->[$m->{manuscript_writing}-1])));
  print p($status);

  if ($m->{manuscript_withdrawn}) {
    print(p(txt('Your manuscript has been withdrawn.')),
	  p({align=>'center'}, leaf('Go Back', 'front')));
    return;
  }

  if (!$m->{ready}) {
    my $info = sprintf('Reviews will be available at %s (in %s).',
		       substr($m->{manuscript_due}, 0, 16),
		       format_hours($m->{manuscript_hours_remain}));
    print(p(txt($info)),
	  tag('p'),
	  tag('table'));

    if ($got and $got < $m->{manuscript_min_reviews}) {
      my $min = $m->{manuscript_min_reviews};
      my $plur = $got>1?'s':'';
      print(Tr(td("The time allowance for your manuscript will be extended
until your manuscript attracts the attention of $min reviewers. If you are
tired of waiting, you can proceed with only $got reviewer$plur."),
	      td(leaf("Enough",
		      'show_assignments', id=>$id, enough=>$got))));
    }

    print(Tr(td(txt('You can withdraw the manuscript and be liable for only
a portion of the word-count. You may wish to withdraw if you have made
significant changes to the manuscript since you submitted it or for some
other reason you anticipate that the feedback will not be useful.')),
	     td(leaf('Withdraw Manuscript',
		     'withdraw_manuscript', id => $id, confirmed => 1))),
	  gat('table','p'),
	  p({align=>'center'}, leaf('Go Back', 'front')));
    return;
  }

  if (@r) {
    my $superhead =
      'border-bottom: medium solid gray; border-top: medium solid gray;';
    print(tag('p'),
	  tag('table'),
	  Tr(td({colspan => 3, style => $superhead }, 'On Time'),
	     td(hskip(2)),
	     td({colspan => 3, style => $superhead }, 'Late')),
	  Tr(join(th(hskip(2)),
		  th('Review'),
		  th('Quality')),
	     td(),
	     join(th(hskip(2)),
		  th('Updated'),
		  th('Quality'))),
	 );
  
    my $rx = 0;
    for my $r (@r) {
      next if $r->{assignment_for} == ASSIGN_FOR_SCREENING;
      my @arg = (manuscript_id => $id, reviewer_id => $r->{member_id});
      my $has_version1 = defined $r->{assignment_rev1time};
      my $has_version2 = defined $r->{assignment_rev2time};
      my $name = txt('#' . ++$rx);
      my $leaf1;
      my $qual1 = '-';
      if (defined $r->{assignment_cancelled} or
	 ($m->{manuscript_is_closed} and !$has_version1 and !$has_version2)) {
	if (defined $r->{assignment_cancelled} and
	    !$m->{manuscript_archived}) {
	  $leaf1 = 1;
	}
	if (defined $r->{assignment_value}) {
	  $qual1 = "cancel ($r->{assignment_value} words";
	  $qual1 .= " & late"
	    if $r->{assignment_prefer_late};
	  $qual1 .= ')';
	  $qual1 = txt($qual1);
	} else {
	  $qual1 = 'cancelled';
	  $leaf1 = 'e';
	}
      } elsif ($has_version1) {
	$leaf1 = 1;
	$qual1 = do {
	  if (!defined $r->{assignment_quality1}) {
	    $leaf1 = 'e';
	    'unevaluated';
	  } else {
	    txt(REVISION_QUALITY->[$r->{assignment_quality1}]);
	  }
	};
      }
      if ($leaf1) {
	if ($archive and $leaf1 eq 'e') {
	  # improve leaf_error API TODO
	  $name = leaf_error($name, 'show_revision', @arg, version => 1);
	  $qual1 = leaf_error($qual1, 'show_revision', @arg, version => 1);
	} else {
	  $name = leaf_plain($name, 'show_revision', @arg, version => 1);
	  $qual1 = leaf_plain($qual1, 'show_revision', @arg, version => 1);
	}
      }

      my $leaf2;
      my $updated = '-';
      my $qual2 = '-';
      if ($has_version2) {
	$updated = txt('+'.format_hours($r->{hours_late}));

	if (defined $r->{assignment_quality1} or !$has_version1) {
	  $leaf2 = 1;
	}
      }
      if ($has_version2 or defined $r->{assignment_quality2}) {
	$qual2 = do {
	  if (!defined $r->{assignment_quality2}) {
	    $leaf2 = 'e'
	      if $leaf2;
	    'unevaluated';
	  } else {
	    txt(REVISION_QUALITY->[$r->{assignment_quality2}]);
	  }
	};
      }
      if ($leaf2) {
	if ($archive and $leaf2 eq 'e') {
	  $updated = leaf_error($updated, 'show_revision', @arg, version => 2);
	  $qual2 = leaf_error($qual2, 'show_revision', @arg, version => 2);
	} else {
	  $updated = leaf_plain($updated, 'show_revision', @arg, version => 2);
	  $qual2 = leaf_plain($qual2, 'show_revision', @arg, version => 2);
	}
      }

      print(Tr(td({align => 'center'}, $name),
	       td(),
	       td($qual1),
	       td(),
	       td({align => 'center'}, $updated),
	       td(),
	       td($qual2),
	      ));
    }

    print(gat('table','p'));
  }

  if (1) {
    my $full=0;
    my $total=0;
    my $extra=0;
    my $rebate=0;
    for my $asn (@r) {
      next if (!defined $asn->{assignment_value} or
	       $asn->{assignment_value} < 0 or
	       $asn->{assignment_for} == ASSIGN_FOR_VETTING or
	       defined $asn->{assignment_cancelled});
      if ($asn->{assignment_for} == ASSIGN_FOR_EDITING and
	  $asn->{has_revision}) {
	$rebate += $m->{manuscript_wc} - $asn->{assignment_value};
	++$full;
      } elsif ($asn->{assignment_for} == ASSIGN_FOR_SCREENING) {
	$extra += $asn->{assignment_value}; # screening
      }
      $total += $asn->{assignment_value};
    }
    my $cost = "Cost: $full reviews * $m->{manuscript_wc} words";
    if ($rebate) {
      $cost .= " - $rebate rebate";
    }
    if ($extra) {
      $cost .= " + $extra words for screening"
    }
    print(p($cost . " = $total words"));
  }

  print(p('The best way to insure that you receive useful reviews
is to give them to others. Once you build your reputation as a good
reviewer then the system will match your manuscripts with other
good reviewers.'));

  my $archive_msg='';

  if (!$m->{manuscript_is_closed}) {
    if (!$got) {
      print(p('Reviewers will received a missed deadline remark on
their record.'));
    }
    print(p('Until the manuscript is closed, reviewers
may continue to refine late reviews.'));
  } else {
    print(p(txt('This manuscript is closed. You will receive no further
feedback from the reviewers.')));
    if (!$got) {
      print(p('Unfortunately, you did not receive any reviews.
Reviewers will received a missed deadline remark on their records.'));
    }
    if (!$m->{manuscript_archived}) {
      $archive_msg = leaf('Archive', 'show_assignments', id => $id,
			  archive => 1).hskip(4);
    }
  }

  print(p({align=>'center'},
	  $archive_msg.leaf('Go Back', 'front')));
}

sub front {
  my ($class, $err) = @_;
  $err ||= {};

  set_timezone();
  return query_tz1() if !member('member_tz');

  if (!member('has_chosen_criteria')) {
    if (1) {
      $DB->do(<<SQL, {}, member());
INSERT INTO member_criteria (member_id, criteria_id)
values (?,
  (SELECT c.criteria_id
   FROM criteria c
   WHERE c.criteria_name = 'all' AND c.member_id IS NULL))
SQL
    } else {
      return $class->choose_criteria('front');
    }
  }

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_submitted AT TIME ZONE ? as manuscript_submitted,
  manuscript_withdrawn, manuscript_hours_remain,
  manuscript_due < now() as ready,
  m.manuscript_id, manuscript_title,
  manuscript_max_reviews,
  (SELECT COUNT(a.member_id) FROM assignment a
   WHERE a.manuscript_id = m.manuscript_id AND
     a.assignment_cancelled IS NULL AND
     a.assignment_for != ?) AS assignments,
  get_manuscript_status(m.*) AS status
FROM manuscript_tm m
WHERE m.member_id = ? AND m.manuscript_archived = 'f'
ORDER BY manuscript_submitted, manuscript_title
SQL
  $q1->execute(member('member_tz'), ASSIGN_FOR_SCREENING, member());
  my @m;
  while (defined(my $m = $q1->fetchrow_hashref)) { push @m, $m }

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT m.manuscript_id, manuscript_title, manuscript_wc,
  manuscript_due AT TIME ZONE ? AS manuscript_due,
  manuscript_close_time AT TIME ZONE ? AS manuscript_close_time,
  manuscript_due < now() AS overdue,
  manuscript_is_closed,
  assignment_for = ? AS assignment_for_screening,
  extract_hours_from_interval(manuscript_due, now()) as hours_till_due,
  extract_hours_from_interval(manuscript_close_time, now())
    AS hours_till_close,
  extract(minute from assignment_ctime + interval '1 hour' - now())
    AS min_remaining,
  manuscript_withdrawn
FROM assignment a
  JOIN manuscript_tm m ON (a.manuscript_id = m.manuscript_id)
WHERE a.member_id = ? AND a.assignment_archived = 'f'
ORDER BY manuscript_due
SQL
  $q2->execute(member('member_tz'), member('member_tz'),
	       ASSIGN_FOR_SCREENING, member());
  my @asn;
  while (defined(my $m = $q2->fetchrow_hashref)) { push @asn, $m }

  banner('Status');

  print(div({ class => 'error' }, $err->{msg}))
    if $err->{msg};

  print(div({ class => 'error' }, 'Your current e-mail address is bouncing.
Please update your e-mail address.'))
    if member('member_bouncing');

  print(tag('div', style => 'float: right; font-size: 70%; padding: .5ex; text-align: center; border: thin dashed pink; background: yellow;'),
	txt('Balance: '),
	leaf_plain(query_current_balance(), 'show_history'),br,
	'Credit limit: '.member('member_credit_limit'),
	gat('div'),
	div({ class => 'section' }, txt('Your Manuscripts')));

  if (@m) {
    print(tag('p'),tag('table'),
	  Tr(join(th(hskip(2)),
	     th(),
	     th('Title'),
	     th('Reviews'),
	     th('Status')))
	 );
    for my $m (@m) {
      my $stime = $m->{manuscript_submitted};
      my $stopsign = '';
      if ($stime and !$m->{ready}) {
	$stopsign = leaf_plain(libimg('stopsign.png'),
			       'withdraw_manuscript', id=>$m->{manuscript_id});
      }
      my $title = truncate_to_chars($m->{manuscript_title}, 60);
      if (!$title) {
	$title = span({ class=>'error'}, txt(gettext('(no title)')));
      }
      $title = leaf_plain($title, $stime? 'show_assignments' :'reg_manuscript',
			  id=>$m->{manuscript_id});
      my $submit;
      if ($m->{manuscript_withdrawn}) {
	$submit = 'withdrawn'
      } elsif ($stime) {
	if ($m->{ready}) {
	  $submit = span({class=>'error'}, 'Done')
	} else {
	  $submit = $m->{status};
	  $submit .= ' ('.format_hours($m->{manuscript_hours_remain}).')';
	}
      } else {
	$submit = leaf_plain('Submit', 'presubmit',
			     id=>$m->{manuscript_id});
      }
      print(Tr(td($stopsign), td(),
	       td($title), td(),
	       td({ align=>'right'},
		  $m->{assignments}.' / '.$m->{manuscript_max_reviews}),td(),
	       td({ align=>'right'}, $submit),
	      ));
    }
    print(gat('table','p'));
  }

  print(p(leaf('Register Manuscript', 'reg_manuscript').
	 ' Have a manuscript you want somebody to edit?'));

  print(p(hskip(1)), div({ class => 'section' }, txt('Your Assignments')));

  if (@asn) {
    print(tag('p'),
	  tag('table'),
	  Tr(join(th(hskip(2)),
		  th('Title'),
		  th('Words'),
		  th('Due'))),
	 );
    for my $m (@asn) {
      my $due;
      if ($m->{manuscript_withdrawn}) {
	$due = 'withdrawn';
      } else {
	if ($m->{assignment_for_screening}) {
	  $due = abs($m->{min_remaining}).' min';
	  $due = span({class=>'error'}, $due)
	    if $m->{min_remaining} < 0;
	} elsif ($m->{manuscript_is_closed}) {
	  $due = 'closed';
	} else {
	  my ($t,$h);
	  if (!$m->{overdue}) {
	    $t = $m->{manuscript_due};
	    $h = $m->{hours_till_due};
	  } else {
	    $t = $m->{manuscript_close_time};
	    $h = $m->{hours_till_close};
	  }
	  $due = (substr($t,0,16).' ('.format_hours($h).')');
	  $due = span({class=>'error'}, $due)
	    if $m->{overdue};
	}
      }
      print(Tr(td(leaf_plain(truncate_to_chars($m->{manuscript_title}, 60),
			     'address_assignment',id => $m->{manuscript_id})),
	       td(),
	       td({ align=>'right'}, $m->{manuscript_wc}),td(),
	       td({ align=>'right'}, $due),
	      ));
    }
    print(gat('table','p'));
  }

  print(p(leaf('Search', 'find_manuscript').
	 ' Find a suitable manuscript to edit.'));
  if (member('member_adult')) {
    my $have = query_num_screening_assignments();
    if ($have < SCREENINGS_PER_MANUSCRIPT*3) {
      my $avail = query_available_for_screening();
      if (@$avail) {
	print(p(leaf('Screen', 'find_screening_needed').
		' Select a manuscript for screening.'));
      }
    }
  }
  if (member('member_flag_admin')) {
    my $q3 = $DB->prepare_cached(<<SQL);
SELECT manuscript_id, manuscript_title,
  extract_hours_from_interval(now(), manuscript_submitted) AS elapsed
FROM manuscript
WHERE manuscript_submitted IS NOT NULL AND manuscript_flagged IS NULL AND
  extract_hours_from_interval(now(), manuscript_submitted) >= ?
ORDER BY extract_hours_from_interval(now(), manuscript_submitted) DESC
SQL
    $q3->execute(HOURS_BEFORE_SCREENING_ESCALATION);
    my @m;
    while (defined(my $m = $q3->fetchrow_hashref)) { push @m, $m }
    if (@m) {
      print(p(hskip(1)),
	    div({ class => 'section' }, txt('Pending Unapproved Manuscripts')),
	    tag('p'),
	    tag('table'),
	    Tr(join(th(hskip(2)),
		    th('Title'),
		    th('Since'))));
      for my $m (@m) {
	my $title = txt(truncate_to_chars($m->{manuscript_title}, 60));
	print(Tr(join(td(), td(leaf_plain($title, 'approve_manuscript',
					  id=>$m->{manuscript_id})),
		      td(format_hours($m->{elapsed})))));
      }
      print(gat('table', 'p'));
    }
  }
  my @extra;
  if (member('member_admin')) {
    push @extra, td(leaf('Switch User', 'switch_user'));
    push @extra, td(leaf('Bounce', 'bounce_email'));
  }
  print(p(hskip(1)), div({ class => 'section' }, txt('Your Account')),
	tag('p'),
	tag('table'),
	Tr(join(td(hskip(4)),
		td(leaf('History', 'show_history')),
		td(leaf('Time Zone', 'query_tz1')),
		td(leaf('Password', 'chg_passwd')),
		td(leaf('E-mail', 'chg_email')),
		@extra
	       )),
	gat('table'),
	gat('p'),
	tag('p'),
	tag('table'),
	Tr(join(td(hskip(4)),
		td(leaf('Exchange Statistics', 'stats'))
	       )),
	gat('table'),
	gat('p'),
       );
}

sub stats {
  banner('Statistics');

  my $q2 = $DB->prepare(<<SQL);
SELECT quality, count(quality)
FROM (SELECT COALESCE(
       (SELECT MAX(COALESCE(extract_assignment_quality(asn.*), -1))
        FROM assignment asn
        WHERE asn.manuscript_id = m.manuscript_id AND
          asn.assignment_archived AND
          asn.assignment_for != ? AND
          (asn.assignment_revtime[1] IS NOT NULL OR
           asn.assignment_revtime[2] IS NOT NULL) AND
          asn.assignment_cancelled IS NULL), -1) AS quality
     FROM manuscript m
     WHERE manuscript_submitted IS NOT NULL AND manuscript_archived AND
       manuscript_withdrawn IS NULL) AS data
GROUP BY data.quality
ORDER BY quality
SQL
  $q2->execute(ASSIGN_FOR_SCREENING);

  print(tag('p'),
	tag('table'),
	tag('tr'),
	tag('td'),
	p({align=>'center'}, 'Feedback Quality by Manuscript'),
	tag('table'),
	Tr(th('Code'), th('Quality'), th(hskip(4)), th('Manuscripts')));
  while (my ($q, $c) = $q2->fetchrow_array) {
    my $qual = do {
      if ($q == -1) { 'no review proffered' }
      else { REVISION_QUALITY->[$q] }
    };
    print(Tr(td($q), td($qual), td(),
	     td({ align => 'right' }, $c)));
  }
  print(gat('table','td'),
	td(hskip(8)),
	tag('td'),
	img('static/hours.png', 'Hours per Review'),
	gat('td','tr'));
  print(gat('table','p'),
	p('
Each manuscript is counted once. The quality rating for a given
manuscript is the highest rating given to any of the reviewers by the author.
Feedback quality by manuscript is the most important
benchmark because it is like a measure of customer satisfaction.'),
	hr);

  print(p({ align => 'center' }, img('static/members.png', 'Active Members')),
	p('This graph counts members who have visited the site
within the last 90 days.'),
	p({ align => 'center' }, img('static/volume.png', 'Trading Volume')),
	p({ align => 'center' },
	  img('static/words_per_member.png', 'Words Per Member')));

  print(p({align=>'center'},
	  leaf('Go Back', 'front')));
}

sub show_history {
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT * FROM
(SELECT m.manuscript_id, manuscript_archived as archived, manuscript_title,
  COALESCE(manuscript_withdrawn, manuscript_approved, manuscript_submitted)
    AT TIME ZONE ? AS when,
  manuscript_max_reviews,
  (SELECT count(a.member_id) FROM assignment a
   WHERE a.manuscript_id = m.manuscript_id AND
     a.assignment_cancelled IS NULL AND a.assignment_for = ?)
     AS assignments,
  COALESCE(
   (SELECT -SUM(a.assignment_value) FROM assignment a
    WHERE a.manuscript_id = m.manuscript_id AND
      a.assignment_value > 0 AND a.assignment_for != ?), 0)
     AS nominal_value,
  get_manuscript_status(m.*) AS status,
  now() < manuscript_due AND manuscript_withdrawn IS NULL AS pending,
  manuscript_wc
FROM manuscript_tm m
WHERE m.member_id = ? and manuscript_submitted IS NOT NULL

UNION ALL

SELECT a.manuscript_id, a.assignment_archived,
  (SELECT manuscript_title FROM manuscript m
   WHERE a.manuscript_id = m.manuscript_id),
  COALESCE(assignment_cancelled,
           (SELECT manuscript_withdrawn FROM manuscript m
            WHERE a.manuscript_id = m.manuscript_id),
           assignment_ctime)
    AT TIME ZONE ? as when,
  NULL, NULL, COALESCE(assignment_value,0),
  get_assignment_status(a.*) AS status,
  (SELECT manuscript_escrowed FROM manuscript_tm m
   WHERE m.manuscript_id = a.manuscript_id), NULL
FROM assignment a
WHERE a.member_id = ?) AS data

ORDER BY data.when
SQL
  my $q2 = do {
    my @sql;
    my @arg;
    for my $l (1 .. NUM_WRITING_LEVELS) {
      push(@sql, "round(member_lang_quality[$l] * ?) as q$l",
           "member_lang_samples[$l] as s$l");
      push @arg, MAX_REVISION_QUALITY;
    }
    my $sql = join ',', @sql;
    my $tmp = $DB->prepare_cached(<<SQL);
select $sql
from member_lang
where member_id = ? and language_id = ?
SQL
    $tmp->execute(@arg, member(), LANG_ENGLISH);
    $tmp
  };
  my $ml = $q2->fetchrow_hashref;
  $q2->finish;

  banner('History');

  print(div({ class => 'section' }, txt('Quality of Feedback')));
  if (!$ml) {
    print(p('You have not completed any assignments yet. Quality
ratings are reflected here only after the manuscript and all
associated assignments are archived.'));
  } else {
    print(p('Every time you review a manuscript, the manuscript author
rates the quality of your feedback. All these ratings are combined
together to form a composite score (shown below). Each writing level
is considered separately.'),
	  p('Until
the system has confidence in your performance, your score will be
a mixture of your ratings and the average ratings of all members. '.
	    a({ href => '/static/faq.html' }, 'Refer to the FAQ for a
detailed description of the calculations involved.')),
	  tag('table'),
	  Tr(join(th(hskip(4)),
		  th('Audience'),
		  th('Revision Quality'),
		  th('Confidence'))));

    for my $l (1 .. NUM_WRITING_LEVELS) {
      my $conf = do {
	my $s = $ml->{"s$l"} || 0;
	my $pct = $s / SENIORITY_THRESHOLD;
	if ($pct <= 1) {
	  span({class=>'error'}, sprintf '%d%%', 100 * $pct)
	} else {
	  'ok'
	}
      };
      next if !defined $ml->{"q$l"};
      print(Tr(join(td(),
		    td(WRITING_LEVEL->[$l-1]),
		    td(REVISION_QUALITY->[$ml->{"q$l"}]),
		    td({ align => 'right' }, $conf),
		   )));
    }
    print(gat('table'));
  }
  print(div({ class => 'section' }, txt('Word Count')));

  my $total = member('member_balance');
  my $escrowed = 0;

  $q1->execute(member('member_tz'), ASSIGN_FOR_EDITING, ASSIGN_FOR_VETTING,
	       member(), member('member_tz'), member());
  my @r;
  while (defined (my $r = $q1->fetchrow_hashref)) {
    my $value;
    if (defined $r->{assignments}) {
      $value = $r->{nominal_value};
      $value -= ($r->{manuscript_wc} *
		 ($r->{manuscript_max_reviews} - $r->{assignments}))
	if $r->{pending};
    } else {
      if (!$r->{pending}) {
	$value = $r->{nominal_value};
      } else {
	$escrowed += $r->{nominal_value};
      }
    }

    $r->{value} = $value;
    $total += $value if defined $value;
    $r->{balance} = $total;
    push @r, $r;
  }

  print(tag('p'),
	txt('Your word-count balance is '),
	span({id=>'balance'}, $total),'. ');
  if ($escrowed) {
    print("In escrow are $escrowed words. ");
  }
  print('Your credit limit is '.member('member_credit_limit').'.',
	gat('p'),
	tag('table'),
	Tr(join(th(hskip(2)),
#	       th('A/P'),
		th('When'),
		th('Title'),
		th('Status'),
		th('Value'),
		th('Balance'),
	       )));

  for my $r (reverse @r) {
    my $title = txt(truncate_to_chars($r->{manuscript_title}, 60));

    if (defined $r->{assignments}) {
      $title = leaf_plain($title, 'show_assignments',
			  id => $r->{manuscript_id});
    } else {
      $title = leaf_plain($title, 'address_assignment',
			  id => $r->{manuscript_id});
    }
    my @style;
    my $value = do {
      if (defined $r->{assignments}) {
	$r->{value}
      } else {
	@style = (style => 'color: gray')
	  if $r->{pending};
	$r->{nominal_value}
      }
    };

    print(Tr(join(td(),
		  td(substr($r->{when}, 0, 16)),
		  td($title),
		  td($r->{status}),
		  td({ align=>'right', @style}, defined $value? $value:'?'),
		  td({ align=>'right'}, $r->{balance}),
		 )));
  }

  print(Tr(join(td(),
		td(substr(member('member_cdate'), 0, 10)),
		td(i(txt('initial balance'))),
		td(),
		td(),
		td({ align=>'right'}, member('member_balance')),
	       )));

  print(gat('table'),
	p({align=>'center'},
	  leaf('Go Back', 'front')));
}

package PPX::FlagAdmin::Impl;

sub approve_manuscript {
  my $id = $R->param('id');
  my $flag = $R->param('flag');

  if (defined $flag) {
    $DB->do('SELECT flag_manuscript(?,?)', {}, $id, $flag);
    manuscript_flagged($id, $flag);
  }

  my $q1 = $DB->prepare_cached(<<SQL);
SELECT assignment_ctime AT TIME ZONE ? AS assignment_ctime, assignment_flag,
  extract_hours_from_interval(
    (SELECT manuscript_submitted FROM manuscript m
     WHERE m.manuscript_id = a.manuscript_id), assignment_ctime) AS elapsed
FROM assignment a
WHERE assignment_for = ? AND manuscript_id = ?
SQL
  my $q2 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, manuscript_flagged, manuscript_writing,
  manuscript_submitted AT TIME ZONE ? AS manuscript_submitted,
  extract_hours_from_interval(COALESCE(manuscript_approved,
    manuscript_withdrawn, now()), manuscript_submitted) AS elapsed,
  member_email, manuscript_vulgar
FROM manuscript NATURAL JOIN member
WHERE manuscript_id = ?
SQL
  $q1->execute(member('member_tz'), ASSIGN_FOR_SCREENING, $id);
  $q2->execute(member('member_tz'), $id);
  my $m = $q2->fetchrow_hashref;
  $q2->finish;

  banner('Flag Manuscript');

  print(p('Title: '.txt($m->{manuscript_title})),
	p('Audience: '.WRITING_LEVEL->[$m->{manuscript_writing}-1]),
	p('Author: '. txt($m->{member_email})),
	p('Vulgar: '. ($m->{manuscript_vulgar}? 'y':'n')),
	p(leaf('Download', 'download', manuscript_id=>$id, which => 'orig')));
  if (defined $m->{elapsed}) {
    print p('Pending: '.format_hours($m->{elapsed}).
	    ' (since '.substr($m->{manuscript_submitted},0, 16).')');
  }
  my @flag;
  for my $f (qw( FLAG_OK FLAG_SPAM FLAG_PROHIBITED FLAG_MISCATEGORIZED
 FLAG_CONFUSED)) {
    no strict 'refs';
    $flag[$f->()] = $f;
  }
  if (defined $m->{manuscript_flagged}) {
    print(p('Status: '.$flag[$m->{manuscript_flagged}]));
  }
  print(tag('table'),
        Tr(join(th(hskip(4)), th('When'), th('Elapsed'), th('Flag'))));
  while (defined(my $asn = $q1->fetchrow_hashref)) {
    print(Tr(join(td(),
		  td(substr($asn->{assignment_ctime},0,16)),
		  td(format_hours($asn->{elapsed})),
		  td($flag[$asn->{assignment_flag}]))));
  }
  print(gat('table'));
  $q1->finish;
  my @act;
  for (my $f=1; $flag[$f]; $f++) {
    next if $f == FLAG_CONFUSED;
    push @act, leaf($flag[$f], 'approve_manuscript', id=>$id, flag=>$f);
  }
  print(p(join('', map { p($_) } @act)),
	p({align=>'center'}, leaf('Go Back', 'front')));
}

1;
