use strict;
use warnings;

package PPX::Expired::Impl;
use PPX::HTML;
use PPX::Config;
use PPX::DB;
use PPX::Util;
use Locale::gettext;

sub front {
  banner('Expired');
  print(p(txt(gettext('Your session has expired.'))),
       p(hlink(SITE.CGI_URL, txt(gettext('Please login again.')))));
}

package PPX::Guest::Impl;
use Carp;
use PPX::Config;
use PPX::HTML;
use PPX::DB;
use PPX::Util;
use Locale::gettext;

package PPX::Member::Impl;
use Carp;
use PPX::Config;
use PPX::HTML;
use PPX::DB;
use PPX::Util;
use Locale::gettext;

package PPX::FlagAdmin::Impl;
use Carp;
use PPX::Config;
use PPX::HTML;
use PPX::DB;
use PPX::Util;
use Locale::gettext;

package PPX::Admin::Impl;
use Carp;
use PPX::Config;
use PPX::HTML;
use PPX::DB;
use PPX::Util;
use Locale::gettext;

#---------------------------------------------------------------------
# Copy methods from the *::Impl package to the * package. This insures
# that no extraneous methods are available to folks who edit URLs by
# hand for mischievous fun.

require PPX::Account;
require PPX::Manuscript;
require PPX::Search;

package PPX::Expired;

for my $m (qw(front)) {
  no strict 'refs';
  my $sub = \&{__PACKAGE__ . '::Impl::'. $m};
  if (defined &$sub) {
    *$m = $sub;
  } else {
    warn __PACKAGE__ . '::Impl::'. $m;
  }
}

package PPX::Guest;

my @test;
if (PPX::Config::SYSTEM_TEST) {
  @test = qw(crash_test1 crash_test2);
}

for my $m (@test,
	   qw(front handshake new_member await_activation reset_passwd
eval_reset_passwd await_login evaluate_login login email feed)) {
  no strict 'refs';
  my $sub = \&{__PACKAGE__ . '::Impl::'. $m};
  if (defined &$sub) {
    *$m = $sub;
  } else {
    warn __PACKAGE__ . '::Impl::'. $m;
  }
}

package PPX::Member;

# A valid cookie should not prevent the following pages from working.
#
*email = \&PPX::Guest::email;
*feed = \&PPX::Guest::feed;

for my $m (qw(front logout query_tz1 query_tz2 show_history
reg_manuscript edit_manuscript download delete_manuscript
find_manuscript show_manuscript assign_manuscript
withdraw_manuscript address_assignment redress_withdrawl
upload_revision show_revision show_assignments
contact_reviewer shirk_assignment redress_cancellation
find_screening_needed flag_manuscript edit_criteria update_criteria
delete_criteria choose_criteria record_criteria_subset
chg_passwd update_passwd chg_email await_email_confirm criteria_unsub
presubmit set_assignment_value stats edit_online)) {
  no strict 'refs';
  my $sub = \&{__PACKAGE__ . '::Impl::'. $m};
  if (defined &$sub) {
    *$m = $sub;
  } else {
    warn __PACKAGE__ . '::Impl::'. $m;
  }
}

package PPX::FlagAdmin;
use base 'PPX::Member';

for my $m (qw(approve_manuscript)) {
  no strict 'refs';
  my $sub = \&{__PACKAGE__ . '::Impl::'. $m};
  if (defined &$sub) {
    *$m = $sub;
  } else {
    warn __PACKAGE__ . '::Impl::'. $m;
  }
}

package PPX::Admin;
use base 'PPX::FlagAdmin';

for my $m (qw(switch_user bounce_email)) {
  no strict 'refs';
  my $sub = \&{__PACKAGE__ . '::Impl::'. $m};
  if (defined &$sub) {
    *$m = $sub;
  } else {
    warn __PACKAGE__ . '::Impl::'. $m;
  }
}

1;
