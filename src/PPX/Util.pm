# Copyright (C) 2004, 2005, 2006, 2007, 2008 Joshua Nathaniel Pritikin

use strict;
use warnings;

package PPX::Util;
use Carp;
use PPX::HTML;
use Locale::gettext;
use PPX::Config;
use PPX::DB;
require Text::Wrapper;

our @ISA = 'Exporter';
our @EXPORT = qw(sendmail sendmail_no_msgstamp
		 expert set_expert member set_member
                 leaf hlink leaf_plain mailto leaf_error
		 set_session_id cookie_get currval
		 print_postscript banner $HasBanner
                 start_form standard_nav
		 get_member_properties lsty Crypt
		 extract_email gen_token join_remedy
truncate_to_words truncate_to_chars convert_to_text cheap_password_hint
parse_duration format_hours manuscript_flagged
get_job_directory create_download_link get_email_link
get_assignment_directory query_current_balance query_available_manuscripts
		 read_from_pipe spawn_wait PERLIO_IS_ENABLED
query_member_quality_and_late keyword_to_query escalate_screening
banner_announce determine_member_class get_best_review);

use IO::Select;
use Config;
use constant PERLIO_IS_ENABLED => $Config{useperlio};

our $HasBanner;

sub read_from_pipe {
    my($fh) = @_;
    my $data = '';
    while (1) {
      if (PERLIO_IS_ENABLED || IO::Select->new($fh)->can_read(10)) {
	my $l = <$fh>;
	last if !defined $l;
	$data .= $l;
      }
    }
    $data;
}

{
  my @Cookie;
  sub cookie_add { push @Cookie, @_ }
  sub cookie_get {
    my @r = @Cookie;
    @Cookie =();
    \@r;
  }
}

sub join_remedy {
  my @err = @_;
  @err = map { txt($_) } @err;
  if (@err > 1) {
    join('',
	 gettext('Please remedy the following problems').':',
	 tag('ol'),
	 (map { li($_) } @err),
	 gat('ol'))
  } else {
    $err[0]
  }
}

sub gen_token {
  my ($len) = @_;
  $len ||= 2;
  my $tok = '';
  for (my $r=0; $r < $len; $r++) {
    $tok .= sprintf("%04x", int rand 65536);
  }
  $tok;
}

sub get_email_link {
  my ($purpose, $id, $payload) = @_;
  confess if !$id;

  my $tok = gen_token(4);

  my $i1 = $DB->prepare_cached(<<"SQL");
INSERT INTO email (email_purpose, email_token, member_id, email_payload)
VALUES (?,?,?,?)
SQL
  $i1->execute($purpose, $tok, $id, $payload);

  join('', SITE, CGI_URL, '?leaf=email', ';token=', $tok)
}

sub extract_email($) {
  my ($t) = @_;
  $t ||= '';
  $t =~ s/^\s+//;
  $t = lc substr $t, 0, EMAIL_LEN;
  my ($o) = Mail::Address->parse($t);
  if ($o) { $o->address }
  else    { undef }
}

sub get_member_properties {
  (' '.join(',',
	    ('current_date - member_hitstamp as stale',
	     'current_time at time zone member_tz as member_time',
	     'extract(year from now()) - member_birth > '.ADULT_AGE.' as member_adult',
	     '(SELECT COUNT(criteria_id) FROM member_criteria mc WHERE mc.member_id = m.member_id LIMIT 1) AS has_chosen_criteria',
	     'criteria_id',
	     map { "member_$_" } qw(email admin per_page tz balance cdate
 credit_limit failed_login flag_admin bouncing))).' ')
}

sub Crypt {
    my ($password, $origsalt) = @_;

    my $md5magic = "\$1\$";

    if (!defined $origsalt) {
      my @saltchars = ('a'..'z', 'A'..'Z', 0..9, '.', '/');

      my $salt = $md5magic;   # assume MD5 aware crypt

      # Salt is like a "hash" of the login_id which make it hard
      # to determine whether two accounts have the same password.

      for (my $i=0 ; $i < 8 ; ++$i) {
        $salt .= $saltchars[rand @saltchars];
      }
      $origsalt = $salt;
    } else {
      $origsalt = substr $origsalt, 0, 11;
    }

    my $foo = crypt($password, $origsalt);

    die "crypt returned $foo"
      if substr($foo,0,3) ne $md5magic;

    $foo;
}

sub start_form($) {
  my ($leaf) = @_;

  my @session;
  @session = (session_id => expert('session_id'),
	      session_salt => expert('session_salt'))
    if expert('session_id');
  push @session, previous_leaf => expert('leaf')
    if expert('leaf');

  join('',
       tag('form', name => $leaf, method=>'post', action=> CGI_URL,
	   enctype=>'multipart/form-data'),
       hidden(leaf => $leaf, @session));
}

{
  my @announce;

  sub banner_announce {
    push @announce, @_;
  }

  sub banner {
    my $title = shift;
    my %opt = @_;

    if ($HasBanner) {
      warn "banner($HasBanner) already called, banner($title) ignored";
      print hr; # oops, fake up a page divider
      return;
    }

    my @body = exists $opt{onload}? (onload => $opt{onload}) : ();

    $HasBanner = $title;
    print $R->header(-charset=>'utf-8',
		     -cookies => cookie_get());

    my $nav = '';
    if (member()) {
      my $full = member('member_time');
      my $tm = $full? substr($full, 0, 8) : hskip(1);
      $nav = leaf('Logout', 'logout').br().br().
	span({ style => 'background: blue; color: white;'}, $tm)
    }

    print(PPX::HTML::start(),
	  tag('head'),
	  emp('meta', 'http-equiv' => 'Content-Type',
	      content => 'text/html; charset=UTF-8'),
	  emp('link', rel=>'shortcut icon', type=>'image/vnd.microsoft.icon',
	      href => SITE.'/favicon.ico'),
	  tag('script', src=> ETC_URL.'toolbox.js', type=>'text/javascript'),
	  gat('script'),
	 );

    print(emp('link', rel=> 'stylesheet', type=>'text/css',
	      href => ETC_URL.'normal.css', title => 'Normal'),
	  elem('title', "PEX | $title"),
	  gat('head'),
	  tag('body', @body),

	  tag('div', style => 'float: right; margin: 1em; font-size: 70%; text-align: center;'),
	  $nav,
	  gat('div'),
	  tag('div', style => 'background: blue; color: white; padding: 1em; font-size: 125%; font-weight: bold;'),
	  gettext('Peer Editing Exchange'),
	  gat('div')
	 );

    my @id;
    if (SYSTEM_TEST) {
      my $caller = (caller(1))[3];
      $caller =~ s/^PPX:://;
      $caller =~ s/:Impl://;
      @id = (id => $caller);
    }
    print(tag('div', class=>'matter', @id, style => 'margin: 1em'),
	  @announce);
    @announce = ();
  }

  sub print_postscript {
    return if !$HasBanner;
    undef $HasBanner;

    if (!expert()) {
      print(tag('noscript'),
	    p(small(txt("Your browser doesn't support JavaScript.  Even so,
this is not a big problem since JavaScript is only used for
some minor visual effects.  You can probably navigate this site
just fine without JavaScript.  Enjoy."))),
	    gat('noscript'));
    }

    print(gat('div'),
	  br,br,
	  tag('div', style => 'background: blue; color: white; padding: 1em; text-align: center; font-size: 70%;'),
	  join(hskip(3).'&middot;'.hskip(3),
	       a({ href => '/static/copyright.html', style=>'color: white;' },
		 gettext('Copyright')),
	       a({ href => '/static/terms.html', style=>'color: white;' },
		 gettext('Terms and Conditions of Use')),
	       a({ href => '/static/faq.html', style=>'color: white;' },
		 gettext('FAQ')),
	       a({ href => 'mailto:'.EMAIL_FROM, style => 'color: white;' },
		 gettext('Contact'))),
	  gat('div')
	 );
  }
}


sub standard_nav {
  my @nav = (leaf('Edit Biographical', 'chg_profile'));
  push @nav,(leaf('Change Password', 'chg_passwd'),
	     leaf('Change Email', 'chg_email'))
    if !is_guest();
  push @nav, leaf('Goodbye', 'logout');

  print(p(),
	tag('table', style => 'border-spacing: .5ex'));
  for my $n (@nav) {
    print Tr(td($n));
  }
  print gat('table');
}

{
  # misnamed, a session does not imply the existance of an expert_id
  my $MemberData;
  sub member(;$) {
    my $k;
    if (@_ == 0) {
      $k = 'member_id';
    } else {
      $k = shift;
    }
    if (exists $MemberData->{$k}) {
      $MemberData->{$k}
    } else {
      undef
    }
  }
  sub set_member($;$) {
    if (@_ == 1) {
      $MemberData = shift;
      confess "$MemberData not ref"
	if (defined $MemberData and !ref $MemberData);
    } else {
      confess "no member" if !$MemberData;
      my ($k, $v) = @_;
      $MemberData->{$k} = $v;
    }
  }
  *expert = \&member;
  *set_expert = \&set_member;
}

sub set_session_id($;$) {
  my ($o, $cookie) = @_;
  for my $k (qw(session_id session_salt)) {
    carp "$k missing"
      if !exists $o->{$k};
  }
  if ($cookie) {
    cookie_add($R->cookie(-name => 'PPX_SESSION',
			  -value => "$o->{session_id}:$o->{session_salt}",
			  -expires => '+1y'));
  }
  set_expert($o);
}

sub keyword_to_query {
  my ($kw) = @_;
  $kw =~ s/[()!&|]/ /g;     # don't try to escape strange stuff
  my @hunk = split /(\-?\")/, $kw;
  my $phrase = 0;
  my $neg;
  my @or;
  while (@hunk) {
    if ($hunk[0] =~ /(-?)\"/) {
      $neg = $1;
      $phrase = !$phrase;
      shift @hunk;
      next;
    }
    my $piece = shift @hunk;
    $piece =~ s/^\s+//;
    my @w = split /\s+/, $piece;
    if (!$phrase) {
      for my $w (@w) {
	$w =~ s/^-/!/;
	push @or, $w
      }
    } else {
      for my $w (@w) { $w =~ s/^-// }
      my $ph = '('.join('&', @w).')';
      $ph = "!$ph" if $neg;
      push @or, $ph;
    }
  }
  join('|', @or) || '';
}

sub currval {
  my ($table, $col) = @_;
  my $q = $DB->prepare_cached('SELECT currval(pg_get_serial_sequence(?,?))');
  $q->execute($table, $col);
  my ($num) = $q->fetchrow_array;
  $q->finish;
  $num;
}

sub get_leaf_url {
  my $leaf = shift;
  confess "undef" if !defined $leaf;
  my @p;
  if (member('session_id')) {
    unshift(@_,
	    session_id => member('session_id'),
	    session_salt => member('session_salt'));
  }
  if (member('leaf')) {
    push @_, previous_leaf => member('leaf')
  }
  while (@_) {
    my $k = shift;
    my $v = shift;
    if (!defined $v) {
      carp("Value for $k is undef");
      next;
    }
    $v = CGI::Simple::Util::escape($v);
    push @p, "$k=$v";
  }
  if (!@p) {
    CGI_URL.'?leaf='.$leaf;
  } else {
    CGI_URL.'?leaf='.$leaf.';'.join(';', @p)
  }
}

{
  my $middot = '&middot;';
  sub hlink($$) {
    my ($href, $html) = @_;
    span({ class=>'button' }, a({ href => $href,
				  style => 'text-decoration: none;' },
				join(hskip(1), $middot, $html, $middot)));
  }
}

sub mailto {
  my ($addr) = @_;
  a({ href=>'mailto:'.$addr}, txt($addr));
}

sub cheap_password_hint {
  gettext('This is not a secure connection.
Please do not use a valuable password.')
}

sub leaf {
  my $html = shift;
  $html =~ s/ /&nbsp;/g; # never word-wrap buttons
  my $href = get_leaf_url(@_);
  hlink($href, $html)
}

sub leaf_plain {
  my $html = shift;
  a({ href => get_leaf_url(@_) }, $html);
}

sub leaf_error {
  my $html = shift;
  a({ class => 'error', href => get_leaf_url(@_) }, $html);
}

sub paging_setup {
  my $page;
  {
    no warnings 'numeric';
    $page = int($R->param('page') || 1);
  }
  $page = 1 if $page < 1;
  my $limit = ($page-1) * expert('per_page');
  ($limit, $page)
}

sub paging_nav {
  my $page = shift;

  my $cnt = $DB->prepare_cached('SELECT FOUND_ROWS()');
  $cnt->execute();
  my ($count) = $cnt->fetchrow_array;

  my $pages = int(($count + expert('per_page') - 1) / expert('per_page'));
  my $nav = '';

  if ($pages > 1) {
    my $p0 = $page - 5;
    my $p1 = $page + 5;
    $p0 = 1 if $p0 < 1;
    $p1 = $pages if $p1 > $pages;
    $nav .= join('',
		 tag('p'),
		 tag('div', align => 'center'),
		 tag('table'),
		 tag('tr'),
		 td({id => 'navlabel' },"Page"));

    for (my $n = $p0; $n <= $p1; $n++) {
      if ($n == $page) {
	$nav .= td({id => 'curnav'}, $n)
      } else {
	$nav .= td({id => 'nav'},
		   a({ href => leaf_url(@_, page => $n)}, $n))
      }
    }
    $nav .= join('',
		 gat('tr'),
		 gat('table'),
		 gat('div'),
		 gat('p'));
  }

  ($count, $pages, $nav)
}

sub truncate_to_chars {
  my ($str, $max) = @_;
  confess if $max <= 4;
  if (length $str > $max) {
    substr($str, 0, $max-4).' ...';
  } else {
    $str;
  }
}

sub truncate_to_words {
  my ($str, $count) = @_;
  my @w = split /\s+/, $str;
  if ($#w > $count-1) {
    $#w = $count-1;
  }
  join(' ', @w)
}

sub format_hours {
  my ($in_hours) = @_;
  return '?' if !defined $in_hours;
  my $neg;
  if ($in_hours < 0) {
    $in_hours = -$in_hours;
    $neg = '-';
  } else {
    $neg = '';
  }
  my $d = int($in_hours / 24);
  my $h = int($in_hours % 24);
  my @c;
  push @c, $d.'d' if $d;
  push @c, $h.'h' if $h;
  push @c, '0h' if !@c;
  $neg.join("\x{a0}", @c)
}

sub parse_duration {
  my ($due) = @_;
  my ($h,$d);
  while ($due =~ /\G\s*(\d+)\s*([hd])/gi) {
    if ($2 eq 'h') {
      last if defined $h;
      $h = $1;
    } elsif ($2 eq 'd') {
      last if defined $d;
      $d = $1;
    }
  }
  ($h||0) + ($d||0) * 24;
}

sub get_job_directory {
  my ($id) = @_;
  my $dir = sprintf '%02x', $id & 0xff;
  my $key = sprintf '%x', $id >> 8;
  
  my $d = "$BaseDir/var/jobs/$dir";
  -d $d or mkdir $d;
  $d .= "/$key";
  -d $d or mkdir $d;
  $d;
}

sub get_assignment_directory {
  my ($id, $reviewer_id) = @_;
  $reviewer_id = member()
    if !$reviewer_id;
  my $d = get_job_directory($id);
  my $rev = sprintf "%x", $reviewer_id;
  $d .= "/$rev";
  -d $d or mkdir $d;
  $d;
}

sub create_download_link {
  my ($path) = @_;
  my $ext = '';
  if ($path =~ /\.(\w+?)$/) {
    $ext = $1;
  } else {
    warn "Can't guess file extension of $path";
  }
  my $tok = '';
  for (my $r=0; $r < 6; $r++) {
    $tok .= sprintf("%04x", int rand 65536);
  }
  symlink $path, "$BaseDir/htdocs/download/$tok.$ext" or do {
    warn "symlink: $!";
    return;
  };
  SITE."/download/$tok.$ext";
}

sub query_current_balance {
  my ($member_id) = @_;
  $member_id = member()
    if !defined $member_id;
  my $q1 = $DB->prepare_cached('SELECT query_balance(?)');
  $q1->execute($member_id);
  my ($balance) = $q1->fetchrow_array;
  $q1->finish;
  $balance;
}

sub determine_member_class {
  my ($u) = @_;
  if ($u->{member_admin}) {
    'PPX::Admin';
  } elsif ($u->{member_flag_admin}) {
    'PPX::FlagAdmin';
  } else {
    'PPX::Member';
  }
}

sub query_member_quality_and_late {
  my ($member_id, $lang, $wl) = @_;

  my $sql = '';
  for (my $x=1; $x <= NUM_WRITING_LEVELS; $x++) {
    next if (defined $wl and $x != $wl);
    $sql .= sprintf('
COALESCE(member_lang_samples[%d],0) > %d as q_valid%d,
  round(%d*COALESCE(ml.member_lang_quality[%d], p.member_lang_quality[%d]))
    as quality%d,', $x, SENIORITY_THRESHOLD, $x,
		    MAX_REVISION_QUALITY, ($x) x 3);
  }

  my $q1 = $DB->prepare_cached(<<"SQL");
SELECT $sql
  (query_late_assignments(?)).late as assignments_late,
  (query_late_assignments(?)).total as assignments_total
FROM language p
  LEFT JOIN member_lang ml on
   (ml.language_id = p.language_id AND ml.member_id = ?)
WHERE p.language_id = ?;
SQL
  $q1->execute($member_id, $member_id, $member_id, $lang);
  my $i = $q1->fetchrow_hashref;
  $q1->finish;

  if (defined $wl) {
    $i->{q_valid} = $i->{"q_valid$wl"};
    $i->{quality} = $i->{"quality$wl"};
  }

  $i->{late} = 100 * $i->{assignments_late} / ($i->{assignments_total} || 1);
  $i;
}

sub query_available_manuscripts {
  my ($member_id, $criteria_id) = @_;

  my $u1 = $DB->prepare_cached(<<SQL);
UPDATE criteria
SET criteria_hitstamp = now(), criteria_hits = criteria_hits + 1
WHERE criteria_id = ?
SQL
  my $ok = int $u1->execute($criteria_id);
  if (!$ok) {
#    warn "query_available_manuscripts criteria_id $criteria_id not found";
    return;
  }

  my $i = query_member_quality_and_late($member_id, LANG_ENGLISH);
  my @arg;
  my @vet;
  for (my $x=1; $x <= NUM_WRITING_LEVELS; $x++) {
    push @vet, $i->{"q_valid$x"}? 'f':'t';
    push @arg, $i->{"q_valid$x"}? $i->{"quality$x"} : 0;
  }
  push @arg, int $i->{late};

  my $v1 = $DB->prepare_cached(<<SQL);
SELECT join_manuscript_criteria(?, 'mc_view')
SQL
  my $v2 = $DB->prepare_cached(<<SQL);
CREATE OR REPLACE TEMP VIEW mc_filtered AS
SELECT m.*,
  (((SELECT COUNT(*) FROM assignment a
     WHERE a.manuscript_id = m.manuscript_id AND
       assignment_cancelled IS NULL AND assignment_for = ?)
         < manuscript_max_reviews AND
      (ARRAY[?::smallint,?,?,?])[manuscript_writing] >= manuscript_min_quality AND
        ? <= manuscript_max_late AND
     NOT EXISTS(SELECT * FROM assignment a
                WHERE a.manuscript_id = m.manuscript_id AND
                  a.member_id = ?) AND
     NOT (m.manuscript_escrowed AND
       is_member_in_escrow(m.language_id, m.manuscript_writing, ?)))
   OR
   ((ARRAY[?::boolean,?,?,?])[manuscript_writing] AND
    manuscript_eligible_for_vetting AND
    (SELECT COUNT(*) FROM assignment a
     WHERE a.manuscript_id = m.manuscript_id AND
     assignment_cancelled IS NULL AND assignment_for = ?) < ? AND
     m.member_id NOT IN
       (SELECT anym.member_id
        FROM assignment a
          JOIN manuscript anym ON (a.manuscript_id = anym.manuscript_id)
        WHERE a.member_id = ?))) AS manuscript_open
FROM mc_view m
WHERE manuscript_submitted IS NOT NULL AND m.member_id != ? AND
  manuscript_withdrawn IS NULL AND manuscript_approved IS NOT NULL AND
  (manuscript_vulgar = 'f' OR
    (SELECT extract(year from now()) - member_birth > ?
     FROM member WHERE member_id = ?))
SQL
  my $v3 = $DB->prepare_cached(<<SQL);
SELECT query_manuscript_by_criteria_order('mc_filtered', ?)
SQL
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT manuscript_id, manuscript_title, manuscript_genre, manuscript_wc,
  manuscript_hours_remain, manuscript_submitted, manuscript_abstract,
  manuscript_context, manuscript_writing, manuscript_open,
  to_char(manuscript_submitted at time zone 'GMT',
    'DD Mon YYYY HH24:MI') || ' GMT' as pubDate
FROM mc_ordered
LIMIT 50
SQL

  $DB->begin_work;
  $v1->execute($criteria_id);
  $v1->finish;
  my @tmp = (ASSIGN_FOR_EDITING, @arg, $member_id, $member_id, @vet,
	     ASSIGN_FOR_VETTING, MAX_VETTING_ASSIGNMENTS, $member_id,
	     $member_id, ADULT_AGE, $member_id);
  $v2->execute(@tmp);
  $v2->finish;
  $v3->execute($criteria_id);
  $v3->finish;
  $q1->execute;
  $DB->commit;

  $q1;
}

sub get_best_review {
  my ($id, $min, $for) = @_;

  # Are all assignments archived?
  my $q1 = $DB->prepare_cached(<<SQL);
SELECT member_id
FROM assignment
WHERE manuscript_id = ? AND NOT assignment_archived
LIMIT 1
SQL
  $q1->execute($id);
  my ($pending) = $q1->fetchrow_array;
  $q1->finish;
  return if $pending;

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT member_id, extract_assignment_quality(data.*)
FROM (SELECT *
      FROM assignment a
      WHERE a.assignment_cancelled IS NULL AND
        assignment_for != ? AND
        manuscript_id = ? AND
       (a.assignment_revtime[1] IS NOT NULL OR
        a.assignment_revtime[2] IS NOT NULL)) AS data
WHERE
  extract_assignment_quality(data.*) >= ? AND member_id != ?
ORDER BY extract_assignment_quality(data.*) DESC, assignment_ctime
LIMIT 1
SQL
  $min = -1 if !defined $min;
  $q2->execute(ASSIGN_FOR_SCREENING, $id, $min, ($for||0));
  my @r = $q2->fetchrow_array;
  $q2->finish;
  @r;
}

sub manuscript_flagged {
  my ($id, $flag) = @_;

  if ($flag == FLAG_MISCATEGORIZED) {
    my $q2 = $DB->prepare_cached(<<SQL);
SELECT manuscript_title, member_email, member_id
FROM manuscript NATURAL JOIN member
WHERE manuscript_id = ?
SQL
  $q2->execute($id);
    my ($m) = $q2->fetchrow_hashref;
    $q2->finish;

    my $title = $m->{manuscript_title};
    my $link = get_email_link(EMAIL_LOGIN, $m->{member_id},
			      "leaf=reg_manuscript;id=$id");

    sendmail_no_msgstamp($m->{member_email},
			 'Re: '.truncate_to_chars($title, 40),<<"EMAIL");
Your manuscript has been screened and found lacking in at least one
of the following ways:

- The genre or abstract is not an accurate reflection of the content
  of the manuscript.

- The title is misleading.

- The audience designation does not seem to accurately reflect the
target audience of the manuscript.

- The instructions and context are confusing or request action which
is inappropriate or unethical.

Please login and remedy the problem:

  $link
EMAIL

  } elsif ($flag == FLAG_CONFUSED) {
    escalate_screening($id);
  }
}

sub lsty {
  my ($err, $k, $label) = @_;
  if ($err and $err->{$k}) {
    span({ class => 'error' }, txt $label)
  } else {
    txt $label
  }
}

sub sendmail {
  my $id = shift;
  $DB->do("update member set member_msgstamp = now()
where member_id = ?", {}, $id);
  sendmail_no_msgstamp(@_);
}

sub sendmail_no_msgstamp {
  my ($to, $subject, $body, $replyto) = @_;

  $body = "[No message]\n" if !defined $body;
  $body =~ s/^\s+//;
  $body .= "\n" if $body !~ /\n$/s;

  my $msgto;
  if (ref $to) {
    $msgto = join(", ", @$to);
  } else {
    $msgto = $to;
  }
  my @head;
  if ($replyto) {
    push @head, "Reply-To: $replyto";
  }
  my $header = join("\n",
		    'From: PEX <'.EMAIL_FROM.'>',
		    "To: $msgto",
		    "Subject: $subject",
		    @head);
my $msg = "$header

$body";

  my @args = ('-t', '-f', EMAIL_FROM);
  push @args, EMAIL_TO_OVERRIDE
    if EMAIL_TO_OVERRIDE;
  my ($in,$out,$err);
  my $try = 0;
  my $ok;
  while (++$try < 10) {
    if ($R) {
      ($in,$out,$err) =
	$R->_mod_perl_request->spawn_proc_prog(EMAIL_INJECT, \@args);
    } else {
      require IPC::Open3;
      $err = Symbol::gensym(); # tell open3 not to merge STDOUT and STDERR
      IPC::Open3::open3($in, $out, $err, EMAIL_INJECT, @args);
    }
    if (!defined $in) {
      sleep 1;
      next;
    }
    binmode $in, ':utf8';
    print $in $msg;
    close $in;
    my $st = spawn_wait(EMAIL_INJECT, $out, $err);
    if ($st == 0) { $ok=1; last }
    warn sprintf '%s exited with status %d, proceeding with retry #%d', EMAIL_INJECT, $st, $try;
    sleep 1;
  }
  warn "Failed to send message:\n$msg" if !$ok;
}

sub convert_to_text {
  use warnings FATAL => qw(utf8);
  my ($orig, $dest, $wrap) = @_;

  open my $in, $orig or do {
    warn "open $orig: $!";
    return 1;
  };
  binmode $in, ':utf8';
  open my $out, ">$dest" or do {
    warn "open >$dest: $!";
    return 1;
  };
  binmode $out, ':utf8';
  my $blob = join '', <$in>;  # this will die on bad utf8
  if ($blob =~ s/\r\n/\n/g) {
    # dos2unix TODO
  } elsif ($blob =~ s/\r/\n/g) {
    # mac2unix TODO
  }
  if ($wrap) {
    my $wrapper = Text::Wrapper->new(columns => 65);
    my @par = split /\n\n+/, $blob;
    @par = map { s/\n/ /g; $wrapper->wrap($_) } @par;
    $blob = join "\n", @par;
  }
  print $out $blob;
  close($out) or do {
    warn "close >$dest: $!";
    return 1;
  };
  0; #no error
}

sub spawn_wait {
  my ($exe, @fh) = @_;
  for my $fh (@fh) {
    my $output = read_from_pipe($fh);
    warn $output if $output;
  }
  wait;
  my $ok;
  if ($? == -1) {
    warn "Failed to execute $exe";
    $ok = -1;
  } elsif ($? & 127) {
    warn sprintf "$exe died with signal %d", ($? & 127);
    $ok = -1;
  } else {
    $ok = $? >> 8;
  }
  $ok;
}

sub trim_whitespace {
  my ($s) = @_;
  if (!defined $s) {
    carp "Undefined string in trim_whitespace";
    return $s;
  }
  $s =~ s/(?s)\s+$//;
  $s =~ s/(?s)^\s+$//;
  $s
}

sub escalate_screening {
  my ($id) = @_;

  my $dir = get_job_directory($id);
  my $name = "$dir/orig.txt";
  open my $fh, $name or warn "open $name: $!";
  binmode $fh, ':utf8';
  my $manuscript = join '', <$fh>;

  my $q2 = $DB->prepare_cached(<<SQL);
SELECT manuscript_context, manuscript_genre, manuscript_title,
  manuscript_abstract, manuscript_writing
FROM manuscript
WHERE manuscript_id = ?
SQL
  $q2->execute($id);
  my ($m) = $q2->fetchrow_hashref;
  $q2->finish;

  for (qw(manuscript_context manuscript_abstract)) {
    $m->{$_} = '' if !$m->{$_};
  }

  my $q1 = $DB->prepare(<<SQL);
select member_id, member_email from member where member_flag_admin = 't'
SQL
  $q1->execute;
  my $ok=0;
  while (my ($member_id, $member_email) = $q1->fetchrow_array) {
    my $body = '';
    my $pdf='';
    if (-e "$dir/orig.pdf") {
      $pdf = 'PDF: '.create_download_link("$dir/orig.pdf");
    }
    my $link = get_email_link(EMAIL_LOGIN, $member_id,
			     "leaf=approve_manuscript;id=$id");
    $body .= "$link\n";
    my $wl = WRITING_LEVEL->[$m->{manuscript_writing}-1];
    $body .= "
Context and Instructions[$m->{manuscript_context}]
Genre[$m->{manuscript_genre}]
Title[$m->{manuscript_title}]
Audience[$wl]
Abstract[$m->{manuscript_abstract}]

$pdf

- - -
$manuscript";

    my $title = truncate_to_chars($m->{manuscript_title}, 60);
    sendmail_no_msgstamp($member_email, "[FLAG] $title", $body);
    ++$ok;
  }
  if (!$ok) {
    warn "No member_flag_admin";
  } else {
    my $u1 = $DB->prepare_cached(<<SQL);
UPDATE manuscript SET manuscript_escalated = 't' WHERE manuscript_id = ?
SQL
    $u1->execute($id);
  }
}

package PPX::Menu;
use Carp;
use PPX::HTML;

sub new { bless $_[1], $_[0] }

sub simple_menu {
  my ($o) = @_;
  my $buf = '';
  my @leaf = @$o;
  shift @leaf;
  shift @leaf;
  for (my $x=0; $x < @leaf; $x += 2) {
    my ($html, $url) = ($leaf[$x], $leaf[$x+1]);
    $buf .= p(a({ href => $url }, $html));
  }
  $buf
}

sub nav {
  my ($o, $cur) = @_;

  my @leaf = @$o;
  my $group_name = shift @leaf;
  shift @leaf;

  my $xcur;
  for (my $x=0; $x < @leaf; $x += 2) {
    if ($cur and $cur eq $leaf[$x]) {
      $xcur = $x;
      last;
    }
  }

  croak "Can't find $cur in menu"
    if defined $cur && !defined $xcur;

  my $buf = (br.br.tag('p').tag('div', align =>'center').
	     tag('table', border=>1, cellspacing=>0, cellpadding=>4));
  $buf .= Tr(td({align=>'center',bgcolor=>'yellow', colspan => @leaf/2},
		$group_name.' (Navigation Panel)'));
  $buf .= tag('tr');
  for (my $x=0; $x < @leaf; $x += 2) {
    my ($html, $url) = ($leaf[$x], $leaf[$x+1]);
    if (defined $xcur and $x == $xcur) {
      $buf .= td({align => 'center'}, $html);
    } else {
      $buf .= td({align => 'center'}, a({ href => $url }, $html));
    }
  }
  $buf .= gat('tr') . gat('table') . gat('div') . gat('p');
  $buf;
}

1;
